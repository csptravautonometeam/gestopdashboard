#tag WebStyle
WebStyle LightBox
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid E8E8E8FF
		border-top=3px solid B6B6B6FF
		border-left=3px solid B6B6B6FF
		border-bottom=3px solid B6B6B6FF
		border-right=3px solid B6B6B6FF
		shadow-box=3px 0px 8px 0000007F
		misc-opacity=0.3058824
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle LightBox
#tag EndWebStyle

