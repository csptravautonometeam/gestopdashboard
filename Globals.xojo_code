#tag Module
Protected Module Globals
	#tag Method, Flags = &h0
		Function arrondirCentaineSuperieure(integer_param As Integer) As Integer
		  // Arrondir à la centaine supérieure
		  Dim maxScaleTemp As String = str(integer_param)
		  Dim maxScaleDouble As Double = integer_param
		  Dim longueur As Integer = Len(maxScaleTemp)
		  Dim centaineSuperieure As Integer = Floor((maxScaleDouble + 10 ^ (longueur - 2)) / 10 ^ (longueur - 2)) * 10 ^ (longueur - 2)
		  
		  Return centaineSuperieure
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GMTextConversion(tempString As String) As String
		  Dim c as TextConverter
		  c=GetTextConverter(GetTextEncoding(&h500), GetTextEncoding(0))
		  return c.convert( tempString)
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
