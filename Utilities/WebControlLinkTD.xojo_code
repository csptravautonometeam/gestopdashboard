#tag Module
Protected Module WebControlLinkTD
	#tag Method, Flags = &h0
		Sub LinkAddTD(Extends obj As WebControl, strLink As String, strTarget As String = "_blank")
		  // NOTE: If you add a link to a dynamically created control, be sure to remove the link before
		  // deleting the control (if your code later deletes the control).
		  
		  'Add the link.
		  Dim js As String = jsLinkAdd
		  js = js.ReplaceAll("^id", obj.ControlID)
		  js = js.ReplaceAll("^link", strLink)
		  js = js.ReplaceAll("^target", strTarget)
		  obj.ExecuteJavaScript(js)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LinkChangeTD(Extends obj As WebControl, strLink As String, strTarget As String = "_blank")
		  'Change the link.
		  Dim js As String = jsLinkChange
		  js = js.ReplaceAll("^id", obj.ControlID)
		  js = js.ReplaceAll("^link", strLink)
		  js = js.ReplaceAll("^target", strTarget)
		  obj.ExecuteJavaScript(js)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LinkRemoveTD(Extends obj As WebControl)
		  'Remove the link.
		  Dim js As String = jsLinkRemove
		  js = js.ReplaceAll("^id", obj.ControlID)
		  obj.ExecuteJavaScript(js)
		  
		End Sub
	#tag EndMethod


	#tag Note, Name = License
		'* Copyright (c) 2012, Taylor Design
		'* All rights reserved.
		'*
		'* Redistribution and use in source and binary forms, with or without
		'* modification, are permitted provided that the following conditions are met:
		'*     * Redistributions of source code must retain the above copyright
		'*       notice, this list of conditions and the following disclaimer.
		'*     * Redistributions in binary form must reproduce the above copyright
		'*       notice, this list of conditions and the following disclaimer in the
		'*       documentation and/or other materials provided with the distribution.
		'*     * Neither the name of the company nor the
		'*       names of its contributors may be used to endorse or promote products
		'*       derived from this software without specific prior written permission.
		'*
		'* THIS SOFTWARE IS PROVIDED BY Taylor Design ``AS IS'' AND ANY
		'* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
		'* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
		'* DISCLAIMED. IN NO EVENT SHALL Taylor Design BE LIABLE FOR ANY
		'* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		'* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
		'* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
		'* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		'* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
		'* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
		
	#tag EndNote

	#tag Note, Name = Overview
		Author: Daniel L. Taylor
		Date: August 5, 2012
		Version: 1.1
		
		This module lets you add a standard HREF link to a WebControl.
		In other words the control will still look the same, but clicking it will be just like
		clicking a WebLink.
		
		I have only tested this with WebButton and WebImageView. The original intent was
		to use this code only with those control types. But I no longer enforce this in code
		just in case a user finds it to be useful with another control type.
		
		If you add a link to a dynamically created control, be sure to remove the link before
		deleting the control (if your code later deletes the control).
		
		Some people have requested this so that they can have butons or images on their
		page which open external web pages in new windows/tabs. Popup blockers will not
		block a new window if it's initiated by a user click, but they will block JavaScript
		which means you can't just run some JavaScript from the control event to open
		a new window.
		
		NOTES
		
		DoubleClick - this event no longer fires in FireFox, Safari, or Chrome after you've 
		added a link.
	#tag EndNote


	#tag Constant, Name = jsLinkAdd, Type = String, Dynamic = False, Default = \"try\r{\r\tvar obj \x3D document.getElementById(\'^id_td_link\');\r\tif (obj !\x3D null && obj !\x3D undefined)\r\t{\r\t\tobj.href \x3D \'^link\';\r\t\tobj.target \x3D \'^target\';\r\t}\r\telse\r\t{\r\t\tvar objControl \x3D document.getElementById(\'^id\');\r\t\tif (objControl !\x3D null && objControl !\x3D undefined)\r\t\t{\r\t\t\tvar objLink \x3D document.createElement(\'a\');\r\t\t\tobjLink.id \x3D \'^id_td_link\';\r\t\t\tobjLink.href \x3D \'^link\';\r\t\t\tobjLink.target \x3D \'^target\';\r\t\t\tobjLink.style.textDecoration \x3D \'none\';\r\t\t\t\r\t\t\tobjControl.parentNode.replaceChild(objLink\x2C objControl); \r\t\t\tobjLink.appendChild(objControl);\r\t\t}\r\t}\r}\rcatch (err)\r{\r\t//alert(err);\r}", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = jsLinkChange, Type = String, Dynamic = False, Default = \"try\r{\r\tvar obj \x3D document.getElementById(\'^id_td_link\');\r\tif (obj !\x3D null && obj !\x3D undefined)\r\t{\r\t\tobj.href \x3D \'^link\';\r\t\tobj.target \x3D \'^target\';\r\t}\r}\rcatch (err)\r{\r\t//alert(err);\r}", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = jsLinkRemove, Type = String, Dynamic = False, Default = \"try\r{\r\tvar objLink \x3D document.getElementById(\'^id_td_link\');\r\tvar objControl \x3D document.getElementById(\'^id\');\r\tobjLink.parentNode.replaceChild(objControl\x2C objLink);\r}\rcatch (err)\r{\r\t//alert(err);\r}", Scope = Protected
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
