#tag Class
Protected Class INISettings
	#tag Method, Flags = &h21
		Private Sub Clear()
		  INI_Data.Clear
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ini_filename As folderItem)
		  FileInfo=GetFolderItem(INI_Filename.NativePath,FolderItem.PathTypeAbsolute)
		  ini_data=new Dictionary
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get(section As String, keyvalue As String, default_value As Boolean) As Boolean
		  Dim s As String
		  s=Uppercase(Get(Section,keyvalue,Str(default_value)))
		  Return (s="TRUE")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get(section As String, keyvalue As String, default_value As color) As color
		  Dim s As String
		  Dim r As Integer
		  Dim g As Integer
		  Dim b As Integer
		  s=Get(Section,keyvalue,Str(default_value))
		  r=Val("&h"+Mid(s,3,2))
		  g=Val("&h"+Mid(s,5,2))
		  b=Val("&h"+Mid(s,7,2))
		  Return RGB(r,g,b)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get(section As String, keyvalue As String, default_value As Integer) As Integer
		  Dim s As String
		  s=Get(Section,keyvalue,Str(default_value))
		  Return Val(s)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get(section As String, keyvalue As String, default_value As Single) As Single
		  Dim s As String
		  s=Get(Section,keyvalue,Str(default_value))
		  Return Val(s)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get(section As String, keyvalue As String, default_value As String) As String
		  Dim answer As String
		  Dim new_key As String
		  answer=default_value
		  new_key=Real_Key(Section,keyvalue)
		  default_value=Trim(default_value)
		  If INI_Data.haskey(new_key) Then
		    answer=INI_Data.Value(new_key)
		  Else
		    If default_value<>"" Then Put Section,keyvalue,default_value
		  End If
		  
		  Return answer
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Load()
		  
		  Dim t As TextInputStream
		  Dim x As Integer
		  Dim s As String
		  Dim Section As String
		  INI_Data=New Dictionary
		  Section=""
		  
		  Clear
		  If fileinfo <> Nil Then
		    If fileinfo.Exists Then
		      t = TextInputStream.Open(fileinfo)
		      If t <> Nil Then
		        Do Until t.EOF
		          s=Trim(t.ReadLine)
		          If Left(s,1)="[" And Right(s,1)="]" Then
		            Section=Trim(Mid(s,2,Len(s)-2))
		          Else
		            If Section<>"" Then
		              x=InStr(s,"=")
		              If x>1 And x<Len(s) Then
		                Put Section,Trim(Left(s,x-1)),Trim(Mid(s,x+1))
		              End If
		            End If
		          End If
		        Loop
		        t.Close
		      End If
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Put(section As String, keyvalue As String, arg_value As Boolean)
		  Put Section,keyvalue,Str(arg_value)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Put(section As String, keyvalue As String, arg_value As color)
		  Put Section,keyvalue,Str(arg_value)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Put(section As String, keyvalue As String, arg_value As Integer)
		  Put Section,keyvalue,Str(arg_value)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Put(section As String, keyvalue As String, arg_value As Single)
		  Put Section,keyvalue,Str(arg_value)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Put(section As String, keyvalue As String, arg_value As String)
		  Dim new_key As String
		  new_key=Real_Key(Section,keyvalue)
		  INI_Data.Value(new_key)=arg_value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Real_Key(section As String, keyvalue As String) As String
		  Return Trim(Uppercase(Section))+"/"+Trim(Lowercase(keyvalue))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Remove(section As String, keyvalue As String)
		  Dim new_key As String
		  new_key=Real_Key(Section,keyvalue)
		  If INI_Data.haskey(new_key) Then INI_Data.Remove(new_key)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Remove_Section(section As String)
		  Dim sect_keys(-1) As String
		  Dim slen As Integer
		  Dim cnt As Integer
		  Dim i As Integer
		  Dim key As String
		  section=Slash_Add_Trailing(Trim(Uppercase(Section)))
		  slen=len(section)
		  cnt=INI_Data.count
		  if cnt=0 then exit sub
		  for i=cnt-1 downto 0
		    key=INI_Data.key(i)
		    if left(key,slen)=section then sect_keys.append key
		  next i
		  if ubound(sect_keys)<0 then exit sub
		  for i=0 to ubound(sect_keys)
		    ini_Data.remove(sect_keys(i))
		  next i
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Save()
		  Dim i As Integer
		  Dim x As Integer
		  Dim new_section As String
		  Dim t As TextOutputStream
		  Dim s As String
		  Dim Section As String
		  Dim key As String
		  Dim Value As String
		  Dim cnt As Integer
		  Dim SortMe() As String
		  Section=""
		  If fileinfo<> Nil Then
		    t = TextOutputStream.Create(fileinfo)
		    If t <> Nil Then
		      cnt=INI_Data.Count
		      If cnt>0 Then
		        '
		        ' Read Keys into an Array to be Sorted
		        '
		        for i=0 to cnt-1
		          sortme.append INI_Data.key(i)
		        next i
		        sortme.Sort
		        
		        '
		        ' Write back to File
		        '
		        Section=""
		        
		        For i=0 To cnt-1
		          s=SortMe(i)
		          x=InStr(s,"/")
		          If x>0 Then
		            new_section="["+Uppercase(Trim(Left(s,x-1)))+"]"
		            key=Lowercase(Trim(Mid(s,x+1)))
		            Value=INI_Data.Value(s)
		            If new_section<>Section Then
		              Section=new_section
		              t.WriteLine ""
		              t.WriteLine new_section
		            End If
		            t.WriteLine key+"="+Value
		          End If
		        Next i
		      End If
		      t.Close
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Slash_Add_Trailing(s as string) As string
		  if right(s,1)<>"/" then s=s+"/"
		  return s
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private FileInfo As folderItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private INI_Data As dictionary
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
