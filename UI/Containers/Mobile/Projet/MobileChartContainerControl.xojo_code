#tag WebPage
Begin WebContainer MobileChartContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   415
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   320
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebChartView EstimeReelMobileChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   195
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   0
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   60
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   320
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle HeaderRectangle
      Cursor          =   0
      Enabled         =   True
      Height          =   42
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1116138280"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   320
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel HeaderLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   42
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   78
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1787973756"
      TabOrder        =   6
      Text            =   "#DASHBOARD"
      TextAlign       =   0
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   182
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BackToProjectsButton
      AutoDisable     =   False
      Caption         =   "#BACKTOPROJECTS"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   4
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1076597147"
      TabOrder        =   0
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   78
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h21
		Private Sub estimeReelDisp()
		  EstimeReelMobileChartView.Type = EstimeReelMobileChartView.TypeBullet
		  
		  Dim S As new ChartSerie
		  S.Title = ACTUAL
		  S.Data = Array(reelMontantTotalFacture, reelMontantTotalMO, reelMontantTotalMAT, reelProfit)
		  //S.Data = Array(8430, 4963, 1581)
		  
		  EstimeReelMobileChartView.AddSerie(S)
		  
		  S = New ChartSerie
		  S.Title = ESTIMATE
		  If estimeMontantFacture <> 0 Then
		    S.Data = Array(estimeMontantFacture, estimeMontantMO, estimeMontantMAT, estimeProfit)
		  Else
		    S.Data = Array(estimeMontantFactureExtra, estimeMontantMO, estimeMontantMAT, estimeProfit)
		  End If
		  //S.Data = Array(8500, 5000, 1500)
		  S.FillColor = &c505050
		  'S.FillType = ChartSerie.FillPicture
		  
		  EstimeReelMobileChartView.AddSerie(S)
		  
		  EstimeReelMobileChartView.BackgroundType = WebChartView.BackgroundSolid
		  EstimeReelMobileChartView.BackgroundColor.append &cFFFFFF
		  
		  EstimeReelMobileChartView.Axes(0).Label.append REVENUE
		  EstimeReelMobileChartView.Axes(0).Label.append MANPOWER
		  EstimeReelMobileChartView.Axes(0).Label.append MATERIAL
		  EstimeReelMobileChartView.Axes(0).Label.append PROFIT
		  
		  EstimeReelMobileChartView.LegendPosition = WebChartView.Position.Right
		  
		  EstimeReelMobileChartView.setTransparency(40)
		  
		  EstimeReelMobileChartView.Axes(1).Visible = False
		  EstimeReelMobileChartView.Axes(1).MaximumScale = estimeReelMaxScale
		  EstimeReelMobileChartView.Axes(0).AxeLine.visible = False
		  
		  EstimeReelMobileChartView.setDataLabel("", 14)
		  EstimeReelMobileChartView.DataLabel.Position = 5
		  
		  EstimeReelMobileChartView.Redisplay()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub estimeReelExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture de l'estimé pour le revenu total
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "          AND SUBSTRING(estime_rev_mo_mat,1,2) <> 'AJ' " 
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantFacture = dashboardRS.Field("MontantAvecProfit").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture de l'estimé pour le revenu  des demandes de changement
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "          AND estime_rev_mo_mat = 'AJREV' " 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantFactureExtra = dashboardRS.Field("MontantAvecProfit").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Arrondir estimeReelMaxScaleTemp à la centaine supérieure
		  If estimeMontantFactureExtra > estimeMontantFacture Then
		    estimeReelMaxScale = Globals.arrondirCentaineSuperieure(estimeMontantFactureExtra)
		  Else
		    estimeReelMaxScale = Globals.arrondirCentaineSuperieure(estimeMontantFacture)
		  End If
		  
		  // Lecture de l'estimé pour la main d'oeuvre
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + sauvegardeProjetNo +"'"   + _
		  "          AND estime_rev_mo_mat    = 'MO'" 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantMO = dashboardRS.Field("Montant").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture de l'estimé pour le matériel
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + sauvegardeProjetNo +"'"   + _
		  "          AND estime_rev_mo_mat    = 'MAT'" 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantMAT = dashboardRS.Field("Montant").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Calcul du profit estimé
		  estimeProfit = estimeMontantFacture - estimeMontantMAT - estimeMontantMO
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = "SELECT ROUND(SUM(realiserev_montant),0) As Montant, ROUND(SUM(realiserev_paiement),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE realiserev_projet_no = '" + sauvegardeProjetNo +"'" 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  reelMontantTotalFacture = dashboardRS.Field("Montant").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  If estimeReelMaxScale < reelMontantTotalFacture Then
		    estimeReelMaxScale =  Globals.arrondirCentaineSuperieure(reelMontantTotalFacture)
		  End If
		  If estimeMontantFacture = 0 Then estimeMontantFacture = reelMontantTotalFacture
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre
		  strSQL = _
		  "SELECT SUM(Montant) As Montant  FROM" + _
		  "   (SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + sauvegardeProjetNo +"'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) = '6' "  + _
		  "   UNION " + _
		  "   SELECT ROUND(SUM(realisemocont_salbrut+realisemocont_das+realisemocont_cnesst),0) As Montant " + _
		  "        FROM realisemocont " + _ 
		  "       WHERE realisemocont_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "   )" + _
		  "  AS MOTable "
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  reelMontantTotalMO = dashboardRS.Field("Montant").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture du réalisé pour les dépenses de matériel
		  strSQL = _
		  "SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + sauvegardeProjetNo +"'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) IN('5','8') " 
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  reelMontantTotalMAT = dashboardRS.Field("Montant").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Calcul du profit réalisé
		  reelProfit = reelMontantTotalFacture - reelMontantTotalMAT - reelMontantTotalMO
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listProjeMobileDetail()
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  //GenControlModule.resetControls(Self, True, True, True)
		  
		  Dim panneauMobileSelectionContainerControl As MobileSelectionContainerControl = MobileSelectionContainerControl(Self.Parent)
		  Dim pointeurTemp As MobileProjetSideBar = panneauMobileSelectionContainerControl.MobileSelectionMobileProjetSideBar
		  
		  // Mode Modification, Lire les données
		  Dim projetRS As RecordSet = pointeurTemp.sProjet.loadDataByField(Session.bdGroupeRPF, "projet", _
		  "projet_id", str(pointeurTemp.projetStruct.id), "projet_id")
		  If projetRS <> Nil Then pointeurTemp.sProjet.LireDonneesBD(projetRS, "projet_id")
		  
		  // Sauvegarde
		  sauvegardeProjetNo = projetRS.Field("projet_no").StringValue
		  Dim projet_no As String = sauvegardeProjetNo
		  pointeurTemp.projetStruct.id = projetRS.Field("projet_id").IntegerValue
		  projetRS.Close
		  projetRS = Nil
		  
		  // Graphique estimé
		  estimeReelExtr
		  EstimeReelDisp
		  
		  // Assignation des propriétés aux Contrôles
		  //pointeurTemp.sProjet.assignPropertiesToControls(Self, Self.prefix)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  //GenControlModule.resetControls(Self, False, False, True)
		  
		  
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private estimeMontantFacture As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantFactureExtra As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantMAT As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantMO As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeProfit As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeReelMaxScale As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelMontantTotalFacture As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelMontantTotalMAT As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelMontantTotalMO As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelProfit As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeProjetNo As String
	#tag EndProperty


	#tag Constant, Name = ACTUAL, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Actual"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9el"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Actual"
	#tag EndConstant

	#tag Constant, Name = AMOUNT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Amount"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Montant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Amount"
	#tag EndConstant

	#tag Constant, Name = BACKTOPROJECTS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"< Projects"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"< Projets"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"< Projects"
	#tag EndConstant

	#tag Constant, Name = DASHBOARD, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Dashboard"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Tableau de bord"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Dashboard"
	#tag EndConstant

	#tag Constant, Name = DIVERSIONFROMESTIMATETITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Diversion from Estimate"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89cart de l\'Estim\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Diversion from Estimate"
	#tag EndConstant

	#tag Constant, Name = ESTIMATE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Estimate"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Estim\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Estimate"
	#tag EndConstant

	#tag Constant, Name = ESTIMEREELTITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ESTIMATE vs ACTUAL"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"ESTIM\xC3\x89 vs R\xC3\x89EL"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"ESTIMATE vs ACTUAL"
	#tag EndConstant

	#tag Constant, Name = EXTRAS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Extras"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Extras"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Extras"
	#tag EndConstant

	#tag Constant, Name = MANPOWER, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Manpower"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Main d\'oeuvre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Manpower"
	#tag EndConstant

	#tag Constant, Name = MATERIAL, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Material"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mat\xC3\xA9riel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Material"
	#tag EndConstant

	#tag Constant, Name = PROFIT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Profit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Profit"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Profit"
	#tag EndConstant

	#tag Constant, Name = REVENUE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Revenue"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Revenu"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Revenue"
	#tag EndConstant

	#tag Constant, Name = REVENUTOTALTITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Total Revenue"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Revenu Total"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Total Revenue"
	#tag EndConstant

	#tag Constant, Name = SUMMARYCODEGLTITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"SUMMARY BY GENERAL LEDGER CODE"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"SOMMAIRE PAR CODE DE GRAND LIVRE"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SUMMARY BY GENERAL LEDGER CODE"
	#tag EndConstant


#tag EndWindowCode

#tag Events EstimeReelMobileChartView
	#tag Event
		Sub Open()
		  Me.Type = Me.TypePie
		  Me.DoughnutRadius = 0.25
		  Me.EnableSelection = True
		  
		  
		  Me.Title = "European Sales by Countries"
		  Me.BackgroundType = WebChartView.BackgroundSolid
		  Me.BackgroundColor.append &cFFFFFF
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BackToProjectsButton
	#tag Event
		Sub Action()
		  Dim panneauMobileSelectionContainerControl As MobileSelectionContainerControl = MobileSelectionContainerControl(Self.Parent)
		  Self.Visible = False
		  panneauMobileSelectionContainerControl.MobileSelectionMobileProjetSideBar.populateProjet
		  panneauMobileSelectionContainerControl.MobileSelectionMobileProjetSideBar.Visible = True
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
