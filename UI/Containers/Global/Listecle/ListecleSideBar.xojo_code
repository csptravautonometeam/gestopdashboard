#tag WebPage
Begin WebContainer ListecleSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   160
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "931806350"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   170
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   160
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1804472319"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox ListecleListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   1
      ColumnWidths    =   "100%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   115
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModListecle
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#ListecleModule.HINTLOCKUNLOCK"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   96
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1238587391
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVEnrListecle
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#ListecleModule.HINTUPDATEDETAILS"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   132
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1113270271
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ListeCleLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   37
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   4
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "565612543"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   6
      VerticalCenter  =   0
      Visible         =   True
      Width           =   87
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  populateListecle
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub majListecleDetail()
		  // Vider dans la table les enregistrements correspondant à la liste sélectionnée
		  Dim messageErreur  As String = Self.sListecle.supprimerDataByFieldCond(Session.bdGroupeRPF, tableName, "listecle_def", " = ", cle_def, _
		  " AND ", "listecle_prem", " = ", cle_prem)
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = ListecleModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  // Remplacer par les nouvelles valeurs
		  Self.sListecle.listecle_def = cle_def
		  Self.sListecle.listecle_prem = cle_prem
		  For index As Integer = 0 To ListecleListBox.RowCount - 1
		    If ListecleListBox.Cell(index, 0) = &u221A Then
		      Self.sListecle.listecle_sec = ListecleListBox.CellTag(index,0)
		      messageErreur = Self.sListecle.writeDataWithoutId(Session.bdGroupeRPF, tableName, prefix)
		      If (messageErreur <> "Succes") Then
		        Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		        pointeurProbMsgModal.WLAlertLabel.Text = ListecleModule.ERREUR + " " + messageErreur
		        pointeurProbMsgModal.Show
		        Exit
		      End If
		      
		    End If
		  Next Index
		  
		  
		  // Signaler que la modif a été complétée
		  Dim ML6MajModalBox As New MajModal
		  ML6MajModalBox.Show
		  
		  // Faire comme si on avait appuyé sur le crayon une deuxième fois
		  modeModification = True
		  modListecleDetail
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modListecleDetail()
		  Select Case cle_extraction
		    
		  Case "employe"
		    
		    If Not modeModification Then  // On n'est pas en modification et on veut entrer en mode Modification
		      modeModification = True
		      ListecleListBox.ColumnCount = 2
		      ListecleListBox.ColumnWidths = "10%, 90%"
		      ListecleListBox.Heading(0) = &u221A
		      ListecleListBox.Heading(1) = ListecleModule.Nom
		      IVEnrListecle.Visible = True
		    Else
		      modeModification = False
		      ListecleListBox.Heading(1) = ""
		      ListecleListBox.ColumnCount = 1
		      ListecleListBox.ColumnWidths = "100%"
		      ListecleListBox.Heading(0) = ListecleModule.NOM
		      IVEnrListecle.Visible = False
		    End If
		    
		  Case "equipement"
		    
		    If Not modeModification Then  // On n'est pas en modification et on veut entrer en mode Modification
		      modeModification = True
		      ListecleListBox.ColumnCount = 3
		      ListecleListBox.ColumnWidths = "10%, 20%,70%"
		      ListecleListBox.Heading(0) = &u221A
		      ListecleListBox.Heading(1) = ListecleModule.UNITENO
		      ListecleListBox.Heading(2) = ListecleModule.NOM
		      IVEnrListecle.Visible = True
		    Else
		      modeModification = False
		      ListecleListBox.Heading(1) = ""
		      ListecleListBox.Heading(2) = ""
		      ListecleListBox.ColumnCount = 2
		      ListecleListBox.ColumnWidths = "20%,80%"
		      ListecleListBox.Heading(0) = ListecleModule.UNITENO
		      ListecleListBox.Heading(1) = ListecleModule.NOM
		      IVEnrListecle.Visible = False
		    End If
		    
		  End Select
		  
		  populateListecle
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateListecle()
		  
		  If Self.sListecle = Nil Then
		    Self.sListecle = new ListecleClass()
		  End If
		  
		  Dim listecleRS As RecordSet
		  listecleRS = Self.sListecle.loadDataByFieldCond(Session.bdGroupeRPF,  tableName, "listecle_def", " = ", cle_def, _
		  " AND ", "listecle_prem", " = ", cle_prem, "listecle_sec")
		  
		  listecleListBox.DeleteAllRows
		  If listecleRS = Nil Then Exit Sub
		  
		  // Si on est en mode affichage et qu'il n'y a aucun enregistrement, on sort
		  If Not modeModification And listecleRS.RecordCount <= 0 Then Exit Sub
		  
		  Select Case cle_extraction
		    
		  Case "employe"
		    Dim aNomCompletEmploye(), aNas(), aCaseACocher() As String
		    Dim nbreEmployesSelectionnes As Integer = listecleRS.RecordCount
		    // Si la liste n'est pas vide, la remplir
		    If nbreEmployesSelectionnes > 0 Then
		      For i As Integer = 1 To listecleRS.RecordCount
		        aNomCompletEmploye.Append(Self.sListecle.getDataByField(Session.bdGroupeRPF,  "employe", "employe_nom || ' ' || employe_prenom", "employe_nas", listecleRS.Field("listecle_sec").StringValue))
		        aNas.Append(listecleRS.Field("listecle_sec").StringValue)
		        aCaseACocher.Append(&u221A)
		        listecleRS.MoveNext
		      Next
		    End If
		    listecleRS.Close
		    
		    // Si on est en mode modification, ajouter les autres employés à la liste
		    If modeModification Then
		      listecleRS = Self.sListecle.loadData(Session.bdGroupeRPF,  "employe", "employe_nom", "employe_prenom")
		      
		      For i As Integer = 1 To listecleRS.RecordCount
		        If listecleRS.Field("employe_statut").StringValue = "I" Then Continue // Si c'est un employé inactif, on n'en tient pas compte
		        Dim nomCompletEmploye As String = listecleRS.Field("employe_nom").StringValue + " " + listecleRS.Field("employe_prenom").StringValue
		        Dim nomCompletEmployeIndex As Integer = aNomCompletEmploye.IndexOf(nomCompletEmploye)
		        If nomCompletEmployeIndex = -1 Then
		          aNomCompletEmploye.Append(nomCompletEmploye)
		          aNas.Append(listecleRS.Field("employe_nas").StringValue)
		          aCaseACocher.Append(&u25A1)
		        End If
		        listecleRS.MoveNext
		      Next
		    End If
		    listecleRS.Close
		    listecleRS = Nil
		    
		    // Trier la liste par rapport au nom complet
		    aNomCompletEmploye.SortWith(aNas, aCaseACocher)
		    
		    // Afficher la liste
		    // Si on est en mode Modification afficher la liste avec la case à cocher, sinon afficher seulement les noms
		    For i As Integer = 0 To Ubound(aNomCompletEmploye)
		      If Not modeModification Then
		        ListecleListBox.AddRow(aNomCompletEmploye(i))
		      Else
		        ListecleListBox.AddRow(aCaseACocher(i),aNomCompletEmploye(i))
		      End If
		      
		      // Garder le numéro de ligne
		      ListecleListBox.RowTag(ListecleListBox.LastIndex) = i
		      // Garder le NAS
		      ListecleListBox.CellTag(ListecleListBox.LastIndex,0) = aNas(i)
		      
		    Next I
		    
		  Case "equipement"
		    Dim aNomEquipement(), aUniteNo(), aCaseACocher() As String
		    Dim nbreEquipementsSelectionnes As Integer = listecleRS.RecordCount
		    // Si la liste n'est pas vide, la remplir
		    If nbreEquipementsSelectionnes > 0 Then
		      For i As Integer = 1 To listecleRS.RecordCount
		        aNomEquipement.Append(Self.sListecle.getDataByField(Session.bdGroupeRPF,  "equipm", _
		        "(CASE WHEN equipm_marque Is Null Then '' Else equipm_marque || ' ' END || " + _
		        " CASE WHEN equipm_modele Is Null Then ' ' Else equipm_modele || ' ' END ||" + _
		        " CASE WHEN equipm_annee    Is Null Then ' ' Else equipm_annee   || ' ' END ||" + _
		        " CASE WHEN equipm_couleur Is Null Then ' ' Else equipm_couleur || ' ' END ||" + _
		        " CASE WHEN equipm_moteur Is Null Then ' ' Else equipm_moteur  || ' ' END)" ,  _
		        "equipm_unite_no", listecleRS.Field("listecle_sec").StringValue))
		        aUniteNo.Append(listecleRS.Field("listecle_sec").StringValue)
		        aCaseACocher.Append(&u221A)
		        listecleRS.MoveNext
		      Next
		    End If
		    listecleRS.Close
		    
		    // Si on est en mode modification, ajouter les autres équipements à la liste
		    If modeModification Then
		      listecleRS = Self.sListecle.loadData(Session.bdGroupeRPF,  "equipm", "equipm_unite_no")
		      
		      For i As Integer = 1 To listecleRS.RecordCount
		        If listecleRS.Field("equipm_statut").StringValue = "I" Then Continue // Si c'est un équipement inactif, on n'en tient pas compte
		        Dim nomEquipement As String = Self.sListecle.getDataByField(Session.bdGroupeRPF,  "equipm", _
		        "(CASE WHEN equipm_marque Is Null Then '' Else equipm_marque || ' ' END || " + _
		        " CASE WHEN equipm_modele Is Null Then ' ' Else equipm_modele || ' ' END ||" + _
		        " CASE WHEN equipm_annee    Is Null Then ' ' Else equipm_annee   || ' ' END ||" + _
		        " CASE WHEN equipm_couleur Is Null Then ' ' Else equipm_couleur || ' ' END ||" + _
		        " CASE WHEN equipm_moteur Is Null Then ' ' Else equipm_moteur  || ' ' END)" ,  _
		        "equipm_unite_no", listecleRS.Field("equipm_unite_no").StringValue)
		        Dim nomEquipementIndex As Integer = aNomEquipement.IndexOf(nomEquipement)
		        If nomEquipementIndex = -1 Then
		          aNomEquipement.Append(nomEquipement)
		          aUniteNo.Append(listecleRS.Field("equipm_unite_no").StringValue)
		          aCaseACocher.Append(&u25A1)
		        End If
		        listecleRS.MoveNext
		      Next
		    End If
		    listecleRS.Close
		    listecleRS = Nil
		    
		    // Trier la liste par rapport au nom complet
		    aNomEquipement.SortWith(aUniteNo, aCaseACocher)
		    
		    // Afficher la liste
		    // Si on est en mode Modification afficher la liste avec la case à cocher, sinon afficher seulement les noms
		    For i As Integer = 0 To Ubound(aNomEquipement)
		      If Not modeModification Then
		        ListecleListBox.AddRow(aUniteNo(i),aNomEquipement(i))
		      Else
		        ListecleListBox.AddRow(aCaseACocher(i),aUniteNo(i),aNomEquipement(i))
		      End If
		      
		      // Garder le numéro de ligne
		      ListecleListBox.RowTag(ListecleListBox.LastIndex) = i
		      // Garder le NAS
		      ListecleListBox.CellTag(ListecleListBox.LastIndex,0) = aUniteNo(i)
		      
		    Next I
		    
		  End Select
		  
		  
		  // Positionnement à la ligne gardée en mémoire
		  //If ListecleListBox.ListIndex  <> listecleStruct.no_ligne And DocumentListBox.RowCount > 0 Then
		  //Dim v As Variant = ListecleListBox.LastIndex
		  //v = listecleStruct.no_ligne
		  //If listecleStruct.no_ligne > DocumentListBox.LastIndex  Then documentStruct.no_ligne = DocumentListBox.LastIndex
		  //ListecleListBox.ListIndex  = listecleStruct.no_ligne
		  //listecleStruct.id = DocumentListBox.CellTag(documentStruct.no_ligne,1)
		  //End If
		  
		  
		  // Si la liste n'est pas vide, bloquer la possibilité de changement du no. unité
		  //Session.pointeurEquipementContainer.equipm_unite_no_TextField.Enabled = True
		  //If ListecleListBox.RowCount > 0 Then
		  //Session.pointeurEquipementContainer.equipm_unite_no_TextField.Enabled = False
		  //End If
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub resetIV()
		  Self.IVEnrListecle.Picture = Enregistrer30x30
		  Self.IVModListecle.Picture = ModModal30x30
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		cle_def As String
	#tag EndProperty

	#tag Property, Flags = &h0
		cle_extraction As String
	#tag EndProperty

	#tag Property, Flags = &h0
		cle_prem As String
	#tag EndProperty

	#tag Property, Flags = &h0
		listecleStruct As listecleStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		modeModification As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		prefix As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sListecle As ListecleClass
	#tag EndProperty

	#tag Property, Flags = &h0
		tableName As String
	#tag EndProperty


	#tag Structure, Name = listecleStructure, Flags = &h0
		nom As String*100
		  id As Integer
		  no_ligne As Integer
		  tri as Integer
		edit_mode As String*30
	#tag EndStructure


#tag EndWindowCode

#tag Events ListecleListBox
	#tag Event
		Sub SelectionChanged()
		  
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    If modeModification Then
		      // Si non coché, Marquer à coché
		      If Me.Cell(Me.ListIndex,0) = &u25A1 Then
		        Me.Cell(Me.ListIndex,0) =&u221A
		      Else  // Sinon, Marquer à non coché
		        Me.Cell(Me.ListIndex,0) = &u25A1
		      End If
		    End If
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModListecle
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVModListecle.Picture = ModModalOrange30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  modListecleDetail
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVEnrListecle
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVEnrListecle.Picture = EnregistrerOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  majListecleDetail
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="cle_def"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="cle_extraction"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="cle_prem"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="modeModification"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="prefix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tableName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
