#tag WebStyle
WebStyle StylePickerDayNr_Selected
Inherits WebStyle
	#tag WebStyleStateGroup
		text-align=center
		text-decoration=True false false false false
		border-top=1px solid 8C8C8CFF
		border-left=1px solid 8C8C8CFF
		border-bottom=1px solid CCCCCCFF
		border-right=1px solid CCCCCCFF
		misc-background=gradient vertical 0 64AAEDFF 1 005DDAFF
		text-size=10px
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=solid 5676A7FF
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 F5F5F5FF 1 2B2B2BFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StylePickerDayNr_Selected
#tag EndWebStyle

