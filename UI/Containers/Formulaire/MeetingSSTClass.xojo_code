#tag Class
Protected Class MeetingSSTClass
Inherits AllFormulaireClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdGroupeRPF, "fss0023_meeting_sst_journ", "fss0023")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(formulaireDB As PostgreSQLDatabase, ByRef formulaire_tri_param As WebTextField, ByRef formulaire_cle_param As WebTextField, ByRef formulaire_desc_fr_param As WebTextField, ByRef formulaire_desc_en_param As WebTextField) As String
		  // Couper les zones à leur longeur maximum dans la BD
		  
		  formulaire_cle_param.text =  Left(formulaire_cle_param.text,30)
		  formulaire_desc_fr_param.text = Left(formulaire_desc_fr_param.text,100)
		  formulaire_desc_en_param.text = Left(formulaire_desc_en_param.text,100)
		  
		  // Vérification des zones obligatoires
		  If formulaire_tri_param.Text = "" Then formulaire_tri_param.Style = TextFieldErrorStyle
		  If formulaire_cle_param.Text = "" Then formulaire_cle_param.Style = TextFieldErrorStyle
		  If formulaire_desc_fr_param.Text = "" Then formulaire_desc_fr_param.Style = TextFieldErrorStyle
		  If formulaire_desc_en_param.Text = "" Then formulaire_desc_en_param.Style = TextFieldErrorStyle
		  
		  If formulaire_tri_param.Style = TextFieldErrorStyle Or formulaire_cle_param.Style = TextFieldErrorStyle Or formulaire_desc_fr_param.Style = TextFieldErrorStyle Or formulaire_desc_en_param.Style = TextFieldErrorStyle  Then
		    Return FormulaireModule.ZONESOBLIGATOIRES
		  End If
		  
		  // Vérification des zones numériques
		  If formulaire_tri_param.text <> "" And  IsNumeric(formulaire_tri_param.text) = False Then formulaire_tri_param.Style = TextFieldErrorStyle
		  
		  If formulaire_tri_param.Style = TextFieldErrorStyle Then
		    Return FormulaireModule.ZONEDOITETRENUMERIQUE
		  End If
		  
		  formulaire_tri_param .text = str(Format( Val(formulaire_tri_param .text), "0"))
		  
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		fss0023_canal As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_employe_present_nas_1 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_employe_present_nas_2 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_employe_present_nas_3 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_employe_present_nas_4 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_employe_present_nas_5 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_employe_present_nas_6 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_entry_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_epi As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_heure_debut As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_heure_fin As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_inspection_vehicule As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_outils As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_personnel As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_produits As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_responsable_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_revision As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_sujet_meteo As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_tel_urgence As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_tours As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_type_travaux As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0023_vehicules_carburant As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="formulaire_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_form_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_canal"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_employe_present_nas_1"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_employe_present_nas_2"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_employe_present_nas_3"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_employe_present_nas_4"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_employe_present_nas_5"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_employe_present_nas_6"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_entry_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_epi"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_heure_debut"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_heure_fin"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_inspection_vehicule"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_outils"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_personnel"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_produits"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_responsable_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_sujet_meteo"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_tel_urgence"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_tours"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_type_travaux"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0023_vehicules_carburant"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
