#tag Module
Protected Module FormulaireModule
	#tag Constant, Name = ACCIDENT, Type = String, Dynamic = True, Default = \"Accident", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Accident"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Accident"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Accident"
	#tag EndConstant

	#tag Constant, Name = ACCOMPAGNEMENTEOLIENNE, Type = String, Dynamic = True, Default = \"Accompagnement \xC3\xA9olienne", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Accompagnement \xC3\xA9olienne"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Accompanying in a tower"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Accompagnement \xC3\xA9olienne"
	#tag EndConstant

	#tag Constant, Name = AUTRECOMMENTAIRE, Type = String, Dynamic = True, Default = \"Autre commentaire de visite", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Autre commentaire de visite"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Other visit comment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Autre commentaire de visite"
	#tag EndConstant

	#tag Constant, Name = AUTRESCOMMENTAIRESTTRAVAUX, Type = String, Dynamic = True, Default = \"Autres commentaires sur les travaux", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Autres commentaires sur les travaux"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Other comments on achieved work"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Autres commentaires sur les travaux"
	#tag EndConstant

	#tag Constant, Name = AVISNONCONFORMITE, Type = String, Dynamic = True, Default = \"Avis de non-conformit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Avis de non-conformit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Notice of nonconformity"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Avis de non-conformit\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = CANALRADIO, Type = String, Dynamic = True, Default = \"Canal radio", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Canal radio"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Radio channel"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Canal radio"
	#tag EndConstant

	#tag Constant, Name = CHEFCHANTIER, Type = String, Dynamic = True, Default = \"Chef de chantier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Chef de chantier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project leader"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chef de chantier"
	#tag EndConstant

	#tag Constant, Name = CLIENT, Type = String, Dynamic = True, Default = \"Client", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Client"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Client"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Client"
	#tag EndConstant

	#tag Constant, Name = COMMENTAIRE, Type = String, Dynamic = True, Default = \"Commentaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Commentaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Comment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Commentaire"
	#tag EndConstant

	#tag Constant, Name = COMMENTAIREGLOBAL, Type = String, Dynamic = True, Default = \"Commentaire global", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Commentaire global"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Global comment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Commentaire global"
	#tag EndConstant

	#tag Constant, Name = CONTROLEQUALITE, Type = String, Dynamic = True, Default = \"Controle de qualit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Controle de qualit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Quality control"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Controle de qualit\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = False, Default = \"form", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = True, Default = \"paramequip", Scope = Public
	#tag EndConstant

	#tag Constant, Name = DEFIBRILLATEUR, Type = String, Dynamic = True, Default = \"D\xC3\xA9fibrillateur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9fibrillateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Defibrillator"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9fibrillateur"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTATIONSITECOMPLETEE, Type = String, Dynamic = True, Default = \"Documentation de site compl\xC3\xA9t\xC3\xA9e et envoy\xC3\xA9e", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Documentation de site compl\xC3\xA9t\xC3\xA9e et envoy\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Site documentation filled and sent"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Documentation de site compl\xC3\xA9t\xC3\xA9e et envoy\xC3\xA9e"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTSREMPLIS, Type = String, Dynamic = True, Default = \"Documents remplis", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Documents remplis"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Filled documents"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Documents remplis"
	#tag EndConstant

	#tag Constant, Name = ELINGUES, Type = String, Dynamic = True, Default = \"\xC3\x89lingues", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89lingues"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89lingues"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Slings"
	#tag EndConstant

	#tag Constant, Name = EMPLOYESPRESENTS, Type = String, Dynamic = True, Default = \"Employ\xC3\xA9s pr\xC3\xA9sents", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Employ\xC3\xA9s pr\xC3\xA9sents"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Employ\xC3\xA9s pr\xC3\xA9sents"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Present employee"
	#tag EndConstant

	#tag Constant, Name = ENREGNEPEUTPASETREDETRUIT, Type = String, Dynamic = True, Default = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record cannot be deleted. It is still part of a database record."
	#tag EndConstant

	#tag Constant, Name = ENTRETIENEMPLACEMENTSFIXES, Type = String, Dynamic = True, Default = \"Entretien emplacements fixes", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Entretien emplacements fixes"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Fixed location maintenance"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Entretien emplacements fixes"
	#tag EndConstant

	#tag Constant, Name = EPI, Type = String, Dynamic = True, Default = \"EPI inspect\xC3\xA9 et disponible", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"EPI inspect\xC3\xA9 et disponible"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"EPI inspected and available"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"EPI inspect\xC3\xA9 et disponible"
	#tag EndConstant

	#tag Constant, Name = EQUIPEMENTOUTILLAGE, Type = String, Dynamic = True, Default = \"\xC3\x89quipements et outillage", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89quipements et outillage"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89quipements et outillage"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment and tool"
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = FEUILLESECURITECOMPLETE, Type = String, Dynamic = True, Default = \"Feuilles de s\xC3\xA9curit\xC3\xA9 compl\xC3\xA9t\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Feuilles de s\xC3\xA9curit\xC3\xA9 compl\xC3\xA9t\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Security sheet filled"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Feuilles de s\xC3\xA9curit\xC3\xA9 compl\xC3\xA9t\xC3\xA9es"
	#tag EndConstant

	#tag Constant, Name = FORMATION, Type = String, Dynamic = True, Default = \"Formation", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Formation"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Training"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Formation"
	#tag EndConstant

	#tag Constant, Name = FORMULAIRE, Type = String, Dynamic = True, Default = \"Formulaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Formulaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Form"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Formulaire"
	#tag EndConstant

	#tag Constant, Name = FORMULAIREPREFIX, Type = String, Dynamic = True, Default = \"formulaire", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FORMULAIRETABLENAME, Type = String, Dynamic = True, Default = \"formulaire", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HARNAIS, Type = String, Dynamic = True, Default = \"Harnais", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Harnais"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Harness"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Harnais"
	#tag EndConstant

	#tag Constant, Name = HEUREDEBUT, Type = String, Dynamic = True, Default = \"Heure d\xC3\xA9but", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Heure d\xC3\xA9but"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Beginning hour"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Heure d\xC3\xA9but"
	#tag EndConstant

	#tag Constant, Name = HEUREFIN, Type = String, Dynamic = True, Default = \"Heure fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Heure fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ending hour"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Heure fin"
	#tag EndConstant

	#tag Constant, Name = HINTADD, Type = String, Dynamic = True, Default = \"Ajouter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
	#tag EndConstant

	#tag Constant, Name = HINTLOCKUNLOCK, Type = String, Dynamic = True, Default = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lock/Unlock record"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
	#tag EndConstant

	#tag Constant, Name = HINTSUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
	#tag EndConstant

	#tag Constant, Name = HINTUPDATEDETAILS, Type = String, Dynamic = True, Default = \"Enregistrer les modifications.", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrer les modifications."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save modifications."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrer les modifications."
	#tag EndConstant

	#tag Constant, Name = INSPECTIONHEBDOMADAIRE, Type = String, Dynamic = True, Default = \"Inspection hebdomadaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inspection hebdomadaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Weekly inspection"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspection hebdomadaire"
	#tag EndConstant

	#tag Constant, Name = INSPECTIONVEHICULE, Type = String, Dynamic = True, Default = \"Inspection des v\xC3\xA9hicules", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inspection des v\xC3\xA9hicules"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Vehicles inspection"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspection des v\xC3\xA9hicules"
	#tag EndConstant

	#tag Constant, Name = INTERVENTIONEMPLOYE, Type = String, Dynamic = True, Default = \"Intervention et rencontre d\'un employ\xC3\xA9 pour raison de s\xC3\xA9curit\xC3\xA9 ou autres", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Intervention et rencontre d\'un employ\xC3\xA9 pour raison de s\xC3\xA9curit\xC3\xA9 ou autres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Intervening and meeting with an employee for security reason"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Intervention et rencontre d\'un employ\xC3\xA9 pour raison de s\xC3\xA9curit\xC3\xA9 ou autres"
	#tag EndConstant

	#tag Constant, Name = KITARCFLASH, Type = String, Dynamic = True, Default = \"Kit arc flash", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Kit arc flash"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Kit arc flash"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Kit arc flash"
	#tag EndConstant

	#tag Constant, Name = KITSAUVETAGEHAUTEUR, Type = String, Dynamic = True, Default = \"Kit sauvetage en hauteur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Kit sauvetage en hauteur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Height safety kit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Kit sauvetage en hauteur"
	#tag EndConstant

	#tag Constant, Name = LIFTBAGS, Type = String, Dynamic = True, Default = \"Sacs de levage", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sacs de levage"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lift bags"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sacs de levage"
	#tag EndConstant

	#tag Constant, Name = LISTEPIECESADATE, Type = String, Dynamic = True, Default = \"Liste de pi\xC3\xA8ces pris \xC3\xA0 date", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Liste de pi\xC3\xA8ces pris \xC3\xA0 date"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Parts list up to date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Liste de pi\xC3\xA8ces pris \xC3\xA0 date"
	#tag EndConstant

	#tag Constant, Name = MEETINGSST, Type = String, Dynamic = True, Default = \"Meeting SST journalier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Meeting SST journalier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SST daily meeting"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Meeting SST journalier"
	#tag EndConstant

	#tag Constant, Name = NAS, Type = String, Dynamic = True, Default = \"No. Assurance sociale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. Assurance sociale"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Social Insurance number"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. Assurance sociale"
	#tag EndConstant

	#tag Constant, Name = NOMFORMULAIRE, Type = String, Dynamic = True, Default = \"Nom du formulaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom du formulaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Form name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom du formulaire"
	#tag EndConstant

	#tag Constant, Name = NOMPROJET, Type = String, Dynamic = True, Default = \"Nom du projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom du projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom du projet"
	#tag EndConstant

	#tag Constant, Name = NON, Type = String, Dynamic = True, Default = \"Non", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Non"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Non"
	#tag EndConstant

	#tag Constant, Name = OUI, Type = String, Dynamic = True, Default = \"Oui", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Oui"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Yes"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Oui"
	#tag EndConstant

	#tag Constant, Name = OUTILS, Type = String, Dynamic = True, Default = \"Assur\xC3\xA9 d\'avoir les bons outils pour faire le travail", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Assur\xC3\xA9 d\'avoir les bons outils pour faire le travail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ensure to have good tools to make the work"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Assur\xC3\xA9 d\'avoir les bons outils pour faire le travail"
	#tag EndConstant

	#tag Constant, Name = PERSONNEL, Type = String, Dynamic = True, Default = \"Personnel pr\xC3\xAAt au travail\x2C bonne sant\xC3\xA9 physique et mentale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Personnel pr\xC3\xAAt au travail\x2C bonne sant\xC3\xA9 physique et mentale"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Staff ready to work\x2C good physical and mental health"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Personnel pr\xC3\xAAt au travail\x2C bonne sant\xC3\xA9 physique et mentale"
	#tag EndConstant

	#tag Constant, Name = PHOTOSPROBTOURSREMIS, Type = String, Dynamic = True, Default = \"Photos de probl\xC3\xA8mes des tours remis", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Photos de probl\xC3\xA8mes des tours remis"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Photos problems on towers delivered"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Photos de probl\xC3\xA8mes des tours remis"
	#tag EndConstant

	#tag Constant, Name = PRODUITS, Type = String, Dynamic = True, Default = \"Produit identifi\xC3\xA9 dans les contenants utilis\xC3\xA9s", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Produit identifi\xC3\xA9 dans les contenants utilis\xC3\xA9s"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Product identified in used containers"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Produit identifi\xC3\xA9 dans les contenants utilis\xC3\xA9s"
	#tag EndConstant

	#tag Constant, Name = RAPPORTHEBDO, Type = String, Dynamic = True, Default = \"Rapport hebdomadaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rapport hebdomadaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Weekly report"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rapport hebdomadaire"
	#tag EndConstant

	#tag Constant, Name = RAPPORTSECURITE, Type = String, Dynamic = True, Default = \"Rapport sur la s\xC3\xA9curit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rapport sur la s\xC3\xA9curit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Security report"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rapport sur la s\xC3\xA9curit\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = RAPPORTVISITECHANTIER, Type = String, Dynamic = True, Default = \"Rapport visite de chantier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rapport visite de chantier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project visit report"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rapport visite de chantier"
	#tag EndConstant

	#tag Constant, Name = REMORQUE, Type = String, Dynamic = True, Default = \"Remorque", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Remorque"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Trailer"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Remorque"
	#tag EndConstant

	#tag Constant, Name = RESPONSABLE, Type = String, Dynamic = True, Default = \"Responsable", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Responsable"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Responsible"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Responsable"
	#tag EndConstant

	#tag Constant, Name = RESPONSABLEEHS, Type = String, Dynamic = True, Default = \"Responsable EHS", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Responsable EHS"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"EHS responsible"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Responsable EHS"
	#tag EndConstant

	#tag Constant, Name = REVISE, Type = String, Dynamic = True, Default = \"R\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = SEMAINEFINISSANTLE, Type = String, Dynamic = True, Default = \"Semaine finissant le", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Semaine finissant le"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Semaine finissant le"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Week ending on"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = SUJETJOUR, Type = String, Dynamic = True, Default = \"Sujet du jour et condition m\xC3\xA9t\xC3\xA9orologique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sujet du jour et condition m\xC3\xA9t\xC3\xA9orologique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sujet du jour et condition m\xC3\xA9t\xC3\xA9orologique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Daily subject and weather condition"
	#tag EndConstant

	#tag Constant, Name = TELURGENCE, Type = String, Dynamic = True, Default = \"Tel. urgence", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Tel. urgence"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Emerg. tel."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Tel. urgence"
	#tag EndConstant

	#tag Constant, Name = TOTALHEURESFACTURABLESSEMAINE, Type = String, Dynamic = True, Default = \"Total des heures hommes cette semaine facturables", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Total des heures hommes cette semaine facturables"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Total manhours to be billed this week"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Total des heures hommes cette semaine facturables"
	#tag EndConstant

	#tag Constant, Name = TOTALTOURSCOMPLETES, Type = String, Dynamic = True, Default = \"Total des tours compl\xC3\xA9t\xC3\xA9s", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Total des tours compl\xC3\xA9t\xC3\xA9s"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Total towers completed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Total des tours compl\xC3\xA9t\xC3\xA9s"
	#tag EndConstant

	#tag Constant, Name = TOTALTOURSCOMPLETESEMAINE, Type = String, Dynamic = True, Default = \"Total des tours compl\xC3\xA9t\xC3\xA9s cette semaine", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Total des tours compl\xC3\xA9t\xC3\xA9s cette semaine"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Total towers completed this week"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Total des tours compl\xC3\xA9t\xC3\xA9s cette semaine"
	#tag EndConstant

	#tag Constant, Name = TOURS, Type = String, Dynamic = True, Default = \"Tours", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Tours"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Towers"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Tours"
	#tag EndConstant

	#tag Constant, Name = TROUSSEPELICAN, Type = String, Dynamic = True, Default = \"Trousse P\xC3\xA9lican", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Trousse P\xC3\xA9lican"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pelican kit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Trousse P\xC3\xA9lican"
	#tag EndConstant

	#tag Constant, Name = TROUSSEPREMIERSSOINS, Type = String, Dynamic = True, Default = \"Trousse de premiers soins", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Trousse de premiers soins"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"First-aid kit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Trousse de premiers soins"
	#tag EndConstant

	#tag Constant, Name = TROUSSESSOSEXTINCTEURS, Type = String, Dynamic = True, Default = \"Trousse SOS et Extincteurs", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Trousse SOS et Extincteur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Fire extinguisher"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Trousse SOS et Extincteur"
	#tag EndConstant

	#tag Constant, Name = TYPETRAVAUX, Type = String, Dynamic = True, Default = \"Type de travaux", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type de travaux"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Work type"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type de travaux"
	#tag EndConstant

	#tag Constant, Name = VEHICULECARBURANT, Type = String, Dynamic = True, Default = \"V\xC3\xA9hicules plein de carburant", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"V\xC3\xA9hicules plein de carburant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Vehicle full of gazoline"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"V\xC3\xA9hicules plein de carburant"
	#tag EndConstant

	#tag Constant, Name = VERIFHEBDOCHEFCHANTIER, Type = String, Dynamic = True, Default = \"V\xC3\xA9rification hebdo chef de chantier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"V\xC3\xA9rification hebdo chef de chantier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Plant manager weekly verification"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"V\xC3\xA9rification hebdo chef de chantier"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant

	#tag Constant, Name = ZONESOBLIGATOIRES, Type = String, Dynamic = True, Default = \"Required field(s)", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required field(s)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Zone(s) obligatoire(s)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required field(s)"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
