#tag WebPage
Begin WebContainer FormulaireSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   585
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   320
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebListBox FormulaireListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   3
      ColumnWidths    =   "30%,50%,20%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "1065947135"
      Height          =   530
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Date	#FormulaireModule.NOMFORMULAIRE	#FormulaireModule.REVISE	"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "1065947135"
      Style           =   "1065947135"
      TabOrder        =   -1
      Top             =   55
      VerticalCenter  =   0
      Visible         =   True
      Width           =   320
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel FormulaireLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddFormulaire
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#FormulaireModule.HINTADD"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   208
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1401872383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelFormulaire
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#FormulaireModule.HINTSUPPRIMER"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   242
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   415664127
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVEnrFormulaire
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#FormulaireModule.HINTUPDATEDETAILS"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   158
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1113270271
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "#FormulaireModule.FORMULAIRE"
      TextAlign       =   0
      Top             =   12
      VerticalCenter  =   0
      Visible         =   True
      Width           =   138
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModFormulaire
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#FormulaireModule.HINTLOCKUNLOCK"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1238587391
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVRafFormulaire
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   282
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   2017697791
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ajouterFormulaire()
		  Dim compteur As Integer
		  compteur = FormulaireListBox.RowCount  'Pour Debug
		  compteur = FormulaireListBox.LastIndex    'Pour Debug
		  compteur = FormulaireListBox.ListIndex ' Pour Debug
		  
		  //FormulaireListBox.insertRow(compteur+1, FormulaireModule.NOUVEAUNAS, FormulaireModule.NOUVEAUNOM, FormulaireModule.NOUVEAUPRENOM, "A")
		  
		  // Faire pointer le curseur sur le nouveal formulaire
		  FormulaireListBox.ListIndex = FormulaireListBox.ListIndex + 1
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne = FormulaireListBox.ListIndex
		  
		  //Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.nas = FormulaireModule.NOUVEAUNAS
		  //Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.nom = FormulaireModule.NOUVEAUNOM
		  //Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.prenom = FormulaireModule.NOUVEAUPRENOM
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.revision = ""
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement
		  Self.sAllFormulaire = Nil
		  //Self.sFormulaire = new FormulaireClass()
		  //Self.sFormulaire.formulaire_nas = FormulaireModule.NOUVEAUNAS
		  //Self.sFormulaire.formulaire_form_id = FormulaireModule.NOUVEAUNOM
		  //Self.sFormulaire.formulaire_prenom = FormulaireModule.NOUVEAUPRENOM
		  //Self.sFormulaire.formulaire_statut = "A"
		  // formulaire_cleetr_nom et formulaire_cleetr_cle vont prendre la valeur de la ligne précédente
		  
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id = 0  // indique que c'est un nouvel enregistrement
		  //Self.sFormulaire.formulaire_id = 0
		  //Session.pointeurFormulaireContainerControl.FormulaireContainerDialog.majFormulaireDetail
		  
		  // Renumérotation de la listbox et de la table paramètre
		  //renumerotation
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  //populateFormulaire(Self.sFormulaire.formulaire_id)
		  
		  // Se mettre en mode lock
		  //implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementFormulaire(idFormulaire_param As Integer)
		  // Rendre invisible les informations de blocage
		  Session.pointeurFormulaireContainerControl.IVLockedFormulaire.Visible = False
		  Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Text = ""
		  Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Visible = False
		  
		  // Si on est en verrouillage et qu'il y a changement d'enregistrement, on remet les icones pertinents actifs et on bloque l'écran Container 
		  If Self.sAllFormulaire.lockEditMode = True Then
		    Self.sAllFormulaire.lockEditMode = False
		    // Enlever les verrouillages existants
		    Dim message As String = Self.sAllFormulaire.cleanUserLocks(Session.bdGroupeRPF)
		    If  message <> "Succes" Then afficherMessage(message)
		    
		    // Rendre disponible l'ajout et la suppression
		    IVModFormulaire.Picture = ModModal30x30
		    //IVAddFormulaire.Visible = True
		    //IVAddFormulaire.Enabled = True
		    IVDelFormulaire.Visible = True
		    IVDelFormulaire.Enabled = True
		    IVEnrFormulaire.Visible = False
		    IVEnrFormulaire.Enabled = False
		  End If
		  
		  // Si la valeur de idFormulaire est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  //If idFormulaire_param  = 0 Then
		  //Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.edit_mode = "Creation"
		  //Exit Sub
		  //End If
		  
		  //Déterminer quel formulaire il faut afficher
		  Dim formulaireAafficher As String = FormulaireListBox.CellTag(idFormulaire_param,2)
		  
		  // Restaurer la valeur de la clé du record 
		  allFormulaireStruct.id = FormulaireListBox.CellTag(idFormulaire_param,1)
		  
		  // Libérer l'espace mémoire si c'est le cas
		  If Session.pointeurAllFormulaireContainer <> Nil Then
		    Session.pointeurAllFormulaireContainer.Close
		    Session.pointeurAllFormulaireContainer = Nil
		  End If
		  
		  Dim hauteurPanneauFormulaireSideBar As Integer = Me.Height
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function genClauseWhere(listeSelection_param As Dictionary) As String
		  
		  Dim listeSelection As String = ""
		  Dim keys(), k, v as variant
		  
		  keys = listeSelection_param.keys()
		  for each k in keys
		    Select Case k
		      
		    Case "DateDebut"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and formulaire_date >= '" + v + "'  "
		      
		    Case "DateFin"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and formulaire_date <= '" + v + "'  "
		      
		    Case "Projet"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and formulaire_projet IN('" + v + "')  "
		      
		    Case "Revision"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and formulaire_revision IN(" + v + ")  "
		      
		    End Select
		    
		  Next
		  
		  Return listeSelection
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub implementLock()
		  //Déterminer sur quel formulaire on est
		  Dim formulaireActuel As String = FormulaireListBox.CellTag(FormulaireListBox.ListIndex,2)
		  Dim dBSchemaName As String = "form"
		  Dim dBTableName As String = ""
		  
		  Select Case formulaireActuel
		  Case "9FA6D6AE78" // Formulaire Meeting SST Journalier
		    dBTableName = "fss0023_meeting_sst_journ"
		  Case "46B73282F9" // Formulaire Vérification hebdomadaire chef de chantier
		    dBTableName = "fss0006_verif_hebdo_chef_chantier"
		  Case "C56E5F0670" // Formulaire Rapport visite de chantier
		    dBTableName = "rop0008_rapport_visite_chantier"
		  Case "9004B3B2CC" // Formulaire Rapport hebdomadaire
		    dBTableName = "rop0009_rapport_hebdomadaire"
		  End Select 
		  
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = dBSchemaName + "." + dBTableName + "." + str(FormulaireListBox.CellTag(FormulaireListBox.ListIndex,1))
		  Message = Self.sAllFormulaire.check_If_Locked(Session.bdGroupeRPF, param_recherche)
		  If  Message <> "Locked" And Message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    Session.pointeurFormulaireContainerControl.IVLockedFormulaire.Visible = True
		    Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Text = Self.sAllFormulaire.lockedBy
		    Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Visible = True
		    Exit Sub
		  End If
		  
		  //Déverrouiller l'enregistrement si on est actuellement en Verrouillage
		  If Self.sAllFormulaire.lockEditMode = True Then
		    message = Self.sAllFormulaire.unLockRow(Session.bdGroupeRPF)
		    If  message <> "Succes" Then
		      afficherMessage(message)
		      Exit Sub
		    End If
		    IVModFormulaire.Picture = ModModal30x30
		    // Rendre disponible l'ajout et la suppression
		    //IVAddFormulaire.Visible = True
		    //IVAddFormulaire.Enabled = True
		    IVDelFormulaire.Visible = True
		    IVDelFormulaire.Enabled = True
		    // Libérer 
		    Session.pointeurFormulaireContainerControl.IVLockedFormulaire.Visible = False
		    Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Visible = False
		    IVEnrFormulaire.Visible = False
		    IVEnrFormulaire.Enabled = False
		    // Bloquer les zones du container
		    Self.sAllFormulaire.lockEditMode = False
		    changementFormulaire(FormulaireListBox.RowTag(FormulaireListBox.ListIndex))
		    //Session.pointeurFormulaireContainerControl.FormulaireContainerDialog.listFormulaireDetail
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sAllFormulaire.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sAllFormulaire.recordID =dBSchemaName + "." + dBTableName + "." + str(FormulaireListBox.CellTag(FormulaireListBox.ListIndex,1))
		  message = Self.sAllFormulaire.lockRow(Session.bdGroupeRPF) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  Session.pointeurFormulaireContainerControl.IVLockedFormulaire.Visible = False
		  Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Visible = False
		  IVEnrFormulaire.Enabled = Self.sAllFormulaire.recordAvailable
		  
		  //Activer/désactiver les fonctions pertinentes
		  IVModFormulaire.Picture = ModModalOrange30x30
		  IVAddFormulaire.Visible = False
		  IVAddFormulaire.Enabled = False
		  IVDelFormulaire.Visible = False
		  IVDelFormulaire.Enabled = False
		  IVEnrFormulaire.Visible = True
		  IVEnrFormulaire.Enabled = True
		  
		  // Débloquer les zones du container
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function loadFormulaire(dB As PostgreSQLDatabase, listeSelection_param AS Dictionary) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  // Génération de la clause Where
		  Dim clauseWhere AS  String = genClauseWhere(listeSelection_param)
		  Dim clauseWhereFss0023 AS  String = ""
		  Dim clauseWhereFss0006 AS  String = ""
		  Dim clauseWhereRop0008 AS  String = ""
		  Dim clauseWhereRop0009 AS  String = ""
		  
		  If clauseWhere <> "" Then
		    // Ajout du Where et enlever le premier And
		    clauseWhere = " WHERE " + mid(clauseWhere,4)
		    clauseWhereFss0023 = ReplaceAll(clauseWhere,"formulaire", "fss0023")
		    clauseWhereFss0006 = ReplaceAll(clauseWhere,"formulaire", "fss0006")
		    clauseWhereRop0008 = ReplaceAll(clauseWhere,"formulaire", "rop0008")
		    clauseWhereRop0009 = ReplaceAll(clauseWhere,"formulaire", "rop0009")
		  End If
		  
		  strSQL = "SELECT fss0023_id AS formulaire_id, fss0023_date AS formulaire_date, form_id AS formulaire_form_id, fss0023_projet AS formulaire_projet, fss0023_revision AS formulaire_revision, " + _ 
		  "(SELECT parametre_desc_fr  FROM parametre WHERE parametre_nom = 'formgestionprojet'   AND form_id = parametre_cle LIMIT 1)  AS formulaire_form_description,  " + _
		  "(SELECT projet_no || ' ' || projet_titre  FROM projet where fss0023_projet = projet_no LIMIT 1)  AS formulaire_projet_description, " + _
		  "(CASE WHEN fss0023_revision = '2' Then '' ELSE '" + FormulaireModule.REVISE + "'  END)  As formulaire_revision_description " +_
		  "FROM fss0023_meeting_sst_journ " + _
		  clauseWhereFss0023 + _
		  " UNION " + _
		  "SELECT fss0006_id AS formulaire_id, fss0006_date AS formulaire_date, form_id AS formulaire_form_id, fss0006_projet AS formulaire_projet, fss0006_revision AS formulaire_revision," + _ 
		  "(SELECT parametre_desc_fr  FROM parametre WHERE parametre_nom = 'formgestionprojet'   AND form_id = parametre_cle LIMIT 1)  AS formulaire_form_description,  " + _
		  "(SELECT projet_no || ' ' || projet_titre  FROM projet where fss0006_projet = projet_no LIMIT 1)  AS formulaire_projet_description, " + _
		  "(CASE WHEN fss0006_revision = '2' Then '' ELSE '" + FormulaireModule.REVISE + "'  END)  As formulaire_revision_description " +_
		  "FROM fss0006_verif_hebdo_chef_chantier "+ _
		  clauseWhereFss0006 + _
		  " UNION " + _
		  "SELECT rop0008_id AS formulaire_id, rop0008_date AS formulaire_date, form_id AS formulaire_form_id, rop0008_projet AS formulaire_projet, rop0008_revision AS formulaire_revision," + _ 
		  "(SELECT parametre_desc_fr  FROM parametre WHERE parametre_nom = 'formgestionprojet'   AND form_id = parametre_cle LIMIT 1)  AS formulaire_form_description,  " + _
		  "(SELECT projet_no || ' ' || projet_titre  FROM projet where rop0008_projet = projet_no LIMIT 1)  AS formulaire_projet_description, " + _
		  "(CASE WHEN rop0008_revision = '2' Then '' ELSE '" + FormulaireModule.REVISE + "'  END)  As formulaire_revision_description " +_
		  "FROM rop0008_rapport_visite_chantier "+ _
		  clauseWhereRop0008 + _
		  " UNION " + _
		  "SELECT rop0009_id AS formulaire_id, rop0009_date AS formulaire_date, form_id AS formulaire_form_id, rop0009_projet AS formulaire_projet, rop0009_revision AS formulaire_revision," + _ 
		  "(SELECT parametre_desc_fr  FROM parametre WHERE parametre_nom = 'formgestionprojet'   AND form_id = parametre_cle LIMIT 1)  AS formulaire_form_description,  " + _
		  "(SELECT projet_no || ' ' || projet_titre  FROM projet where rop0009_projet = projet_no LIMIT 1)  AS formulaire_projet_description, " + _
		  "(CASE WHEN rop0009_revision = '2' Then '' ELSE '" + FormulaireModule.REVISE + "'  END)  As formulaire_revision_description " +_
		  "FROM rop0009_rapport_hebdomadaire "+ _
		  clauseWhereRop0009 + _
		  " ORDER BY formulaire_projet_description, formulaire_date "
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modFormulaireDetail()
		  Dim formulaireActuel As String = FormulaireListBox.CellTag(FormulaireListBox.ListIndex,2)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateFormulaire(Optional formulaire_no_ligne_param as Integer)
		  
		  If Self.sAllFormulaire = Nil Then
		    // Par défaut, on donne le formulaire FSS0023 pour initialiser la classe AllFormulaire
		    Self.sAllFormulaire = new MeetingSSTClass()
		  End If
		  
		  
		  Dim listeSelection As Dictionary = Session.pointeurFormulaireContainerControl.FormulaireCritereContainerControl.chargementListeSelection
		  
		  Dim formulaireRS As RecordSet
		  formulaireRS = loadFormulaire(Session.bdGroupeRPF, listeSelection)
		  
		  FormulaireListBox.DeleteAllRows
		  // Ne pas afficher le panneau de description détaillée du formulaire 
		  If formulaireRS = Nil Then 
		    If Session.pointeurAllFormulaireContainer <> Nil Then
		      Session.pointeurAllFormulaireContainer.Close
		      Session.pointeurAllFormulaireContainer  = Nil
		    End If
		    IVModFormulaire.Visible = False
		    IVDelFormulaire.Visible = False
		    Exit Sub
		  End If
		  If formulaireRS.RecordCount <= 0 Then 
		    If Session.pointeurAllFormulaireContainer <> Nil Then
		      Session.pointeurAllFormulaireContainer.Close
		      Session.pointeurAllFormulaireContainer  = Nil
		    End If
		    IVModFormulaire.Visible = False
		    IVDelFormulaire.Visible = False
		    Exit Sub
		  End If
		  
		  IVModFormulaire.Visible = True
		  IVDelFormulaire.Visible = True
		  
		  For i As Integer = 1 To formulaireRS.RecordCount
		    FormulaireListBox.AddRow(formulaireRS.Field("formulaire_date").StringValue, _
		    formulaireRS.Field("formulaire_form_description").StringValue, formulaireRS.Field("formulaire_revision_description").StringValue)
		    
		    // Garder le numéro de ligne
		    FormulaireListBox.RowTag(FormulaireListBox.LastIndex) = i -1
		    // Garder l'id du formulaire
		    FormulaireListBox.CellTag(FormulaireListBox.LastIndex,1) = formulaireRS.Field("formulaire_id").IntegerValue
		    // Garder le numéro du formulaire
		    FormulaireListBox.CellTag(FormulaireListBox.LastIndex,2) = formulaireRS.Field("formulaire_form_id").StringValue
		    
		    formulaireRS.MoveNext
		  Next
		  
		  formulaireRS.Close
		  formulaireRS = Nil
		  
		  
		  // Positionnement par rapport à un numéro en particulier (Le paramètre passé correspond à la ligne + 1)
		  If formulaire_no_ligne_param > 0 Then
		    FormulaireListBox.ListIndex  = formulaire_no_ligne_param -1
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne = formulaire_no_ligne_param - 1
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id= FormulaireListBox.CellTag(Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne,1)
		    Exit Sub
		  End If
		  
		  
		  // Positionnement à la ligne gardée en mémoire
		  If FormulaireListBox.ListIndex  <> Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne And FormulaireListBox.RowCount > 0 Then
		    Dim v As Variant = FormulaireListBox.LastIndex
		    v = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne
		    If Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne > FormulaireListBox.LastIndex  Then Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne = FormulaireListBox.LastIndex
		    FormulaireListBox.ListIndex  = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id = FormulaireListBox.CellTag(Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne,1)
		  End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supFormulaireDetail()
		  Dim messageErreur As String = "Succes"
		  
		  //Déterminer sur quel formulaire on est
		  Dim formulaireActuel As String = FormulaireListBox.CellTag(FormulaireListBox.ListIndex,2)
		  Dim dBSchemaName As String = "form"
		  Dim dBTableName As String = ""
		  Dim dBPrefix As String = ""
		  
		  Select Case formulaireActuel
		  Case "9FA6D6AE78" // Formulaire Meeting SST Journalier
		    dBTableName = "fss0023_meeting_sst_journ"
		    dBPrefix = "fss0023"
		  Case "46B73282F9" // Formulaire Vérification hebdomadaire chef de chantier
		    dBTableName = "fss0006_verif_hebdo_chef_chantier"
		    dBPrefix = "fss0006"
		  Case "C56E5F0670" // Formulaire Rapport visite de chantier
		    dBTableName = "rop0008_rapport_visite_chantier"
		    dBPrefix = "rop0009"
		  Case "9004B3B2CC" // Formulaire Rapport hebdomadaire
		    dBTableName = "rop0009_rapport_hebdomadaire"
		    dBPrefix = "rop0009"
		  End Select 
		  
		  // Vérifier si un enregistrement peut être supprimé
		  messageErreur = Self.sAllFormulaire.validerSuppression(Session.bdGroupeRPF, dBTableName, str(Self.sAllFormulaire.formulaire_id))
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = FormulaireModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String =dBSchemaName + "." +dBTableName+ "." + str(FormulaireListBox.CellTag(FormulaireListBox.ListIndex,1))
		  messageErreur = Self.sAllFormulaire.check_If_Locked(Session.bdGroupeRPF, param_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    Session.pointeurFormulaireContainerControl.IVLockedFormulaire.Visible = True
		    Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Text = Self.sAllFormulaire.lockedBy
		    Session.pointeurFormulaireContainerControl.TLLockedByFormulaire.Visible = True
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable FormulaireListBox.CellTag(FormulaireListBox.ListIndex,1))
		  messageErreur = Self.sAllFormulaire.supprimerDataByField(Session.bdGroupeRPF, dBTableName, dBPrefix + "_id",str(FormulaireListBox.CellTag(FormulaireListBox.ListIndex,1)))
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = FormulaireModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Réafficher la liste des projets
		    Self.populateFormulaire
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		allFormulaireStruct As AllFormulaireStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_numero As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sAllFormulaire As AllFormulaireClass
	#tag EndProperty


	#tag Structure, Name = AllFormulaireStructure, Flags = &h0
		projet As String*12
		  date As String*100
		  formulaire_form_id As String*10
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		revision As String*1
	#tag EndStructure


#tag EndWindowCode

#tag Events FormulaireListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementFormulaire(Me.ListIndex)
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddFormulaire
	#tag Event
		Sub MouseExit()
		  //Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVModFormulaire.Picture = ModModal30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  //Self.IVAddFormulaire.Picture = AjouterModalTE30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVModFormulaire.Picture = ModModal30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    ajouterFormulaire
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelFormulaire
	#tag Event
		Sub MouseExit()
		  Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVModFormulaire.Picture = ModModal30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModalTE30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVModFormulaire.Picture = ModModal30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    supFormulaireDetail
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVEnrFormulaire
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  modFormulaireDetail
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVModFormulaire.Picture = ModModal30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = EnregistrerTE30x30
		  Self.IVModFormulaire.Picture = ModModal30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModFormulaire
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If FormulaireListBox.ListIndex <> -1 And FormulaireListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  //If Self.sFormulaire.lockEditMode = False Then
		  //Self.IVModFormulaire.Picture = ModModal30x30
		  //Else
		  //Self.IVModFormulaire.Picture = ModModalOrange30x30
		  //End If
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddFormulaire.Picture = AjouterModal30x30
		  Self.IVDelFormulaire.Picture = DeleteModal30x30
		  Self.IVEnrFormulaire.Picture = Enregistrer30x30
		  Self.IVModFormulaire.Picture = ModModalTE30x30
		  Self.IVRafFormulaire.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVRafFormulaire
	#tag Event
		Sub MouseExit()
		  Me.Picture = RefreshMenu30x30
		  IVAddFormulaire.Picture = AjouterModal30x30
		  IVModFormulaire.Picture = ModModal30x30
		  IVDelFormulaire.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Picture = RefreshMenuOra30x30
		  IVAddFormulaire.Picture = AjouterModal30x30
		  IVModFormulaire.Picture = ModModal30x30
		  IVDelFormulaire.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  populateFormulaire
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="projet_numero"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
