#tag Class
Protected Class AllFormulaireClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor(dB As PostgreSQLDatabase, tableName As String, prefix As String)
		  // Calling the overridden superclass constructor.
		  Super.Constructor(dB, tableName, prefix)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(formulaireDB As PostgreSQLDatabase, table_nom As String, value As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  Select Case table_nom
		    
		    //Case "reppale_descanomalie"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    //Case "reppale_etat"
		    //strSQL = "SELECT * FROM reppale WHERE reppale_rep_etat = '" + value + "'"
		    //Case "reppale_gravite"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_gravite = '" + value + "'"
		    //Case "reppale_intervention"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_intervention = '" + value + "'"
		    //Case "reppale_acces"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_methode_acces = '" + value + "'"
		    //Case "reppale_pospremiere"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_premiere = '" + value + "'"
		    //Case "reppale_posseconde"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_seconde = '" + value + "'"
		    //Case "reppale_recouvrement"
		    //strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '" + value + "'"
		    //If value = "NS" Then strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '' Or  reppale_recouvrement = '" + value + "'"
		    //Case "responsable"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_responsable = '" + value + "'"
		    //Case "sitecode"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_site = '" + value + "'"
		    //Case "reppale_statut"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_statut = '" + value + "'"
		    //Case "reppale_typeanomalie"
		    // strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    
		  End Select
		  
		  recordSet =  formulaireDB .SQLSelect(strSQL)
		  Dim compteur As Integer = recordSet.RecordCount
		  
		  If compteur > 0 Then
		    Return FormulaireModule.ENREGNEPEUTPASETREDETRUIT
		  Else
		    Return  "Succes"
		  End If
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		formulaire_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		formulaire_form_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		formulaire_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		formulaire_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		formulaire_revision As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="formulaire_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_form_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
