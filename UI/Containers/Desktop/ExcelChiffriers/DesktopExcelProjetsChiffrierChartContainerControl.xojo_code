#tag WebPage
Begin DesktopAllChartsContainerControl DesktopExcelProjetsChiffrierChartContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   584
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebLabel TitreLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   476
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1787973756"
      TabOrder        =   6
      Text            =   "Excel chiffrier projets"
      TextAlign       =   0
      Top             =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   242
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel SheetProjectLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   128
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#CREATEEXCELSHEETPROJECTSUMMARYLABEL"
      TextAlign       =   0
      Top             =   110
      VerticalCenter  =   0
      Visible         =   True
      Width           =   351
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ExcelProjectGenerateButton
      AutoDisable     =   False
      Caption         =   "#CREATEEXCELSHEET"
      Cursor          =   0
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   490
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   110
      VerticalCenter  =   0
      Visible         =   True
      Width           =   227
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea ExcelLogTextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   232
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   745
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "0"
      TabOrder        =   8
      Text            =   ""
      TextAlign       =   0
      Top             =   110
      VerticalCenter  =   0
      Visible         =   True
      Width           =   303
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel SheetProjectDetailLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   128
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#CREATEEXCELSHEETPROJECTDETAILEDLABEL"
      TextAlign       =   0
      Top             =   170
      VerticalCenter  =   0
      Visible         =   True
      Width           =   351
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ExcelLogLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   745
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1349564415"
      TabOrder        =   6
      Text            =   "#LOGLABEL"
      TextAlign       =   0
      Top             =   74
      VerticalCenter  =   0
      Visible         =   True
      Width           =   218
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClearButton
      AutoDisable     =   False
      Caption         =   "#CLEAR"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   982
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   9
      Top             =   81
      VerticalCenter  =   0
      Visible         =   True
      Width           =   61
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ExcelProjectDetailGenerateButton
      AutoDisable     =   False
      Caption         =   "#CREATEEXCELSHEET"
      Cursor          =   0
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   490
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   200
      VerticalCenter  =   0
      Visible         =   True
      Width           =   227
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu ProjetPopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   490
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Text            =   ""
      Top             =   170
      VerticalCenter  =   0
      Visible         =   True
      Width           =   228
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel SheetPeriodDetailLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   128
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#CREATEEXCELSHEETPERIODDETAILEDLABEL"
      TextAlign       =   0
      Top             =   250
      VerticalCenter  =   0
      Visible         =   True
      Width           =   351
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu DatePeriodePaiePopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   491
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Text            =   ""
      Top             =   250
      VerticalCenter  =   0
      Visible         =   True
      Width           =   228
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton DatePeriodePushButton
      AutoDisable     =   False
      Caption         =   "#CREATEEXCELSHEET"
      Cursor          =   0
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   491
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   280
      VerticalCenter  =   0
      Visible         =   True
      Width           =   227
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h21
		Private Function addTitleCell(titre_param As String) As XLSCell
		  Dim c as New XLSCell(titre_param)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 14
		  c.FillColor = &cFFBB44
		  c.FontColor = &cFF0000
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierCcqDasCnesstRbqProjetAjt(selection_param as String) As XLSWorkSheet
		  Dim strSQL As String
		  
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet("AJT")
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleProjetDep
		  
		  // Lecture du réalisé pour les dépenses par GL et date
		  strSQL = _
		  " SELECT * " + _
		  "   FROM   realisedep" + _
		  "   LEFT JOIN projet ON realisedep_projet_no = projet.projet_no " + _
		  "   WHERE realisedep_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "          AND SUBSTR(realisedep_gl,1,1) >= '5' "  + _
		  "          AND realisedep_systeme = 'Ajt' "  + _
		  "    ORDER BY  realisedep_gl, realisedep_date"
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim codeGL_sav, codeGLTrav As String = ""
		  Dim depTotTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  projetRS.MoveFirst
		  codeGL_sav = projetRS.Field("realisedep_gl").StringValue
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    index = index + 1
		    codeGLTrav = projetRS.Field("realisedep_gl").StringValue
		    If  codeGLTrav <> codeGL_sav Then
		      codeGL_sav = codeGLTrav
		      ExcelLogTextArea.text = ExcelLogTextArea.text + "Code GL " + codeGLTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 8
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(depTotTrav)
		      r.AppendCell c
		      
		      depTotTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_gl").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_gl_desc").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_gl_emp_entr").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_date").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_systeme").StringValue)
		    r.AppendCell c
		    
		    depTotTrav = depTotTrav + projetRS.Field("realisedep_montant").DoubleValue
		    c = createStdNumber(projetRS.Field("realisedep_montant").DoubleValue)
		    r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  r =  new XLSRow
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Date période " + codeGLTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 8
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(depTotTrav)
		  r.AppendCell c
		  
		  ws.AppendRow r
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierCcqDasCnesstRbqProjetDep(selection_param as String) As XLSWorkSheet
		  Dim strSQL As String
		  
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet("DEP CP")
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleProjetDep
		  
		  // Lecture du réalisé pour les dépenses par GL et date
		  strSQL = _
		  " SELECT * " + _
		  "   FROM   realisedep" + _
		  "   LEFT JOIN projet ON realisedep_projet_no = projet.projet_no " + _
		  "   WHERE realisedep_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "          AND SUBSTR(realisedep_gl,1,1) >= '5' "  + _
		  "          AND realisedep_systeme <> 'Ajt' "  + _
		  "    ORDER BY  realisedep_gl, realisedep_date"
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim codeGL_sav, codeGLTrav As String = ""
		  Dim depTotTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  projetRS.MoveFirst
		  codeGL_sav = projetRS.Field("realisedep_gl").StringValue
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    index = index + 1
		    codeGLTrav = projetRS.Field("realisedep_gl").StringValue
		    If  codeGLTrav <> codeGL_sav Then
		      codeGL_sav = codeGLTrav
		      ExcelLogTextArea.text = ExcelLogTextArea.text + "Code GL " + codeGLTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 8
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(depTotTrav)
		      r.AppendCell c
		      
		      depTotTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_gl").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_gl_desc").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_gl_emp_entr").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_date").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisedep_systeme").StringValue)
		    r.AppendCell c
		    
		    depTotTrav = depTotTrav + projetRS.Field("realisedep_montant").DoubleValue
		    c = createStdNumber(projetRS.Field("realisedep_montant").DoubleValue)
		    r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  r =  new XLSRow
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Code GL " + codeGLTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 8
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(depTotTrav)
		  r.AppendCell c
		  
		  ws.AppendRow r
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierCcqDasCnesstRbqProjetMInv(selection_param as String) As XLSWorkSheet
		  Dim strSQL As String
		  
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet("REQ-Inv")
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleProjetReq
		  
		  // Lecture du réalisé pour les réquisitions - mouvements d'inventaire
		  strSQL = _
		  " SELECT * " + _
		  "   FROM realisewo" + _
		  "   LEFT JOIN projet ON realisewo_projet_no = projet.projet_no " + _
		  "   WHERE realisewo_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "    ORDER BY  realisewo_no_wo, realisewo_no_ligne"
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim noWO_sav, noWOTrav As String = ""
		  Dim depTotTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  projetRS.MoveFirst
		  noWO_sav = projetRS.Field("realisewo_no_wo").StringValue
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    index = index + 1
		    noWOTrav = projetRS.Field("realisewo_no_wo").StringValue
		    If  noWOTrav <> noWO_sav Then
		      noWO_sav = noWOTrav
		      ExcelLogTextArea.text = ExcelLogTextArea.text + "Work Order " + noWOTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 6
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(depTotTrav)
		      r.AppendCell c
		      
		      depTotTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisewo_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisewo_no_wo").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisewo_no_ligne").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisewo_desc_somm").StringValue)
		    r.AppendCell c
		    
		    depTotTrav = depTotTrav + projetRS.Field("realisewo_montant").DoubleValue
		    c = createStdNumber(projetRS.Field("realisewo_montant").DoubleValue)
		    r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  r =  new XLSRow
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Work Order " + noWOTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 6
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(depTotTrav)
		  r.AppendCell c
		  
		  ws.AppendRow r
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierCcqDasCnesstRbqProjetMO(selection_param as String) As XLSWorkSheet
		  Dim strSQL As String
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet("MO")
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleProjetMO
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre par projet et employé
		  strSQL = _
		  " SELECT  *  " + _
		  "   FROM realisemocont" + _
		  "   LEFT JOIN projet ON realisemocont_projet_no = projet.projet_no " + _
		  "   WHERE realisemocont_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "    ORDER BY  realisemocont_date_periode, realisemocont_date_jour, realisemocont_nas"
		  //"    ORDER BY  realisemocont_projet_no, realisemocont_date_periode, realisemocont_date_jour"
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim datePeriode_sav, datePeriodeTrav, formule As String = ""
		  Dim salTotTrav, salIndhTotTrav, salAip1TotTrav, ccqTotTrav, dasTotTrav, cnesstTotTrav, rbqTotTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  projetRS.MoveFirst
		  datePeriode_sav = projetRS.Field("realisemocont_date_periode").StringValue
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    index = index + 1
		    datePeriodeTrav = projetRS.Field("realisemocont_date_periode").StringValue
		    If  datePeriodeTrav <> datePeriode_sav Then
		      datePeriode_sav = datePeriodeTrav
		      ExcelLogTextArea.text = ExcelLogTextArea.text + "Date période " + datePeriodeTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 9
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(salTotTrav)
		      r.AppendCell c
		      c = createStdNumber(salAip1TotTrav)
		      r.AppendCell c
		      c = createStdNumber(salIndhTotTrav)
		      r.AppendCell c
		      c = createStdNumber(ccqTotTrav)
		      r.AppendCell c
		      c = createStdNumber(dasTotTrav)
		      r.AppendCell c
		      c = createStdNumber(cnesstTotTrav)
		      r.AppendCell c
		      c = createStdNumber(rbqTotTrav)
		      r.AppendCell c
		      formule = "=RC[-5]+RC[-4]+RC[-3]+RC[-2]+RC[-1]"
		      c = createStdFormula(formule)
		      r.AppendCell c
		      
		      salTotTrav = 0
		      salIndhTotTrav = 0
		      salAip1TotTrav = 0
		      ccqTotTrav = 0
		      dasTotTrav = 0
		      cnesstTotTrav = 0
		      rbqTotTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_periode").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_jour").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_nas").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_nom").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_prenom").StringValue)
		    r.AppendCell c
		    
		    c = createStdNumber(projetRS.Field("realisemocont_salperiode").DoubleValue)
		    r.AppendCell c
		    salTotTrav = salTotTrav + projetRS.Field("realisemocont_salbrut").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_salbrut").DoubleValue)
		    r.AppendCell c
		    salIndhTotTrav = salIndhTotTrav + projetRS.Field("realisemocont_indhmont").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_indhmont").DoubleValue)
		    r.AppendCell c
		    salAip1TotTrav = salAip1TotTrav + projetRS.Field("realisemocont_aip1mont").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_aip1mont").DoubleValue)
		    r.AppendCell c
		    ccqTotTrav = ccqTotTrav + projetRS.Field("realisemocont_ccq").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_ccq").DoubleValue)
		    r.AppendCell c
		    dasTotTrav = dasTotTrav + projetRS.Field("realisemocont_das").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_das").DoubleValue)
		    r.AppendCell c
		    cnesstTotTrav = cnesstTotTrav + projetRS.Field("realisemocont_cnesst").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_cnesst").DoubleValue)
		    r.AppendCell c
		    rbqTotTrav = rbqTotTrav + projetRS.Field("realisemocont_rbq").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_rbq").DoubleValue)
		    r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  r =  new XLSRow
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Date période " + datePeriodeTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 9
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(salTotTrav)
		  r.AppendCell c
		  c = createStdNumber(salIndhTotTrav)
		  r.AppendCell c
		  c = createStdNumber(salAip1TotTrav)
		  r.AppendCell c
		  c = createStdNumber(ccqTotTrav)
		  r.AppendCell c
		  c = createStdNumber(dasTotTrav)
		  r.AppendCell c
		  c = createStdNumber(cnesstTotTrav)
		  r.AppendCell c
		  c = createStdNumber(rbqTotTrav)
		  r.AppendCell c
		  formule = "=RC[-5]+RC[-4]+RC[-3]+RC[-2]+RC[-1]"
		  c = createStdFormula(formule)
		  r.AppendCell c
		  ws.AppendRow r
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierCcqDasCnesstRbqProjetMrnfMcnr(selection_param as String) As XLSWorkSheet
		  Dim strSQL As String
		  Dim ws as XLSWorkSheet
		  Dim projetRS As RecordSet
		  Dim r as XLSRow
		  Dim c as XLSCell
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet("MRNF MCNR")
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleProjetMrnfMcnr
		  
		  // Matériel reçu mais non facturé
		  strSQL = _
		  " SELECT * " + _
		  "   FROM   realisepomrnf" + _
		  "   LEFT JOIN projet ON realisepomrnf_projet_no = projet.projet_no " + _
		  "   WHERE realisepomrnf_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "          AND realisepomrnf_montant > 0 "   + _
		  "          AND realisepomrnf_etat <> '030' "   + _
		  "   ORDER BY  realisepomrnf_no_po"
		  
		  projetRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim mrnfTotTrav As Double = 0
		  projetRS.MoveFirst
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    ExcelLogTextArea.text = ExcelLogTextArea.text + "No PO " + projetRS.Field("realisepomrnf_no_po").StringValue + " Traité" + chr(13)
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisepomrnf_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText("Matériel reçu mais non facturé")
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisepomrnf_no_po").StringValue)
		    r.AppendCell c
		    Dim montantDouble As Double = projetRS.Field("realisepomrnf_montant").DoubleValue
		    mrnfTotTrav = mrnfTotTrav + montantDouble
		    c = createStdNumber(montantDouble)
		    r.AppendCell c
		    
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  r =  new XLSRow
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Dépenses engagées Traité" + chr(13)
		  For i As Integer = 1 To 5
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(mrnfTotTrav)
		  r.AppendCell c
		  
		  ws.AppendRow r
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  
		  // Matériel commandé mais non reçu
		  
		  // Lecture du réalisé 
		  strSQL = _
		  " SELECT * " + _
		  "   FROM   realisepomcnr" + _
		  "   LEFT JOIN projet ON realisepomcnr_projet_no = projet.projet_no " + _
		  "   WHERE realisepomcnr_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "          AND realisepomcnr_montant > 0 "   + _
		  "   ORDER BY  realisepomcnr_no_po"
		  
		  projetRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim mcnrTotTrav As Double = 0
		  projetRS.MoveFirst
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    ExcelLogTextArea.text = ExcelLogTextArea.text + "No PO " + projetRS.Field("realisepomcnr_no_po").StringValue + " Traité" + chr(13)
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisepomcnr_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText("Matériel commandé mais non reçu")
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisepomcnr_no_po").StringValue)
		    r.AppendCell c
		    mcnrTotTrav = mcnrTotTrav + projetRS.Field("realisepomcnr_montant").DoubleValue
		    c = createStdNumber(projetRS.Field("realisepomcnr_montant").DoubleValue)
		    r.AppendCell c
		    
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  r =  new XLSRow
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Dépenses engagées Traité" + chr(13)
		  For i As Integer = 1 To 5
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(mcnrTotTrav)
		  r.AppendCell c
		  
		  ws.AppendRow r
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierDasCnesstDatePeriode(selection_param as String) As XLSWorkSheet
		  Dim strSQL As String
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleDatePeriode
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre par projet et employé
		  
		  strSQL = _
		  " SELECT  *  " + _
		  "   FROM   realisemocont" + _
		  "   LEFT JOIN projet ON realisemocont_projet_no = projet.projet_no " + _
		  "   WHERE realisemocont_date_periode = '" + DatePeriodePaiePopupMenu.Text + "' " + _ 
		  "    ORDER BY  realisemocont_nas, realisemocont_date_jour"
		  
		  //"    ORDER BY  realisemocont_projet_no, realisemocont_date_periode, realisemocont_date_jour"
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim nas_sav, nasTrav, formule As String = ""
		  Dim salTotTrav, salIndhTotTrav, salAip1TotTrav, ccqTotTrav, dasTotTrav, cnesstTotTrav, rbqTotTrav, cnesstTauxTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  Dim retenuDict As Dictionary 
		  projetRS.MoveFirst
		  nas_sav = projetRS.Field("realisemocont_nas").StringValue
		  While Not  projetRS.EOF
		    
		    r  =  new XLSRow
		    index = index + 1
		    nasTrav = projetRS.Field("realisemocont_nas").StringValue
		    If  nasTrav <> nas_sav Then
		      // Lire les retenus de l'employeur
		      retenuDict = retenuExtr(nas_sav, projetRS.Field("realisemocont_date_periode").StringValue)
		      nas_sav = nasTrav
		      ExcelLogTextArea.text = ExcelLogTextArea.text + "Nas " + nasTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 9
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(salTotTrav)
		      r.AppendCell c
		      c = createStdNumber(salIndhTotTrav)
		      r.AppendCell c
		      c = createStdNumber(salAip1TotTrav)
		      r.AppendCell c
		      c = createStdNumber(ccqTotTrav)
		      r.AppendCell c
		      c = createStdNumber(dasTotTrav)
		      r.AppendCell c
		      c = createStdNumber(cnesstTotTrav)
		      r.AppendCell c
		      c = createStdNumber(rbqTotTrav)
		      r.AppendCell c
		      c = createStdNumber(cnesstTauxTrav)
		      r.AppendCell c
		      formule = "=RC[-6]+RC[-5]+RC[-4]+(RC[-3]+RC[-2]"
		      c = createStdFormula(formule)
		      r.AppendCell c
		      c = createStdNumber(retenuDict.Lookup("Applicable", 0))
		      r.AppendCell c
		      c = createStdNumber(retenuDict.Lookup("PartEmployeur", 0))
		      r.AppendCell c
		      c = createStdNumber(retenuDict.Lookup("AvImPosFed", 0))
		      r.AppendCell c
		      
		      salTotTrav = 0
		      salIndhTotTrav = 0
		      salAip1TotTrav = 0
		      ccqTotTrav = 0
		      dasTotTrav = 0
		      cnesstTotTrav = 0
		      rbqTotTrav = 0
		      cnesstTauxTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText((Session.bdGroupeRPF.DatabaseName))
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_periode").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_jour").StringValue)
		    r.AppendCell c
		    c = createStdText(nasTrav)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_nom").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_prenom").StringValue)
		    r.AppendCell c
		    
		    // Lecture des données de retenues
		    
		    c = createStdNumber(projetRS.Field("realisemocont_salperiode").DoubleValue)
		    r.AppendCell c
		    salTotTrav = salTotTrav + projetRS.Field("realisemocont_salbrut").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_salbrut").DoubleValue)
		    r.AppendCell c
		    salIndhTotTrav = salIndhTotTrav + projetRS.Field("realisemocont_indhmont").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_indhmont").DoubleValue)
		    r.AppendCell c
		    salAip1TotTrav = salAip1TotTrav + projetRS.Field("realisemocont_aip1mont").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_aip1mont").DoubleValue)
		    r.AppendCell c
		    ccqTotTrav = ccqTotTrav + projetRS.Field("realisemocont_ccq").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_ccq").DoubleValue)
		    r.AppendCell c
		    dasTotTrav = dasTotTrav + projetRS.Field("realisemocont_das").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_das").DoubleValue)
		    r.AppendCell c
		    cnesstTotTrav = cnesstTotTrav + projetRS.Field("realisemocont_cnesst").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_cnesst").DoubleValue)
		    r.AppendCell c
		    rbqTotTrav = rbqTotTrav + projetRS.Field("realisemocont_rbq").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_rbq").DoubleValue)
		    r.AppendCell c
		    cnesstTauxTrav = projetRS.Field("realisemocont_cnesst_taux").DoubleValue
		    
		    // Dépenses CMEQ
		    //MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMO",0) + estimeDict.Lookup("ReelMontantTotalMAT",0)
		    //c = createStdNumber( MontantDouble1)
		    //r.AppendCell c
		    
		    // Marge brute calculée
		    //formule = "=RC[-1]-(RC[-3]+RC[-2])"
		    //c = createStdFormula(formule)
		    //r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  
		  // Écrire dernière ligne
		  r =  new XLSRow
		  retenuDict = retenuExtr(nas_sav, projetRS.Field("realisemocont_date_periode").StringValue)
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Nas " + nasTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 9
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(salTotTrav)
		  r.AppendCell c
		  c = createStdNumber(salIndhTotTrav)
		  r.AppendCell c
		  c = createStdNumber(salAip1TotTrav)
		  r.AppendCell c
		  c = createStdNumber(ccqTotTrav)
		  r.AppendCell c
		  c = createStdNumber(dasTotTrav)
		  r.AppendCell c
		  c = createStdNumber(cnesstTotTrav)
		  r.AppendCell c
		  c = createStdNumber(rbqTotTrav)
		  r.AppendCell c
		  c = createStdNumber(cnesstTauxTrav)
		  r.AppendCell c
		  formule = "=RC[-6]+RC[-5]+RC[-4]+(RC[-3]+RC[-2]"
		  c = createStdFormula(formule)
		  r.AppendCell c
		  c = createStdNumber(retenuDict.Lookup("Applicable", 0))
		  r.AppendCell c
		  c = createStdNumber(retenuDict.Lookup("PartEmployeur", 0))
		  r.AppendCell c
		  c = createStdNumber(retenuDict.Lookup("AvImPosFed", 0))
		  r.AppendCell c
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createChiffrierProjets() As XLSWorkSheet
		  Dim strSQL As String
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createProjetsTitle
		  
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT projet_no,projet_titre, projet_titre, parametre_desc_fr AS projet_type_desc, projet_charge_projet_nom, projet_statut " + _
		  "  FROM projet" + _
		  "      INNER JOIN parametre ON  parametre.parametre_nom = 'projettype' " + _
		  "                                          AND  parametre.parametre_cle    = projet.projet_type " + _
		  "  WHERE projet_statut = 'Travaux en cours' " + _ 
		  "      AND parametre_desc_fr <> 'Autre' " + _ 
		  "  ORDER BY  projet_no "
		  //"  WHERE projet.projet_statut IN('Travaux en cours', 'En suspens', 'Travaux complétés') " + _ 
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  While Not  projetRS.EOF
		    Dim r as XLSRow =  new XLSRow
		    Dim c as XLSCell
		    index = index + 1
		    
		    c = createStdText(Session.bdGroupeRPF.DatabaseName)
		    r.AppendCell c
		    Dim statut As String = projetRS.Field("projet_statut").StringValue
		    //statut = Replace(statut,"Travaux complétés", "Complétés")
		    statut = Replace(statut,"Travaux en cours", "En cours")
		    c =createStdText(statut)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_type_desc").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_charge_projet_nom").StringValue)
		    r.AppendCell c
		    
		    // Lecture de toutes les données estimation et réelle
		    Dim estimeDict As Dictionary = estimeReelExtr(projetRS.Field("projet_no").StringValue)
		    
		    // Revenu estimé
		    Dim MontantDouble1 As Double = estimeDict.Lookup("EstimeMontantFactureProfit", 0)
		    //Dim MontantDouble2 As Double = estimeDict.Lookup("EstimeMontantFactureExtra", 0)
		    c = createStdNumber(MontantDouble1)
		    r.AppendCell c
		    
		    // Dépenses estimées
		    MontantDouble1 = estimeDict.Lookup("EstimeMontantFactureSansProfit",0)
		    c = createStdNumber(MontantDouble1)
		    r.AppendCell c
		    
		    // Extra estimé
		    MontantDouble1 = estimeDict.Lookup("EstimeMontantFactureExtra",0)
		    c = createStdNumber(MontantDouble1)
		    r.AppendCell c
		    
		    // Formule de Marge brute estimée
		    //Dim formule As String = "=SI(G"+ str(index) + "=0; 0; (G"+ str(index) + "-H" + str(index) + ")/G"+ str(index) + ")"
		    Dim formule As String = "=(RC[-3]-RC[-2])/RC[-3]"
		    //Dim formule As String = "=SI(RC[-2]=0;0;(RC[-2]-RC[-1])/RC[-2])"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    //Montant total facturé Chef De Projet
		    //r.AppendCell createStdNumber(0)
		    //Dépense estimées Chef De Projet
		    //r.AppendCell createStdNumber(0)
		    
		    // Dépenses CMEQ
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMAT",0) + estimeDict.Lookup("ReelMontantTotalREQ",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Main d'oeuvre
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMO",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Ajustement
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalAJT",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Dépenses engagées CMEQ
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMRNF",0)  + estimeDict.Lookup("ReelMontantTotalMCNR",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Revenus totaux réalisés CMEQ
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalRealise",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Revenus facturé mains non payé CMEQ
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalFactureNonPaye",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Marge brute calculée
		    formule = "=RC[-2]+RC[-1]-(RC[-6]+RC[-5]+RC[-4]+RC[-3])"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    // Marge brute %
		    formule = "=RC[-1]/(RC[-2]+RC[-3])"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    Suivant:
		    ExcelLogTextArea.text = ExcelLogTextArea.text + "Projet " + projetRS.Field("projet_no").StringValue + " Traité" + chr(13)
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub createChiffrierProjetsDateCompagnie(selection_param as String)
		  dim w as XLSWorkBook
		  
		  // create a new workbook
		  w = new XLSWorkbook
		  ExcelLogTextArea.text = ""
		  
		  w.WindowHeightInTwips = 288
		  w.WindowWidthInTwips = 288
		  
		  Dim d as New Date
		  //<Data ss:Type="DateTime">2004-12-03T12:00:00.000</Data>
		  Dim dateTimeToday As String = Format(d.Year,"0000") + Format(d.month,"00") + Format(d.day,"00") + Format(d.Hour,"00") + Format(d.Minute,"00") + Format(d.Second,"00")
		  
		  // add worksheet to the work book
		  Dim nomFichier As String = ""
		  If selection_param = "Projets" Then
		    w.workSheets.append CreateChiffrierProjets
		    nomFichier = "Excel XML Workbook " + Session.bdGroupeRPF.DatabaseName + " " + dateTimeToday + ".xml"
		  ElseIf selection_param = "Projet" Then
		    w.workSheets.append createChiffrierCcqDasCnesstRbqProjetMInv(selection_param)
		    w.workSheets.append createChiffrierCcqDasCnesstRbqProjetDep(selection_param)
		    w.workSheets.append createChiffrierCcqDasCnesstRbqProjetMO(selection_param)
		    w.workSheets.append createChiffrierCcqDasCnesstRbqProjetAjt(selection_param)
		    w.workSheets.append createChiffrierCcqDasCnesstRbqProjetMrnfMcnr(selection_param)
		    nomFichier = "Excel CCQ DAS CNESST RBQ " + Session.bdGroupeRPF.DatabaseName + " " + selection_param + " " + ProjetPopupMenu.Text + " " + dateTimeToday + ".xml"
		  Else
		    w.workSheets.append createChiffrierDasCnesstDatePeriode(selection_param)
		    nomFichier = "Excel CCQ DAS CNESST RBQ " + Session.bdGroupeRPF.DatabaseName + " " + selection_param + " " +  DatePeriodePaiePopupMenu.Text + " " + dateTimeToday  + ".xml"
		  End If
		  
		  Dim f as FolderItem 
		  f = GetFolderItem(nomFichier)
		  f.Delete
		  // Sauvegarde
		  w.Save(f)
		  
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Téléchargement en cours..." + chr(13)
		  App.CurrentThread.Sleep(1000)
		  
		  exportFile = WebFile.Open(f)
		  exportFile.ForceDownload = True
		  ShowURL(exportFile.URL)
		  
		  ExcelLogTextArea.text = ExcelLogTextArea.text + "Téléchargement terminée" + chr(13)
		  f.Delete
		  f = Nil
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createProjetsTitle() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("Statut")
		  r.AppendCell c
		  c = addTitleCell("Type de projet")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Nom du chargé de projet")
		  r.AppendCell c
		  c = addTitleCell("Revenu estimé")
		  r.AppendCell c
		  c = addTitleCell("Dépense estimée")
		  r.AppendCell c
		  c = addTitleCell("Extra estimé")
		  r.AppendCell c
		  c = addTitleCell("Marge brute estimée")
		  r.AppendCell c
		  c = addTitleCell("Dépenses")
		  r.AppendCell c
		  c = addTitleCell("Main d'oeuvre")
		  r.AppendCell c
		  c = addTitleCell("Ajustement")
		  r.AppendCell c
		  c = addTitleCell("Dépenses engagées")
		  r.AppendCell c
		  c = addTitleCell("Revenu réalisé")
		  r.AppendCell c
		  c = addTitleCell("Revenu facturé non payé")
		  r.AppendCell c
		  c = addTitleCell("Marge brute calculée")
		  r.AppendCell c
		  c = addTitleCell("Marge brute %")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit" + chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createProjetTitle() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("Statut")
		  r.AppendCell c
		  c = addTitleCell("Type de projet")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Nom du chargé de projet")
		  r.AppendCell c
		  c = addTitleCell("Revenu estimé")
		  r.AppendCell c
		  c = addTitleCell("Dépense estimée")
		  r.AppendCell c
		  c = addTitleCell("Extra estimé")
		  r.AppendCell c
		  c = addTitleCell("Marge brute estimée")
		  r.AppendCell c
		  c = addTitleCell("Dépense")
		  r.AppendCell c
		  c = addTitleCell("Main d'oeuvre")
		  r.AppendCell c
		  c = addTitleCell("Ajustement")
		  r.AppendCell c
		  c = addTitleCell("Revenu réalisé")
		  r.AppendCell c
		  c = addTitleCell("Revenu facturé non payé")
		  r.AppendCell c
		  c = addTitleCell("Marge brute calculée")
		  r.AppendCell c
		  c = addTitleCell("Marge brute %")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit" + chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createStdFormula(formula_param As String) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = "0"
		  c.type = XLSCell.typeNumber
		  c.formula = formula_param
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createStdNumber(nombreDouble_param As Double) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = str(nombreDouble_param)
		  c.type = XLSCell.typeNumber
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createStdText(text_param As String) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = text_param
		  c.type = XLSCell.typeString
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createTitleDatePeriode() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Date période")
		  r.AppendCell c
		  c = addTitleCell("Date du jour")
		  r.AppendCell c
		  c = addTitleCell("Nas")
		  r.AppendCell c
		  c = addTitleCell("Nom")
		  r.AppendCell c
		  c = addTitleCell("Prénom")
		  r.AppendCell c
		  c = addTitleCell("Salaire période")
		  r.AppendCell c
		  c = addTitleCell("Salaire contrat")
		  r.AppendCell c
		  c = addTitleCell("Indh")
		  r.AppendCell c
		  c = addTitleCell("Aip1")
		  r.AppendCell c
		  c = addTitleCell("CCQ")
		  r.AppendCell c
		  c = addTitleCell("DAS")
		  r.AppendCell c
		  c = addTitleCell("Cnesst")
		  r.AppendCell c
		  c = addTitleCell("RBQ")
		  r.AppendCell c
		  c = addTitleCell("Taux Cnesst")
		  r.AppendCell c
		  c = addTitleCell("Sal+CCQ+Das+CNESST+RBQ")
		  r.AppendCell c
		  c = addTitleCell("Applicable")
		  r.AppendCell c
		  c = addTitleCell("Part Employeur")
		  r.AppendCell c
		  c = addTitleCell("AvImPosFed")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createTitleProjetDep() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("GL")
		  r.AppendCell c
		  c = addTitleCell("Description GL")
		  r.AppendCell c
		  c = addTitleCell("Autre description")
		  r.AppendCell c
		  c = addTitleCell("Date")
		  r.AppendCell c
		  c = addTitleCell("Système")
		  r.AppendCell c
		  c = addTitleCell("Montant")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createTitleProjetMO() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Date période")
		  r.AppendCell c
		  c = addTitleCell("Date du jour")
		  r.AppendCell c
		  c = addTitleCell("Nas")
		  r.AppendCell c
		  c = addTitleCell("Nom")
		  r.AppendCell c
		  c = addTitleCell("Prénom")
		  r.AppendCell c
		  c = addTitleCell("Salaire période")
		  r.AppendCell c
		  c = addTitleCell("Salaire contrat")
		  r.AppendCell c
		  c = addTitleCell("Indh")
		  r.AppendCell c
		  c = addTitleCell("Aip1")
		  r.AppendCell c
		  c = addTitleCell("CCQ")
		  r.AppendCell c
		  c = addTitleCell("DAS")
		  r.AppendCell c
		  c = addTitleCell("CNESST")
		  r.AppendCell c
		  c = addTitleCell("RBQ")
		  r.AppendCell c
		  c = addTitleCell("Sal+CCQ+Das+CNESST+RBQ")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createTitleProjetMrnfMcnr() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Système")
		  r.AppendCell c
		  c = addTitleCell("PO")
		  r.AppendCell c
		  c = addTitleCell("Montant")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function createTitleProjetReq() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("WO")
		  r.AppendCell c
		  c = addTitleCell("No Ligne")
		  r.AppendCell c
		  c = addTitleCell("Description sommaire")
		  r.AppendCell c
		  c = addTitleCell("Montant")
		  r.AppendCell c
		  
		  ExcelLogTextArea.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function estimeReelExtr(projet_no_param As String) As Dictionary
		  Dim strSQL As String = ""
		  Dim estimeDict As New Dictionary 
		  
		  // Lecture de l'estimé pour le revenu total 
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As EstimeMontantFacture " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + projet_no_param + "'" + _
		  "          AND SUBSTRING(estime_rev_mo_mat,1,2) <> 'EX' " 
		  Dim estimeReelRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantFactureProfit") = estimeReelRS.Field("EstimeMontantFacture").DoubleValue
		  estimeDict.Value("EstimeMontantFactureSansProfit") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture de l'estimé pour le revenu des demandes de changement
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As EstimeMontantFactureExtra " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + projet_no_param + "'" + _
		  "          AND estime_rev_mo_mat = 'EXREV' " 
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantFactureExtra") = estimeReelRS.Field("EstimeMontantFactureExtra").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture de l'estimé pour la main d'oeuvre
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + projet_no_param + "'"   + _
		  "          AND estime_rev_mo_mat    IN( 'MO')" 
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantMO") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture de l'estimé pour le matériel
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + projet_no_param + "'"   + _
		  "          AND estime_rev_mo_mat    IN('MAT')" 
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantMAT") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = "SELECT ROUND(SUM(realiserev_montant),0) As Montant, ROUND(SUM(realiserev_paiement),0) As Paiement " + _
		  "        FROM realiserev " + _ 
		  "       WHERE realiserev_projet_no = '" + projet_no_param + "'" 
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalRealise") = estimeReelRS.Field("Paiement").DoubleValue
		  estimeDict.Value("ReelMontantTotalFactureNonPaye") = estimeReelRS.Field("Montant").DoubleValue - estimeReelRS.Field("Paiement").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre
		  strSQL = _
		  "   SELECT ROUND(SUM(realisemocont_salbrut+realisemocont_indhmont+realisemocont_ccq+realisemocont_das+realisemocont_cnesst+realisemocont_rbq),0) As Montant " + _
		  "        FROM realisemocont " + _ 
		  "       WHERE realisemocont_projet_no = '" + projet_no_param + "'"
		  
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalMO") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les réquisitions - Mouvement d'inventaire
		  strSQL = _
		  "SELECT ROUND(SUM(realisewo_montant),0) As Montant " + _
		  "        FROM realisewo " + _ 
		  "       WHERE realisewo_projet_no  = '" + projet_no_param + "'"   
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalREQ") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les dépenses de matériel
		  strSQL = _
		  "SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + projet_no_param + "'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) >= '5' "  + _
		  "          AND realisedep_systeme <> 'Ajt' " 
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalMAT") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les Ajustements
		  strSQL = _
		  "SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + projet_no_param + "'"   + _
		  "          AND realisedep_systeme = 'Ajt' " 
		  
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalAJT") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour le matériel reçu mais non facturé
		  strSQL = _
		  "SELECT SUM(realisepomrnf_montant) As Montant " + _
		  "        FROM realisepomrnf " + _ 
		  "       WHERE realisepomrnf_projet_no = '" + projet_no_param + "'"   + _
		  "          AND realisepomrnf_montant > 0 "   + _
		  "          AND realisepomrnf_etat <> '030' " 
		  
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalMRNF") = 0.0
		  If estimeReelRS.Field("Montant").Value <> Nil Then estimeDict.Value("ReelMontantTotalMRNF") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour le matériel commandé mais non reçu
		  strSQL = _
		  "SELECT SUM(realisepomcnr_montant) AS Montant " + _
		  "        FROM realisepomcnr " + _ 
		  "       WHERE realisepomcnr_projet_no = '" + projet_no_param + "'"   + _
		  "          AND realisepomcnr_montant > 0 " 
		  
		  estimeReelRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalMCNR") = 0.0
		  If estimeReelRS.Field("Montant").Value <> Nil Then estimeDict.Value("ReelMontantTotalMCNR") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  Return estimeDict
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub listeDatePeriodePaie()
		  Dim strSQL As String
		  
		  DatePeriodePaiePopupMenu.DeleteAllRows
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT realisemocont_date_periode " + _
		  "  FROM realisemocont" + _
		  "  GROUP BY realisemocont_date_periode" + _
		  "  ORDER BY  realisemocont_date_periode DESC"
		  
		  Dim datePeriodeRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If datePeriodeRS = Nil Then Exit
		  While Not  datePeriodeRS.EOF
		    DatePeriodePaiePopupMenu.AddRow(datePeriodeRS.Field("realisemocont_date_periode").StringValue)
		    datePeriodeRS.MoveNext
		  Wend
		  DatePeriodePaiePopupMenu.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub listeProjet()
		  
		  Dim strSQL As String
		  
		  ProjetPopupMenu.DeleteAllRows
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT projet_no " + _
		  "  FROM projet" + _
		  "  WHERE projet_statut IN('Travaux complétés', 'Travaux terminés', 'Travaux en cours', 'En suspens') " + _
		  "  ORDER BY  projet_no DESC"
		  
		  Dim projetRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  While Not  projetRS.EOF
		    ProjetPopupMenu.AddRow(projetRS.Field("projet_no").StringValue)
		    projetRS.MoveNext
		  Wend
		  ProjetPopupMenu.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listExcelProjetsChiffrierDetail()
		  listeDatePeriodePaie
		  listeProjet
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function retenuExtr(nas_param As String, date_periode_param As String) As Dictionary
		  Dim strSQL As String = ""
		  Dim retenueDict As New Dictionary 
		  
		  // Lecture de l'estimé pour le revenu total
		  strSQL = "SELECT  realisemoret_applicable As Applicable, realisemoret_partemployeur As PartEmployeur, realisemoret_avimposfed As AvImPosFed " + _
		  "        FROM realisemoret " + _ 
		  "       WHERE realisemoret_nas = '" + nas_param + "'"  + _
		  "          AND realisemoret_date_periode = '" + date_periode_param + "'" 
		  Dim retenueRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If retenueRS = Nil Then Exit
		  retenueRS.MoveFirst
		  retenueDict.Value("Applicable") = retenueRS.Field("Applicable").DoubleValue
		  retenueDict.Value("PartEmployeur") = retenueRS.Field("PartEmployeur").DoubleValue
		  retenueDict.Value("AvImPosFed") = retenueRS.Field("AvImPosFed").DoubleValue
		  retenueRS.Close
		  retenueRS = Nil
		  
		  
		  Return retenueDict
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		exportFile As WebFile
	#tag EndProperty


	#tag Constant, Name = ANNULER, Type = String, Dynamic = True, Default = \"Cancel", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cancel"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Annuler"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cancel"
	#tag EndConstant

	#tag Constant, Name = CLEAR, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Clear"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vider"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Clear"
	#tag EndConstant

	#tag Constant, Name = CREATEEXCELSHEET, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Create/Export Excel Sheet"
	#tag EndConstant

	#tag Constant, Name = CREATEEXCELSHEETPERIODDETAILEDLABEL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel contenant la liste d\xC3\xA9taill\xC3\xA9e de la paie pour une date s\xC3\xA9lectionn\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel contenant la liste d\xC3\xA9taill\xC3\xA9e de la paie pour une date s\xC3\xA9lectionn\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Create/Export Excel Sheet detailed list for a payroll selected date"
	#tag EndConstant

	#tag Constant, Name = CREATEEXCELSHEETPROJECTDETAILEDLABEL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel contenant la liste d\xC3\xA9taill\xC3\xA9e du projet s\xC3\xA9lectionn\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel contenant la liste d\xC3\xA9taill\xC3\xA9e du projet s\xC3\xA9lectionn\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Create/Export Excel Sheet detailed list for a selected project"
	#tag EndConstant

	#tag Constant, Name = CREATEEXCELSHEETPROJECTSUMMARYLABEL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel contenant la liste sommaire des projets en cours"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cr\xC3\xA9ation/Exportation de la feuille Excel contenant la liste sommaire des projets en cours"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Create/Export Excel Sheet summary list containing in progress projects"
	#tag EndConstant

	#tag Constant, Name = GENERATESHEET, Type = String, Dynamic = True, Default = \"Generate  sheet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Generate sheet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Generate sheet"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"G\xC3\xA9n\xC3\xA9rer la feuille"
	#tag EndConstant

	#tag Constant, Name = LOGLABEL, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9roulement du transfert"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Processing log"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9roulement du transfert"
	#tag EndConstant


#tag EndWindowCode

#tag Events ExcelProjectGenerateButton
	#tag Event
		Sub Action()
		  createChiffrierProjetsDateCompagnie("Projets")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClearButton
	#tag Event
		Sub Action()
		  ExcelLogTextArea.text = ""
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ExcelProjectDetailGenerateButton
	#tag Event
		Sub Action()
		  createChiffrierProjetsDateCompagnie("Projet")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DatePeriodePushButton
	#tag Event
		Sub Action()
		  createChiffrierProjetsDateCompagnie("DatePeriode")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
