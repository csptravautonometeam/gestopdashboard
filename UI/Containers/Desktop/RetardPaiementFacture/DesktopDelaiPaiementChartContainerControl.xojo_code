#tag WebPage
Begin DesktopAllChartsContainerControl DesktopDelaiPaiementChartContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   586
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebChartView RetPaiementFacAnneeChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   250
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   40
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   307
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   740
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton AnneeNombreButton
      AutoDisable     =   False
      Caption         =   "#NOMBRE"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   206
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton AnneeMoyenneButton
      AutoDisable     =   False
      Caption         =   "#MOYENNE"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton AnneeMaximumButton
      AutoDisable     =   False
      Caption         =   "#MAX"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   532
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebChartView RetPaiementFacClientChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   530
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   905
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   27
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   425
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientAnneePlus
      AutoDisable     =   False
      Caption         =   "<"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   905
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientAnneeMoins
      AutoDisable     =   False
      Caption         =   ">"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1270
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientMoyenneButton
      AutoDisable     =   False
      Caption         =   "#MOYENNE"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1074
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientMaximumButton
      AutoDisable     =   False
      Caption         =   "#MAX"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1171
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientNombreButton
      AutoDisable     =   False
      Caption         =   "#NOMBRE"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   977
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   559
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub listRetardPaiementFactureDetail()
		  Dim anneeFin, moisFin As String
		  
		  // Extraire Année et Mois passé
		  Dim interval1 As New Xojo.Core.DateInterval
		  interval1.Months = 1
		  Dim lastMonth As Xojo.Core.Date = Xojo.Core.Date.Now - interval1
		  anneeFin = str(lastMonth.Year)
		  moisFin = str(lastMonth.Month)
		  Dim interval2 As New Xojo.Core.DateInterval
		  interval2.Years = 4
		  Dim firstYear As Xojo.Core.Date = lastMonth - interval2
		  anneeDepart = firstYear.Year
		  //anneeDepart = 2014
		  anneeClient = Xojo.Core.Date.Now.Year _
		  
		  //anneeClient = 2016
		  
		  // Graphique 
		  //revTypeGLExtr
		  //revTypeGLDisp
		  
		  // Graphique retard paiement facture par année
		  nbreMoyMaxAnnee = "nombre"
		  retPaiementFacAnneeExtr
		  retPaiementFacAnneeDisp
		  
		  // Graphique retard paiement facture par client
		  nbreMoyMaxClient = "nombre"
		  retPaiementFacClientExtr
		  retPaiementFacClientDisp
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub retPaiementFacAnneeDisp()
		  retPaiementFacAnneeChartView.Freeze = True
		  
		  Select Case nbreMoyMaxAnnee
		  Case "nombre"
		    retPaiementFacAnneeChartView.Title = RETARDPAIEMENTMOIS + " (" + NOMBRE + ") "
		  Case "moyenne"
		    retPaiementFacAnneeChartView.Title = RETARDPAIEMENTMOIS + " (" + MOYENNE + ")" 
		  Case "maximum"
		    retPaiementFacAnneeChartView.Title = RETARDPAIEMENTMOIS + " (" + MAX + ") "
		  End Select
		  retPaiementFacAnneeChartView.BackgroundType = WebChartView.BackgroundSolid
		  retPaiementFacAnneeChartView.BackgroundColor.append &cFFFFFF
		  retPaiementFacAnneeChartView.Type = retPaiementFacAnneeChartView.TypeLine
		  retPaiementFacAnneeChartView.FollowValues = True
		  retPaiementFacAnneeChartView.FollowAllSeries = True
		  
		  // Vider le graphe précédent, si présent
		  Dim chartSerieTemp() As ChartSerie = retPaiementFacAnneeChartView.Series
		  Dim nbre As Integer = chartSerieTemp.Ubound
		  For i As Integer = 0 To nbre
		    retPaiementFacAnneeChartView.Series.Remove(0)
		  Next I
		  
		  Dim S As ChartSerie
		  Dim retPaiementFacAnneeResultsDoubleTrav() As Double
		  
		  For index As Integer = 0 To 4
		    S = New ChartSerie
		    S.Title = str(anneeDepart+index)
		    ReDim retPaiementFacAnneeResultsDoubleTrav(-1)
		    For i As Integer = 0 To 11
		      retPaiementFacAnneeResultsDoubleTrav.append(retPaiementFacAnneeResultsDouble(index,i))
		    Next i
		    S.Data = retPaiementFacAnneeResultsDoubleTrav
		    S.LineWeight = 2
		    S.MarkerType = S.MarkerNone
		    //S.Shadow = True
		    //S.ShadowColor = &c000000C3
		    retPaiementFacAnneeChartView.AddSerie(S)
		  Next index
		  
		  // Vider Axes 0
		  Dim chartAxesLabelTemp() As Variant = retPaiementFacAnneeChartView.Axes(0).Label
		  nbre = chartAxesLabelTemp.Ubound
		  For i As Integer = 0 To nbre
		    retPaiementFacAnneeChartView.Axes(0).Label.Remove(0)
		  Next I
		  // Remplir Axes 0
		  Dim index As Integer
		  Dim d As date = New Date
		  d.Day = 1
		  For index  = 1 to 12
		    d.Month = index
		    retPaiementFacAnneeChartView.Axes(0).Label.append New Date(d)
		  Next
		  retPaiementFacAnneeChartView.Axes(0).AutoLabel = False
		  retPaiementFacAnneeChartView.Axes(0).LabelStyle.Format = "M"
		  retPaiementFacAnneeChartView.setTransparency(40)
		  
		  // Remplir Axes 1
		  Dim chartLineTemp As ChartLine = retPaiementFacAnneeChartView.Axes(1).MajorGridLine
		  retPaiementFacAnneeChartView.Axes(1).MajorGridLine = Nil
		  retPaiementFacAnneeChartView.Axes(1).MajorGridLine = New ChartLine
		  retPaiementFacAnneeChartView.Axes(1).MajorGridLine.FillColor = &cE4E4E4
		  retPaiementFacAnneeChartView.Axes(1).MaximumScale = maxScale
		  retPaiementFacAnneeChartView.Axes(1).MajorUnit = 50
		  retPaiementFacAnneeChartView.Axes(0).LabelStyle.TextSize = 14
		  retPaiementFacAnneeChartView.Axes(0).LabelInterval = -1
		  retPaiementFacAnneeChartView.LegendPosition = WebChartView.Position.Right
		  
		  retPaiementFacAnneeChartView.HighlightMinValue(&cFF0000, -1)
		  
		  retPaiementFacAnneeChartView.Freeze = False
		  retPaiementFacAnneeChartView.Animate = True
		  retPaiementFacAnneeChartView.Redisplay()
		  'retPaiementFacAnneeChartView.StartAnimation(True, ChartAnimate.kEaseOutCirc)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub retPaiementFacAnneeExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = _
		  "SELECT SUBSTR(Date_facture,1,4) AS Annee, SUBSTR(Date_facture,6,2) AS Mois, Sum(Compteur) AS SumCompteur, ROUND(AVG(Jours_Diff),2) AS Moy_Nbre_Jours_Diff, MAX(Jours_Diff) AS Max_Jours_Diff " + _
		  "FROM " + _
		  "  (SELECT realiserev_entr AS Entreprise,realiserev_facture AS Facture, realiserev_termes AS Termes, realiserev_date AS Date_facture, " + _
		  "           realiserev_dernier_paiement AS Date_Paiement, realiserev_jour_interet AS Echeance, realiserev_paiement AS Paiement, " + _
		  "          (CASE WHEN realiserev_dernier_paiement = '' Then CURRENT_DATE - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) As Nb_Jours, " + _
		  "          (CASE WHEN realiserev_dernier_paiement = '' Then CURRENT_DATE - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) - realiserev_jour_interet AS Jours_Diff, " + _
		  "           SUM(1) AS Compteur, " + _
		  "           SUM(realiserev_montant) AS SumMontant " + _
		  "        FROM realiserev " + _ 
		  "     WHERE realiserev_termes <> '' " + _
		  "     GROUP BY Entreprise, Facture, Termes, Date_facture, Date_paiement, Echeance, Paiement, Nb_Jours, Jours_Diff) " + _
		  "  AS RealiseTable " + _
		  "  LEFT JOIN client_regroupe ON Entreprise = client_regroupe.client_nom " + _
		  "  WHERE Jours_Diff > 0 " + _
		  "     AND SumMontant > 0 " + _
		  "    AND SUBSTR(Date_facture,1,4) >= '" + str(anneeDepart) +"' " + _
		  "  GROUP BY Annee, Mois " + _
		  "  ORDER BY  Annee, Mois "
		  
		  //"          (CASE WHEN realiserev_dernier_paiement = '' Then Date '2016-08-01' - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) As Nb_Jours, " + _
		  //"          (CASE WHEN realiserev_dernier_paiement = '' Then Date '2016-08-01' - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) - realiserev_jour_interet AS Jours_Diff, " + _
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  
		  Dim tempDict As New Dictionary
		  maxScale = 0
		  // Mettre les résultats dans un dictionnaire et trouver la plus grande valeur
		  While Not  dashboardRS.EOF
		    Select Case nbreMoyMaxAnnee
		    Case "nombre"
		      tempDict.Value(dashboardRS.Field("Annee").StringValue + dashboardRS.Field("Mois").StringValue) =  dashboardRS.Field("SumCompteur").StringValue
		      If dashboardRS.Field("SumCompteur").IntegerValue > maxScale Then maxScale =  dashboardRS.Field("SumCompteur").IntegerValue
		    Case "moyenne"
		      tempDict.Value(dashboardRS.Field("Annee").StringValue + dashboardRS.Field("Mois").StringValue) =  dashboardRS.Field("Moy_Nbre_Jours_Diff").StringValue
		      If dashboardRS.Field("Moy_Nbre_Jours_Diff").IntegerValue > maxScale Then maxScale =  dashboardRS.Field("Moy_Nbre_Jours_Diff").IntegerValue
		    Case "maximum"
		      tempDict.Value(dashboardRS.Field("Annee").StringValue + dashboardRS.Field("Mois").StringValue) =  dashboardRS.Field("Max_Jours_Diff").StringValue
		      If dashboardRS.Field("Max_Jours_Diff").IntegerValue > maxScale Then maxScale =  dashboardRS.Field("Max_Jours_Diff").IntegerValue
		    End Select
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Arrondir maxScaleTemp à la centaine supérieure
		  Dim maxScaleTemp As String = str(maxScale)
		  Dim maxScaleDouble As Double = maxScale
		  Dim longueur As Integer = Len(maxScaleTemp)
		  maxScale = Floor((maxScaleDouble + 10 ^ (longueur - 2)) / 10 ^ (longueur - 2)) * 10 ^ (longueur - 2)
		  
		  // Compléter les valeurs
		  Dim valeurPrecedente As String = ""
		  For indexAnnee As Integer = 0 To 4
		    Dim anneeStr As String = str(anneeDepart + indexAnnee)
		    For indexMois As Integer = 1 To 12
		      Dim moisStr As String = str(indexMois)
		      If indexMois < 10 Then moisStr = "0" + moisStr
		      Dim anneeMoisStr As String = anneeStr + moisStr 
		      retPaiementFacAnneeResultsDouble(indexAnnee, indexMois-1) = CDbl(tempDict.Lookup(anneeMoisStr,"0"))
		      If tempDict.Lookup(anneeMoisStr,"0") = "0" Then 
		        retPaiementFacAnneeResultsDouble(indexAnnee, indexMois-1) = CDbl(valeurPrecedente)
		      Else
		        valeurPrecedente = tempDict.Lookup(anneeMoisStr,"0")
		      End If
		      
		    Next indexMois
		  Next indexAnnee
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub retPaiementFacClientDisp()
		  retPaiementFacClientChartView.Type = retPaiementFacClientChartView.TypePie
		  retPaiementFacClientChartView.DoughnutRadius = 0.25
		  retPaiementFacClientChartView.EnableSelection = True
		  
		  
		  Select Case nbreMoyMaxClient
		  Case "nombre"
		    retPaiementFacClientChartView.Title = RETARDPAIEMENTCLIENT + " - " + NOMBRE
		  Case "moyenne"
		    retPaiementFacClientChartView.Title = RETARDPAIEMENTCLIENT + " - " + MOYENNE
		  Case "maximum"
		    retPaiementFacClientChartView.Title = RETARDPAIEMENTCLIENT + " - " + MAX
		  End Select
		  
		  retPaiementFacClientChartView.Title = retPaiementFacClientChartView.Title + " (" + str(anneeClient) + ")"
		  
		  retPaiementFacClientChartView.BackgroundType = WebChartView.BackgroundSolid
		  retPaiementFacClientChartView.BackgroundColor.append &cFFFFFF
		  
		  Dim Data() As String
		  
		  
		  Data.Append retPaiementFacClientLibelle
		  Data.Append retPaiementFacClientMontant
		  
		  retPaiementFacClientChartView.LegendPosition = WebChartView.Position.BottomVertical
		  
		  retPaiementFacClientChartView.setPieRadarStartAngle(-90)
		  retPaiementFacClientChartView.setDataLabelFormat("#.0%")
		  
		  retPaiementFacClientChartView.LoadFromCSV(Join(Data, EndOfLine))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub retPaiementFacClientExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture des délais de paiement pour tous les clients
		  strSQL = _
		  "SELECT SUM(sumCompteur) AS SumCompteur, SUM(Moy_Nbre_Jours_Diff) AS Moy_Nbre_Jours_Diff, SUM(Max_Jours_Diff) AS Max_Jours_Diff FROM " + _
		  "  (SELECT SUM(Compteur) AS SumCompteur, ROUND(AVG(Jours_Diff),2) AS Moy_Nbre_Jours_Diff, MAX(Jours_Diff) AS Max_Jours_Diff FROM " + _
		  "    (SELECT realiserev_entr AS Entreprise,realiserev_facture AS Facture, realiserev_termes AS Termes, realiserev_date AS Date_facture, " + _
		  "                  realiserev_dernier_paiement AS Date_Paiement, realiserev_jour_interet AS Echeance, realiserev_paiement AS Paiement, " + _
		  "                (CASE WHEN realiserev_dernier_paiement = '' Then CURRENT_DATE - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) As Nb_Jours, " + _
		  "                (CASE WHEN realiserev_dernier_paiement = '' Then CURRENT_DATE - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) - realiserev_jour_interet AS Jours_Diff, " + _
		  "                SUM(1) AS Compteur,            SUM(realiserev_montant) AS SumMontant " + _
		  "   FROM realiserev " + _
		  "  WHERE realiserev_termes <> '' " + _
		  "  GROUP BY Entreprise, Facture, Termes, Date_facture, Date_paiement, Echeance, Paiement, Nb_Jours, Jours_Diff) " + _
		  "  AS RealiseSubTable " + _
		  "  LEFT JOIN client_regroupe ON Entreprise = client_regroupe.client_nom " + _
		  "  WHERE Jours_Diff > 0 " + _
		  "      AND SumMontant > 0 " + _
		  "      AND SUBSTR(Date_facture,1,4) = '" + str(anneeClient) +"' " + _
		  "GROUP BY Client_Regroupe) " + _
		  "AS RealiseTable"
		  
		  //"                (CASE WHEN realiserev_dernier_paiement = '' Then Date '2016-08-01' - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) As Nb_Jours, " + _
		  //"                (CASE WHEN realiserev_dernier_paiement = '' Then Date '2016-08-01' - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) - realiserev_jour_interet AS Jours_Diff, " + _
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  
		  Dim valeurTotale As Double = 0
		  Dim valeurCritere As String = ""
		  Select Case nbreMoyMaxClient
		  Case "nombre"
		    valeurTotale = dashboardRS.Field("SumCompteur").DoubleValue
		    valeurCritere = "SumCompteur"
		  Case "moyenne"
		    valeurTotale = dashboardRS.Field("Moy_Nbre_Jours_Diff").DoubleValue
		    valeurCritere = "Moy_Nbre_Jours_Diff"
		  Case "maximum"
		    valeurTotale = dashboardRS.Field("Max_Jours_Diff").DoubleValue
		    valeurCritere = "Max_Jours_Diff"
		  End Select
		  
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  
		  // Lecture des délais de paiement par année et Client
		  strSQL = _
		  "SELECT client_regroupe.client_groupe AS Client_Regroupe, SUM(Compteur) AS SumCompteur, ROUND(AVG(Jours_Diff),2) AS Moy_Nbre_Jours_Diff, MAX(Jours_Diff) AS Max_Jours_Diff FROM " + _
		  "  (SELECT realiserev_entr AS Entreprise,realiserev_facture AS Facture, realiserev_termes AS Termes, realiserev_date AS Date_facture, " + _
		  "realiserev_dernier_paiement AS Date_Paiement, realiserev_jour_interet AS Echeance, realiserev_paiement AS Paiement, " + _
		  "(CASE WHEN realiserev_dernier_paiement = '' Then Date '2016-08-01' - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) As Nb_Jours, " + _
		  "(CASE WHEN realiserev_dernier_paiement = '' Then Date '2016-08-01' - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) - realiserev_jour_interet AS Jours_Diff, " + _
		  "SUM(1) AS Compteur,            SUM(realiserev_montant) AS SumMontant " + _
		  "FROM realiserev " + _
		  "WHERE realiserev_termes <> '' " + _
		  "GROUP BY Entreprise, Facture, Termes, Date_facture, Date_paiement, Echeance, Paiement, Nb_Jours, Jours_Diff) " + _
		  "AS RealiseTable " + _
		  "LEFT JOIN client_regroupe ON Entreprise = client_regroupe.client_nom " + _
		  "WHERE Jours_Diff > 0 " + _
		  "AND SumMontant > 0 " + _
		  "AND SUBSTR(Date_facture,1,4) = '" + str(anneeClient) +"' " + _
		  "GROUP BY Client_Regroupe " + _
		  "ORDER BY  " + valeurCritere + " DESC"
		  
		  //"SELECT client_regroupe.client_groupe AS Client_Regroupe, SUBSTR(Date_facture,1,4) AS Annee, SUM(Compteur) AS SumCompteur, ROUND(AVG(Jours_Diff),2) AS Moy_Nbre_Jours_Diff, MAX(Jours_Diff) AS Max_Jours_Diff FROM " + _
		  //"  (SELECT realiserev_entr AS Entreprise,realiserev_facture AS Facture, realiserev_termes AS Termes, realiserev_date AS Date_facture, " + _
		  //"realiserev_dernier_paiement AS Date_Paiement, realiserev_jour_interet AS Echeance, realiserev_paiement AS Paiement, " + _
		  //"(CASE WHEN realiserev_dernier_paiement = '' Then CURRENT_DATE - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) As Nb_Jours, " + _
		  //"(CASE WHEN realiserev_dernier_paiement = '' Then CURRENT_DATE - to_date(realiserev_date, 'YYYY-MM-DD') Else to_date(realiserev_dernier_paiement, 'YYYY-MM-DD') - to_date(realiserev_date, 'YYYY-MM-DD') END) - realiserev_jour_interet AS Jours_Diff, " + _
		  //"SUM(1) AS Compteur,            SUM(realiserev_montant) AS SumMontant " + _
		  //"FROM dashboard.realiserev " + _
		  //"WHERE realiserev_termes <> '' " + _
		  //"GROUP BY Entreprise, Facture, Termes, Date_facture, Date_paiement, Echeance, Paiement, Nb_Jours, Jours_Diff) " + _
		  //"AS RealiseTable " + _
		  //"LEFT JOIN dbglobal.client_regroupe ON Entreprise = client_regroupe.client_nom " + _
		  //"WHERE Jours_Diff > 0 " + _
		  //"AND SumMontant > 0 " + _
		  //"AND SUBSTR(Date_facture,1,4) = '" + anneeDepart +"' " + _
		  //"GROUP BY Annee, Client_Regroupe " + _
		  //"ORDER BY  Annee, Client_Regroupe , " + retPaiementFacClientCritere + " DESC"
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  
		  Dim retPaiementFacClientLibelleTemp As String = ""
		  Dim retPaiementFacClientMontantTemp As String = "  value"
		  Dim totFractionMontant As Double = 0.0
		  Dim accMontant As Double = 0.0
		  // Mettre les résultats dans deux Strings pour les 9 premiers clients
		  //For i As Integer = 1 to 9
		  For i As Integer = 1 to 7
		    
		    Dim doubleTemp As Double = 0.0
		    Select Case nbreMoyMaxClient
		    Case "nombre"
		      doubleTemp = dashboardRS.Field("SumCompteur").DoubleValue
		    Case "moyenne"
		      doubleTemp = dashboardRS.Field("Moy_Nbre_Jours_Diff").DoubleValue
		    Case "maximum"
		      doubleTemp = dashboardRS.Field("Max_Jours_Diff").DoubleValue
		    End Select
		    accMontant = accMontant + doubleTemp
		    retPaiementFacClientLibelleTemp = retPaiementFacClientLibelleTemp + "," + dashboardRS.Field("Client_Regroupe").StringValue + " (" + str(doubleTemp) + ")"
		    Dim doubleTemp1 As Double = Round(doubleTemp/valeurTotale*100)/100
		    totFractionMontant = totFractionMontant + doubleTemp1
		    retPaiementFacClientMontantTemp = retPaiementFacClientMontantTemp + "," + str(doubleTemp1)
		    
		    dashboardRS.MoveNext
		  Next i
		  
		  // Calculer le reste
		  retPaiementFacClientLibelleTemp = retPaiementFacClientLibelleTemp + "," + "Autres" + " ( " + str(valeurTotale-accMontant) + ")"
		  retPaiementFacClientMontantTemp = retPaiementFacClientMontantTemp + "," + str(1-totFractionMontant)
		  
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  retPaiementFacClientLibelle = retPaiementFacClientLibelleTemp
		  retPaiementFacClientMontant = retPaiementFacClientMontantTemp
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private anneeClient As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private anneeDepart As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private maxScale As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nbreMoyMaxAnnee As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nbreMoyMaxClient As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private retPaiementFacAnneeResultsDouble(5,12) As Double
	#tag EndProperty

	#tag Property, Flags = &h21
		Private retPaiementFacClientLibelle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private retPaiementFacClientMontant As String
	#tag EndProperty


	#tag Constant, Name = MAX, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Maximum"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Maximum"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Maximum"
	#tag EndConstant

	#tag Constant, Name = MOYENNE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Average"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Moyenne"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Average"
	#tag EndConstant

	#tag Constant, Name = NOMBRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Number"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nombre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Number"
	#tag EndConstant

	#tag Constant, Name = RETARDPAIEMENTCLIENT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"LATE PAYMENT CLIENTS"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"RETARD PAIEMENT CLIENTS"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"LATE PAYMENT CLIENTS"
	#tag EndConstant

	#tag Constant, Name = RETARDPAIEMENTMOIS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"LATE PAYMENT by Month"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"RETARD de PAIEMENT par Mois"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"LATE PAYMENT by Month"
	#tag EndConstant


#tag EndWindowCode

#tag Events AnneeNombreButton
	#tag Event
		Sub Action()
		  nbreMoyMaxAnnee = "nombre"
		  retPaiementFacAnneeExtr
		  retPaiementFacAnneeDisp
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AnneeMoyenneButton
	#tag Event
		Sub Action()
		  nbreMoyMaxAnnee = "moyenne"
		  retPaiementFacAnneeExtr
		  retPaiementFacAnneeDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AnneeMaximumButton
	#tag Event
		Sub Action()
		  nbreMoyMaxAnnee = "maximum"
		  retPaiementFacAnneeExtr
		  retPaiementFacAnneeDisp
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientAnneePlus
	#tag Event
		Sub Action()
		  If anneeClient + 1 > Xojo.Core.Date.Now.Year Then
		    anneeClient = Xojo.Core.Date.Now.Year
		  Else
		    anneeClient = anneeClient + 1
		  End 
		  retPaiementFacClientExtr
		  retPaiementFacClientDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientAnneeMoins
	#tag Event
		Sub Action()
		  If anneeClient - 1 < Xojo.Core.Date.Now.Year - 5 Then
		    anneeClient = Xojo.Core.Date.Now.Year - 5
		  Else
		    anneeClient = anneeClient - 1
		  End 
		  retPaiementFacClientExtr
		  retPaiementFacClientDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientMoyenneButton
	#tag Event
		Sub Action()
		  nbreMoyMaxClient = "moyenne"
		  retPaiementFacClientExtr
		  retPaiementFacClientDisp
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientMaximumButton
	#tag Event
		Sub Action()
		  nbreMoyMaxClient = "maximum"
		  retPaiementFacClientExtr
		  retPaiementFacClientDisp
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientNombreButton
	#tag Event
		Sub Action()
		  nbreMoyMaxClient = "nombre"
		  retPaiementFacClientExtr
		  retPaiementFacClientDisp
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
