#tag Module
Protected Module ProjetModule
	#tag Constant, Name = ANNEEENCOURS, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Current year"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Current year"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ann\xC3\xA9e en cours"
	#tag EndConstant

	#tag Constant, Name = APLANIFIER, Type = String, Dynamic = True, Default = \"\xC3\x80 planifier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x80 planifier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"To be planned"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x80 planifier"
	#tag EndConstant

	#tag Constant, Name = CHARGEPROJET, Type = String, Dynamic = True, Default = \"Charg\xC3\xA9 de projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Charg\xC3\xA9 de projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project manager"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Charg\xC3\xA9 de projet"
	#tag EndConstant

	#tag Constant, Name = CHARGEPROJETCMEQ, Type = String, Dynamic = True, Default = \"Charg\xC3\xA9 de projet CMEQ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Charg\xC3\xA9 de projet CMEQ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"CMEQ project manager"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Charg\xC3\xA9 de projet CMEQ"
	#tag EndConstant

	#tag Constant, Name = CHEFCHANTIER, Type = String, Dynamic = True, Default = \"Chef de Chantier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Chef de Chantier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Site manager"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chef de Chantier"
	#tag EndConstant

	#tag Constant, Name = CHEFEQUIPE, Type = String, Dynamic = True, Default = \"Chef d\'\xC3\xA9quipe", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Chef d\'\xC3\xA9quipe"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Team leader"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chef d\'\xC3\xA9quipe"
	#tag EndConstant

	#tag Constant, Name = CLIENT, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Client"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Client"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Client"
	#tag EndConstant

	#tag Constant, Name = CLIENTCONTACT, Type = String, Dynamic = True, Default = \"Contact client", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Contact client"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Client contact"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Contact client"
	#tag EndConstant

	#tag Constant, Name = CLIENTCOORDONNEES, Type = String, Dynamic = True, Default = \"Coordonn\xC3\xA9es du client", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Coordonn\xC3\xA9es du client"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Client coordinates"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Coordonn\xC3\xA9es du client"
	#tag EndConstant

	#tag Constant, Name = CLIENTNOMCMEQ, Type = String, Dynamic = True, Default = \"Nom client CMEQ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom client CMEQ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"CMEQ client name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom client CMEQ"
	#tag EndConstant

	#tag Constant, Name = CLIENTSITEADRESSE, Type = String, Dynamic = True, Default = \"Adresse du site du client", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Adresse du site du client"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Client site address"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Adresse du site du client"
	#tag EndConstant

	#tag Constant, Name = COMPLETE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Completed"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Completed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Compl\xC3\xA9t\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = DATEDEBUTPREVUE, Type = String, Dynamic = True, Default = \"Date de d\xC3\xA9but pr\xC3\xA9vue", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de d\xC3\xA9but pr\xC3\xA9vue"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Beginning planned date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de d\xC3\xA9but pr\xC3\xA9vue"
	#tag EndConstant

	#tag Constant, Name = DATEDEBUTREELLE, Type = String, Dynamic = True, Default = \"Date de d\xC3\xA9but r\xC3\xA9elle", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de d\xC3\xA9but r\xC3\xA9elle"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Beginning real date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de d\xC3\xA9but r\xC3\xA9elle"
	#tag EndConstant

	#tag Constant, Name = DATEFIN, Type = String, Dynamic = True, Default = \"Date fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date fin"
	#tag EndConstant

	#tag Constant, Name = DATEFINPREVUE, Type = String, Dynamic = True, Default = \"Date de fin pr\xC3\xA9vue", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin pr\xC3\xA9vue"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ending planned date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin pr\xC3\xA9vue"
	#tag EndConstant

	#tag Constant, Name = DATEFINREELLE, Type = String, Dynamic = True, Default = \"Date de fin r\xC3\xA9elle", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin r\xC3\xA9elle"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ending real date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin r\xC3\xA9elle"
	#tag EndConstant

	#tag Constant, Name = ENCOURS, Type = String, Dynamic = True, Default = \"En cours", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"En cours"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"In progress"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"En cours"
	#tag EndConstant

	#tag Constant, Name = ENSUSPEND, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Pending"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pending"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"En suspend"
	#tag EndConstant

	#tag Constant, Name = EQUIPEMENT, Type = String, Dynamic = True, Default = \"\xC3\x89quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89quipement"
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = ESTIMATEUR, Type = String, Dynamic = True, Default = \"Estimateur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Estimateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Estimator"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Estimateur"
	#tag EndConstant

	#tag Constant, Name = ESTIMATEURCMEQ, Type = String, Dynamic = True, Default = \"Estimateur CMEQ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Estimateur CMEQ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"CMEQ estimator"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Estimateur CMEQ"
	#tag EndConstant

	#tag Constant, Name = FORMULAIRE, Type = String, Dynamic = True, Default = \"Formulaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Formulaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Form"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Formulaire"
	#tag EndConstant

	#tag Constant, Name = GENERAL, Type = String, Dynamic = True, Default = \"G\xC3\xA9n\xC3\xA9ral", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"G\xC3\xA9n\xC3\xA9ral"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"General"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"G\xC3\xA9n\xC3\xA9ral"
	#tag EndConstant

	#tag Constant, Name = NOMPROJET, Type = String, Dynamic = True, Default = \"Nom du projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom du projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom du projet"
	#tag EndConstant

	#tag Constant, Name = NOPROJET, Type = String, Dynamic = True, Default = \"No. du projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. du projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project number"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. du projet"
	#tag EndConstant

	#tag Constant, Name = PROJET, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet"
	#tag EndConstant

	#tag Constant, Name = STATUTCMEQ, Type = String, Dynamic = True, Default = \"Statut CMEQ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut CMEQ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"CMEQ status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut CMEQ"
	#tag EndConstant

	#tag Constant, Name = TECHNICIEN, Type = String, Dynamic = True, Default = \"Technicien", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Technicien"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Technician"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Technicien"
	#tag EndConstant

	#tag Constant, Name = TERMINE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Terminated"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Terminated"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Termin\xC3\xA9"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
