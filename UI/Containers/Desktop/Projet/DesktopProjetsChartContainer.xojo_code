#tag WebPage
Begin DesktopAllProjetsChartContainer DesktopProjetsChartContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   584
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin TestWebLabel projet_id_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Id"
      TextAlign       =   0
      Top             =   20
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebTextField projet_id_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   200
      Text            =   ""
      TextAlign       =   0
      Top             =   21
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   59
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebChartView EstimeReelChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   False
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   235
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   10
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   9
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   460
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebChartView CodeGLChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   False
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   310
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   10
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   274
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel EstimeReelTitreLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   150
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1889564671"
      TabOrder        =   2
      Text            =   "#ESTIMEREELTITRE"
      TextAlign       =   0
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   172
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel CodeGLTitreLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   261
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1889564671"
      TabOrder        =   2
      Text            =   "#SUMMARYCODEGLTITRE"
      TextAlign       =   0
      Top             =   270
      VerticalCenter  =   0
      Visible         =   True
      Width           =   453
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h21
		Private Sub codeGLDisp()
		  If codeGLLibelle.Ubound = -1 Then Exit Sub
		  
		  CodeGLChartView.Type = CodeGLChartView.TypeBullet
		  
		  Dim S As new ChartSerie
		  S.Title = AMOUNT
		  S.Data = codeGLMontant
		  //S.Data = Array(8430, 4963, 1581)
		  CodeGLChartView.AddSerie(S)
		  
		  S = New ChartSerie
		  S.Title = ""
		  S.Data = codeGLMontant
		  //S.Data = Array(8500, 5000, 1500)
		  S.FillColor = &c505050
		  S.FillType = ChartSerie.FillPicture
		  
		  CodeGLChartView.AddSerie(S)
		  For i As Integer = 0 To codeGLLibelle.Ubound
		    CodeGLChartView.Axes(0).Label.append codeGLLibelle(i)
		  Next i
		  
		  CodeGLChartView.BackgroundType = WebChartView.BackgroundSolid
		  CodeGLChartView.BackgroundColor.append &cFFFFFF
		  CodeGLChartView.LegendPosition = WebChartView.Position.Right
		  
		  CodeGLChartView.setTransparency(40)
		  
		  CodeGLChartView.Axes(1).Visible = False
		  CodeGLChartView.Axes(1).MaximumScale = codeGLmaxScale
		  CodeGLChartView.Axes(0).AxeLine.visible = False
		  
		  CodeGLChartView.setDataLabel("", 14)
		  CodeGLChartView.DataLabel.Position = 5
		  
		  CodeGLChartView.Redisplay()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub codeGLExtr()
		  Dim strSQL As String = ""
		  codeGLMaxScale = 0
		  Redim codeGLLibelle(-1)
		  Redim codeGLMontant(-1)
		  Dim codeGLLibelleTemp() As String
		  Dim codeGLMontantTemp() As Integer
		  
		  // Lecture du réalisé pour les revenus (Facturation) par code de Grand Livre
		  strSQL = "SELECT realiserev_gl AS GL, realiserev_gl_desc AS Gl_Desc, ROUND(SUM(realiserev_montant),0) As SumMontant " + _
		  "        FROM realiserev " + _ 
		  "       WHERE realiserev_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "       GROUP BY GL, Gl_Desc "+ _ 
		  "       ORDER BY GL, Gl_Desc "
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  While Not  dashboardRS.EOF
		    If dashboardRS.Field("SumMontant").IntegerValue > 0 Then
		      codeGLLibelleTemp.Append(dashboardRS.Field("Gl_Desc").StringValue)
		      codeGLMontantTemp.Append(dashboardRS.Field("SumMontant").IntegerValue)
		      If dashboardRS.Field("SumMontant").IntegerValue > codeGLmaxScale Then codeGLmaxScale =  dashboardRS.Field("SumMontant").IntegerValue
		    End If
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT GL, Gl_Desc, SUM(Montant) As SumMontant  FROM" + _
		  "   (SELECT realisedep_gl AS GL, realisedep_gl_desc AS Gl_Desc, ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + sauvegardeProjetNo +"'"  + _
		  "       GROUP BY GL, Gl_Desc " + _
		  "   UNION " + _
		  "   SELECT '9999' AS GL, 'Salaires et bénéfices marginaux' AS Gl_Desc, ROUND(SUM(realisemocont_salbrut+realisemocont_das+realisemocont_cnesst),0) As Montant " + _
		  "        FROM realisemocont " + _ 
		  "       WHERE realisemocont_projet_no = '" + sauvegardeProjetNo +"'"  + _
		  "       GROUP BY GL, Gl_Desc "+ _
		  "   )" + _
		  "  AS GLDepTable " + _ 
		  "  GROUP BY GL, Gl_Desc " + _ 
		  "  ORDER BY  GL, Gl_Desc "
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  While Not  dashboardRS.EOF
		    If dashboardRS.Field("SumMontant").IntegerValue > 0 Then
		      codeGLLibelleTemp.Append(dashboardRS.Field("Gl_Desc").StringValue)
		      codeGLMontantTemp.Append(dashboardRS.Field("SumMontant").IntegerValue)
		      If dashboardRS.Field("SumMontant").IntegerValue > codeGLmaxScale Then codeGLmaxScale =  dashboardRS.Field("SumMontant").IntegerValue
		    End If
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  If codeGLLibelleTemp.Ubound > -1 Then
		    codeGLLibelle = codeGLLibelleTemp
		    codeGLMontant = codeGLMontantTemp
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub estimeReelDisp()
		  EstimeReelChartView.Type = EstimeReelChartView.TypeBullet
		  
		  Dim S As new ChartSerie
		  S.Title = ACTUAL
		  S.Data = Array(reelMontantTotalFacture, reelMontantTotalMO, reelMontantTotalMAT, reelProfit)
		  //S.Data = Array(8430, 4963, 1581)
		  
		  EstimeReelChartView.AddSerie(S)
		  
		  S = New ChartSerie
		  S.Title = ESTIMATE
		  If estimeMontantFacture <> 0 Then
		    S.Data = Array(estimeMontantFacture, estimeMontantMO, estimeMontantMAT, estimeProfit)
		  Else
		    S.Data = Array(estimeMontantFactureExtra, estimeMontantMO, estimeMontantMAT, estimeProfit)
		  End If
		  //S.Data = Array(8500, 5000, 1500)
		  S.FillColor = &c505050
		  'S.FillType = ChartSerie.FillPicture
		  
		  EstimeReelChartView.AddSerie(S)
		  
		  EstimeReelChartView.BackgroundType = WebChartView.BackgroundSolid
		  EstimeReelChartView.BackgroundColor.append &cFFFFFF
		  
		  EstimeReelChartView.Axes(0).Label.append REVENUE
		  EstimeReelChartView.Axes(0).Label.append MANPOWER
		  EstimeReelChartView.Axes(0).Label.append MATERIAL
		  EstimeReelChartView.Axes(0).Label.append PROFIT
		  
		  EstimeReelChartView.LegendPosition = WebChartView.Position.Right
		  
		  EstimeReelChartView.setTransparency(40)
		  
		  EstimeReelChartView.Axes(1).Visible = False
		  EstimeReelChartView.Axes(1).MaximumScale = estimeReelMaxScale
		  EstimeReelChartView.Axes(0).AxeLine.visible = False
		  
		  EstimeReelChartView.setDataLabel("", 14)
		  EstimeReelChartView.DataLabel.Position = 5
		  
		  EstimeReelChartView.Redisplay()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub estimeReelExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture de l'estimé pour le revenu total
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "          AND SUBSTRING(estime_rev_mo_mat,1,2) <> 'AJ' " 
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantFacture = 0
		  If dashboardRS.Field("MontantAvecProfit").Value <> Nil Then
		    estimeMontantFacture = dashboardRS.Field("MontantAvecProfit").IntegerValue
		  End If
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture de l'estimé pour le revenu  des demandes de changement
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "          AND estime_rev_mo_mat = 'AJREV' " 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantFactureExtra = 0
		  If dashboardRS.Field("MontantAvecProfit").Value <> Nil Then
		    estimeMontantFactureExtra = dashboardRS.Field("MontantAvecProfit").IntegerValue
		  End If
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Arrondir estimeReelMaxScaleTemp à la centaine supérieure
		  If estimeMontantFactureExtra > estimeMontantFacture Then
		    estimeReelMaxScale = Globals.arrondirCentaineSuperieure(estimeMontantFactureExtra)
		  Else
		    estimeReelMaxScale = Globals.arrondirCentaineSuperieure(estimeMontantFacture)
		  End If
		  
		  // Lecture de l'estimé pour la main d'oeuvre
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + sauvegardeProjetNo +"'"   + _
		  "          AND estime_rev_mo_mat    = 'MO'" 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantMO = 0
		  If dashboardRS.Field("Montant").Value <> Nil Then
		    estimeMontantMO = dashboardRS.Field("Montant").IntegerValue
		  End If
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture de l'estimé pour le matériel
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + sauvegardeProjetNo +"'"   + _
		  "          AND estime_rev_mo_mat    = 'MAT'" 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  estimeMontantMAT = dashboardRS.Field("Montant").IntegerValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Calcul du profit estimé
		  estimeProfit = estimeMontantFacture - estimeMontantMAT - estimeMontantMO
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = "SELECT ROUND(SUM(realiserev_montant),0) As Montant, ROUND(SUM(realiserev_paiement),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE realiserev_projet_no = '" + sauvegardeProjetNo +"'" 
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  reelMontantTotalFacture = 0
		  If dashboardRS.Field("Montant").Value <> Nil Then
		    reelMontantTotalFacture = dashboardRS.Field("Montant").IntegerValue
		  End If
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  If estimeReelMaxScale < reelMontantTotalFacture Then
		    estimeReelMaxScale =  Globals.arrondirCentaineSuperieure(reelMontantTotalFacture)
		  End If
		  If estimeMontantFacture = 0 Then estimeMontantFacture = reelMontantTotalFacture
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre
		  strSQL = _
		  "SELECT SUM(Montant) As Montant  FROM" + _
		  "   (SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + sauvegardeProjetNo +"'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) = '6' "  + _
		  "   UNION " + _
		  "   SELECT ROUND(SUM(realisemocont_salbrut+realisemocont_das+realisemocont_cnesst),0) As Montant " + _
		  "        FROM realisemocont " + _ 
		  "       WHERE realisemocont_projet_no = '" + sauvegardeProjetNo +"'" + _
		  "   )" + _
		  "  AS MOTable "
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  reelMontantTotalMO = 0
		  If dashboardRS.Field("Montant").Value <> Nil Then
		    reelMontantTotalMO = dashboardRS.Field("Montant").IntegerValue
		  End If
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture du réalisé pour les dépenses de matériel
		  strSQL = _
		  "SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + sauvegardeProjetNo +"'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) IN('5','8') " 
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  reelMontantTotalMAT = 0
		  If dashboardRS.Field("Montant").Value <> Nil Then
		    reelMontantTotalMAT = dashboardRS.Field("Montant").IntegerValue
		  End If
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Calcul du profit réalisé
		  reelProfit = reelMontantTotalFacture - reelMontantTotalMAT - reelMontantTotalMO
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listProjetDetail()
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, True, True, True)
		  
		  Dim pointeurTemp As DeskTopProjetSideBar = Session.pointeurDeskTopProjetContainerControl.DeskTopProjetSideBarDialog
		  
		  // Mode Modification, Lire les données
		  Dim projetRS As RecordSet = pointeurTemp.sProjet.loadDataByField(Session.bdGroupeRPF, Self.tableName, _
		  Self.prefix + "_id", str(pointeurTemp.projetStruct.id), Self.prefix + "_id")
		  If projetRS <> Nil Then pointeurTemp.sProjet.LireDonneesBD(projetRS, Self.prefix)
		  
		  // Sauvegarde
		  sauvegardeProjetNo = projetRS.Field(Self.prefix + "_no").StringValue
		  Dim projet_no As String = sauvegardeProjetNo
		  pointeurTemp.projetStruct.id = projetRS.Field(Self.prefix + "_id").IntegerValue
		  projetRS.Close
		  projetRS = Nil
		  
		  // Graphique estimé
		  estimeReelExtr
		  EstimeReelDisp
		  // Graphique par code de grand livre 
		  codeGLExtr
		  codeGLDisp
		  
		  // Assignation des propriétés aux Contrôles
		  pointeurTemp.sProjet.assignPropertiesToControls(Self, Self.prefix)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, False, True)
		  
		  
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private codeGLLibelle() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private codeGLMaxScale As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private codeGLMontant() As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private codeGLMontantAvecProfit As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantFacture As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantFactureExtra As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantMAT As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeMontantMO As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeProfit As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private estimeReelMaxScale As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelMontantTotalFacture As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelMontantTotalMAT As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelMontantTotalMO As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private reelProfit As Integer
	#tag EndProperty


	#tag Constant, Name = ACTUAL, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Actual"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9el"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Actual"
	#tag EndConstant

	#tag Constant, Name = AMOUNT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Amount"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Montant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Amount"
	#tag EndConstant

	#tag Constant, Name = DIVERSIONFROMESTIMATETITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Diversion from Estimate"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89cart de l\'Estim\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Diversion from Estimate"
	#tag EndConstant

	#tag Constant, Name = ESTIMATE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Estimate"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Estim\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Estimate"
	#tag EndConstant

	#tag Constant, Name = ESTIMEREELTITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ESTIMATE vs ACTUAL"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"ESTIM\xC3\x89 vs R\xC3\x89EL"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"ESTIMATE vs ACTUAL"
	#tag EndConstant

	#tag Constant, Name = EXTRAS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Extras"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Extras"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Extras"
	#tag EndConstant

	#tag Constant, Name = MANPOWER, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Manpower"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Main d\'oeuvre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Manpower"
	#tag EndConstant

	#tag Constant, Name = MATERIAL, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Material"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mat\xC3\xA9riel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Material"
	#tag EndConstant

	#tag Constant, Name = PROFIT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Profit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Profit"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Profit"
	#tag EndConstant

	#tag Constant, Name = REVENUE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Revenue"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Revenu"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Revenue"
	#tag EndConstant

	#tag Constant, Name = REVENUTOTALTITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Total Revenue"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Revenu Total"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Total Revenue"
	#tag EndConstant

	#tag Constant, Name = SUMMARYCODEGLTITRE, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"SUMMARY BY GENERAL LEDGER CODE"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"SOMMAIRE PAR CODE DE GRAND LIVRE"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SUMMARY BY GENERAL LEDGER CODE"
	#tag EndConstant


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="prefix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tableName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
