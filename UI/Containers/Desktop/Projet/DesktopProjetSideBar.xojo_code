#tag WebPage
Begin WebContainer DesktopProjetSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   640
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebListBox ProjetListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   2
      ColumnWidths    =   "30%,70%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "1065947135"
      Height          =   585
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#ProjetModule.NOPROJET	#ProjetModule.NOMPROJET		"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "1065947135"
      Style           =   "1065947135"
      TabOrder        =   -1
      Top             =   55
      VerticalCenter  =   0
      Visible         =   True
      Width           =   350
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ProjetLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "#ProjetModule.PROJET"
      TextAlign       =   0
      Top             =   12
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox ProjetEnCoursCheckbox
      Caption         =   "#ProjetModule.ENCOURS"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   103
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   5
      Value           =   True
      VerticalCenter  =   0
      Visible         =   True
      Width           =   98
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox ProjetSuspendCheckbox
      Caption         =   "#ProjetModule.ENSUSPEND"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   103
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   31
      Value           =   True
      VerticalCenter  =   0
      Visible         =   True
      Width           =   98
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox AnneeEnCoursCheckbox
      Caption         =   "#ProjetModule.ANNEEENCOURS"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   213
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   31
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   117
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox ProjetCompleteCheckbox
      Caption         =   "#ProjetModule.COMPLETE"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   213
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   5
      Value           =   True
      VerticalCenter  =   0
      Visible         =   True
      Width           =   98
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementProjet(idProjet_param As Integer)
		  
		  // Restaurer la valeur de la clé du record 
		  projetStruct.id = ProjetListBox.CellTag(idProjet_param,1)
		  
		  // Libérer l'espace mémoire si c'est le cas
		  If Session.pointeurDeskTopAllProjetsChartContainer <> Nil Then
		    Session.pointeurDeskTopAllProjetsChartContainer.Close
		    Session.pointeurDeskTopAllProjetsChartContainer = Nil
		    //Session.pointeurFormulaireContainerControl  = Nil
		  End If
		  
		  
		  // Pointeur sur le Container père
		  Session.pointeurDeskTopProjetContainerControl = DesktopProjetContainerControl(Self.Parent)
		  Dim hauteurPanneauProjetSideBar As Integer = Session.pointeurDeskTopProjetContainerControl.DeskTopProjetAllProjetContainer.Height
		  Dim largeurPanneauProjetSideBar As Integer = Session.pointeurDeskTopProjetContainerControl.DeskTopProjetAllProjetContainer.Width
		  
		  // Afficher le panneau général 
		  Dim projetContainer As DesktopProjetsChartContainer = New DesktopProjetsChartContainer
		  Session.pointeurDeskTopAllProjetsChartContainer = projetContainer
		  projetContainer.EmbedWithin(Session.pointeurDeskTopProjetContainerControl.DeskTopProjetAllProjetContainer, 0, 0, largeurPanneauProjetSideBar, hauteurPanneauProjetSideBar)
		  projetContainer.LockTop = True
		  projetContainer.LockBottom = True
		  projetContainer.LockLeft = True
		  projetContainer.LockRight = True
		  projetContainer.prefix = "projet"
		  projetContainer.tableName = "projet"
		  sProjet = Nil
		  sProjet = New ProjetClass()
		  projetContainer.listProjetDetail
		  
		  
		  //Select Case panneauAafficher
		  //
		  //Case "General" // Panneau général
		  //// Autoriser la mise à jour
		  //// Afficher le panneau général
		  //Dim projetContainer As DesktopProjetContainer = New DesktopProjetContainer
		  //Session.pointeurDeskTopAllProjetsChartContainer = projetContainer
		  //projetContainer.EmbedWithin(Session.pointeurDeskTopProjetContainerControl.DeskTopProjetAllProjetContainer, 0, 0, 1000, hauteurPanneauProjetSideBar)
		  //projetContainer.LockTop = True
		  //projetContainer.LockBottom = True
		  //projetContainer.LockLeft = True
		  //projetContainer.LockRight = True
		  //projetContainer.prefix = "projet"
		  //projetContainer.tableName = "projet"
		  //sProjet = Nil
		  //sProjet = New ProjetClass()
		  //projetContainer.listProjetDetail
		  //
		  //Case "Formulaire" // Panneau formulaire
		  //// Afficher le panneau formulaire
		  //Dim formulaireContainerControl As FormulaireContainerControl = New FormulaireContainerControl
		  //Session.pointeurDeskTopAllProjetsChartContainer = formulaireContainerControl
		  //Session.pointeurFormulaireContainerControl = formulaireContainerControl
		  //formulaireContainerControl.EmbedWithin(Session.pointeurDeskTopProjetContainerControl.DeskTopProjetAllProjetContainer, 0, 0, 1000, hauteurPanneauProjetSideBar)
		  //formulaireContainerControl.LockTop = True
		  //formulaireContainerControl.LockBottom = True
		  //formulaireContainerControl.LockLeft = True
		  //formulaireContainerControl.LockRight = True
		  //Dim aaa As String = ProjetListBox.Cell(ProjetListBox.ListIndex, 0)
		  //Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.projet_numero = ProjetListBox.Cell(ProjetListBox.ListIndex, 0)
		  //formulaireContainerControl.FormulaireSideBarDialog.populateFormulaire
		  //End Select 
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function genClauseWhere(listeSelection_param As Dictionary) As String
		  
		  Dim listeSelection As String = ""
		  Dim keys(), k, v as variant
		  
		  keys = listeSelection_param.keys()
		  for each k in keys
		    Select Case k
		      
		    Case "DateDebut"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_date >= '" + v + "'  "
		      
		    Case "DateFin"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_date <= '" + v + "'  "
		      
		    Case "Projet"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_projet IN('" + v + "')  "
		      
		    Case "Revision"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_revision IN(" + v + ")  "
		      
		    End Select
		    
		  Next
		  
		  Return listeSelection
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modProjetDetail()
		  
		  Dim ptrProjetCont As DesktopProjetsChartContainer = DesktopProjetsChartContainer(Session.pointeurDeskTopAllProjetsChartContainer)
		  ptrProjetCont.traiteMAJ
		  // Forcer le réaffichage
		  changementProjet(ProjetListBox.ListIndex)
		  
		  Dim ML6MajModalBox As New MajModal
		  ML6MajModalBox.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateProjet(Optional projet_id_param as Integer)
		  
		  If Self.sProjet = Nil Then
		    Self.sProjet = new ProjetClass(Session.bdGroupeRPF, Self.PROJETTABLENAME, Self.PROJETPREFIX)
		  End If
		  
		  Dim projetRS As RecordSet
		  projetRS = Self.sProjet.loadData(Session.bdGroupeRPF, Self.PROJETTABLENAME, "projet_no")
		  
		  ProjetListBox.DeleteAllRows
		  
		  If projetRS = Nil Then Exit Sub
		  
		  Dim index As Integer = 0
		  For i As Integer = 1 To projetRS.RecordCount
		    If (Me.ProjetEnCoursCheckbox.Value = False  And Me.ProjetCompleteCheckbox.Value = False) Or _
		      (projetRS.Field("projet_statut").StringValue = "Travaux en cours" And Me.ProjetEnCoursCheckbox.Value = True)      Or _
		      (projetRS.Field("projet_statut").StringValue = "En suspens" And Me.ProjetSuspendCheckbox.Value = True) Or _
		      (projetRS.Field("projet_statut").StringValue = "Travaux complétés" And Me.ProjetCompleteCheckbox.Value = True) Then
		      
		      Dim date_test As String =  projetRS.Field("projet_dernier_enregistrement").StringValue
		      If Me.AnneeEnCoursCheckbox.Value = False Or _
		        (Me.AnneeEnCoursCheckbox.Value = True And Mid(projetRS.Field("projet_dernier_enregistrement").StringValue,1,4) = str(Xojo.Core.Date.Now.Year)) Then
		        ProjetListBox.AddRow(projetRS.Field("projet_no").StringValue, projetRS.Field("projet_titre").StringValue)
		        // Garder le numéro de ligne
		        ProjetListBox.RowTag(ProjetListBox.LastIndex) = index
		        // Garder l'id numéro du projet
		        ProjetListBox.CellTag(ProjetListBox.LastIndex,1) = projetRS.Field("projet_id").IntegerValue
		        index = index + 1
		      End If
		    End If
		    projetRS.MoveNext
		  Next
		  projetRS.Close
		  projetRS = Nil
		  
		  // Si la liste est vide, sortir
		  If ProjetListBox.RowCount <= 0 Then Exit Sub
		  
		  // Positionnement sur le premier projet
		  ProjetListBox.ListIndex = 0
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		panneauAafficher As String = "General"
	#tag EndProperty

	#tag Property, Flags = &h0
		projetStruct As ProjetStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sProjet As ProjetClass
	#tag EndProperty


	#tag Constant, Name = PROJETPREFIX, Type = String, Dynamic = True, Default = \"projet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PROJETSCHEMA, Type = String, Dynamic = True, Default = \"dbglobal", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PROJETTABLENAME, Type = String, Dynamic = True, Default = \"projet", Scope = Private
	#tag EndConstant


	#tag Structure, Name = ProjetStructure, Flags = &h0
		projet As String*12
		  date As String*100
		  projet_form_id As String*10
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		revision As String*1
	#tag EndStructure


#tag EndWindowCode

#tag Events ProjetListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    projetStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    projetStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementProjet(Me.ListIndex)
		  End If
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ProjetEnCoursCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des projets
		  populateProjet
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ProjetSuspendCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des projets
		  populateProjet
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AnneeEnCoursCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des projets
		  populateProjet
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ProjetCompleteCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des projets
		  populateProjet
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="panneauAafficher"
		Group="Behavior"
		InitialValue="General"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
