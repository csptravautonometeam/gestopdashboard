#tag WebPage
Begin DesktopAllChartsContainerControl DesktopTousProjetsChartContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   584
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebChartView RevTypeGLChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   240
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   20
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   20
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   738
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebChartView RevAnneeChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   266
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   20
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   318
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   740
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebChartView RevAnneeChartViewBis
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   266
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   20
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   318
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   740
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebChartView RevClientChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   526
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   905
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   20
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   425
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton TypeGLAnneePlus
      AutoDisable     =   False
      Caption         =   "<"
      Cursor          =   0
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   272
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton TypeGLAnneeMoins
      AutoDisable     =   False
      Caption         =   ">"
      Cursor          =   0
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   698
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   272
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientAnneePlus
      AutoDisable     =   False
      Caption         =   "<"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   905
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   558
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton ClientAnneeMoins
      AutoDisable     =   False
      Caption         =   ">"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1270
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   558
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub listAllProjetsDetail()
		  Dim anneeFin, moisFin As String
		  
		  // Extraire Année et Mois passé
		  Dim interval1 As New Xojo.Core.DateInterval
		  interval1.Months = 1
		  Dim lastMonth As Xojo.Core.Date = Xojo.Core.Date.Now - interval1
		  anneeFin = str(lastMonth.Year)
		  moisFin = str(lastMonth.Month)
		  Dim interval2 As New Xojo.Core.DateInterval
		  interval2.Years = 4
		  Dim firstYear As Xojo.Core.Date = lastMonth - interval2
		  anneeDepart = firstYear.Year
		  //anneeDepart = 2012
		  anneeClient = Xojo.Core.Date.Now.Year
		  //anneeClient = 2016
		  anneeTypeGL = Xojo.Core.Date.Now.Year
		  //anneeTypeGL = 2016
		  
		  // Graphique 
		  revTypeGLExtr
		  revTypeGLDisp
		  
		  // Graphique revenu par année
		  revAnneeExtr
		  revAnneeDisp(RevAnneeChartViewBis)
		  revAnneeDisp(RevAnneeChartView)
		  
		  // Revenu par client
		  revClientExtr
		  revClientDisp
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub revAnneeDisp(webChartView_param As WebChartView)
		  webChartView_param.Freeze = True
		  
		  webChartView_param.Title = VARREVBRUTMOIS
		  webChartView_param.BackgroundType = WebChartView.BackgroundSolid
		  webChartView_param.BackgroundColor.append &cFFFFFF
		  webChartView_param.Type = webChartView_param.TypeLine
		  webChartView_param.FollowValues = True
		  webChartView_param.FollowAllSeries = True
		  
		  Dim S As ChartSerie
		  Dim revAnneeResultsDoubleTrav() As Double
		  
		  For index As Integer = 0 To 4
		    S = New ChartSerie
		    S.Title = str(anneeDepart+index)
		    ReDim revAnneeResultsDoubleTrav(-1)
		    For i As Integer = 0 To 11
		      revAnneeResultsDoubleTrav.append(revAnneeResultsDouble(index,i))
		    Next i
		    S.Data = revAnneeResultsDoubleTrav
		    S.LineWeight = 2
		    S.MarkerType = S.MarkerNone
		    //S.Shadow = True
		    //S.ShadowColor = &c000000C3
		    webChartView_param.AddSerie(S)
		  Next index
		  
		  Dim i As Integer
		  Dim d As date = New Date
		  d.Day = 1
		  For i  = 1 to 12
		    d.Month = i
		    webChartView_param.Axes(0).Label.append New Date(d)
		  Next
		  webChartView_param.Axes(0).AutoLabel = False
		  webChartView_param.Axes(0).LabelStyle.Format = "M"
		  webChartView_param.setTransparency(40)
		  webChartView_param.Axes(1).MajorGridLine = New ChartLine
		  webChartView_param.Axes(1).MajorGridLine.FillColor = &cE4E4E4
		  webChartView_param.Axes(1).MaximumScale = maxScale
		  //webChartView_param.Axes(1).MaximumScale = 600
		  webChartView_param.Axes(1).MajorUnit = 100
		  webChartView_param.Axes(1).LabelStyle.Format = " k"
		  webChartView_param.Axes(0).LabelStyle.TextSize = 14
		  webChartView_param.Axes(0).LabelInterval = -1
		  webChartView_param.LegendPosition = WebChartView.Position.Right
		  
		  webChartView_param.HighlightMinValue(&cFF0000, -1)
		  
		  webChartView_param.Freeze = False
		  webChartView_param.Animate = True
		  webChartView_param.Redisplay()
		  'webChartView_param.StartAnimation(True, ChartAnimate.kEaseOutCirc)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub revAnneeExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = "SELECT SUBSTR(realiserev_date,1,4) AS Annee, SUBSTR(realiserev_date,6,2) AS Mois, ROUND(SUM(realiserev_montant)/1000,0) As Montant, ROUND(SUM(realiserev_paiement/1000),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE SUBSTR(realiserev_date,1,4) >= '" + str(anneeDepart) +"'" + _
		  "           AND SUBSTR(realiserev_gl,1,1) = '4' "  + _
		  "        GROUP BY Annee, Mois " + _
		  "        ORDER BY  Annee, Mois "
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  
		  Dim tempDict As New Dictionary
		  maxScale = 0
		  // Mettre les résultats dans un dictionnaire et trouver la plus grande valeur
		  While Not  dashboardRS.EOF
		    tempDict.Value(dashboardRS.Field("Annee").StringValue + dashboardRS.Field("Mois").StringValue) =  dashboardRS.Field("Montant").StringValue
		    If dashboardRS.Field("Montant").IntegerValue > maxScale Then maxScale =  dashboardRS.Field("Montant").IntegerValue
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Arrondir maxScaleTemp à la centaine supérieure
		  Dim maxScaleTemp As String = str(maxScale)
		  Dim maxScaleDouble As Double = maxScale
		  Dim longueur As Integer = Len(maxScaleTemp)
		  maxScale = Floor((maxScaleDouble + 10 ^ (longueur - 2)) / 10 ^ (longueur - 2)) * 10 ^ (longueur - 2)
		  
		  // Compléter les valeurs
		  Dim valeurPrecedente As String = ""
		  For indexAnnee As Integer = 0 To 4
		    Dim anneeStr As String = str(anneeDepart + indexAnnee)
		    For indexMois As Integer = 1 To 12
		      Dim moisStr As String = str(indexMois)
		      If indexMois < 10 Then moisStr = "0" + moisStr
		      Dim anneeMoisStr As String = anneeStr + moisStr 
		      revAnneeResultsDouble(indexAnnee, indexMois-1) = CDbl(tempDict.Lookup(anneeMoisStr,"0"))
		      If tempDict.Lookup(anneeMoisStr,"0") = "0" Then 
		        revAnneeResultsDouble(indexAnnee, indexMois-1) = CDbl(valeurPrecedente)
		      Else
		        valeurPrecedente = tempDict.Lookup(anneeMoisStr,"0")
		      End If
		    Next indexMois
		  Next indexAnnee
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub revClientDisp()
		  RevClientChartView.Type = RevClientChartView.TypePie
		  RevClientChartView.DoughnutRadius = 0.25
		  RevClientChartView.EnableSelection = True
		  
		  
		  RevClientChartView.Title = GROSSREVBYCLIENT + " (" + str(anneeClient) + ")"
		  RevClientChartView.BackgroundType = WebChartView.BackgroundSolid
		  RevClientChartView.BackgroundColor.append &cFFFFFF
		  
		  Dim Data() As String
		  
		  
		  Data.Append resClientLibelle
		  Data.Append resClientMontant
		  
		  RevClientChartView.LegendPosition = WebChartView.Position.BottomVertical
		  
		  RevClientChartView.setPieRadarStartAngle(-90)
		  RevClientChartView.setDataLabelFormat("#.0%")
		  
		  RevClientChartView.LoadFromCSV(Join(Data, EndOfLine))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub revClientExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les revenus (Facturation) pour tous les clients
		  strSQL = "SELECT ROUND(SUM(realiserev_montant)/1000,0) As Montant, ROUND(SUM(realiserev_paiement/1000),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE SUBSTR(realiserev_date,1,4) = '" + str(anneeClient) +"'" + _
		  "           AND SUBSTR(realiserev_gl,1,1) = '4' " 
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  Dim montantTotal As Double = dashboardRS.Field("Montant").DoubleValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  
		  // Lecture du réalisé pour les revenus (Facturation) par Client
		  strSQL = "SELECT client_regroupe.client_groupe AS Client, ROUND(SUM(realiserev_montant)/1000,0) As Montant, ROUND(SUM(realiserev_paiement/1000),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "        LEFT JOIN client_regroupe ON realiserev.realiserev_entr = client_regroupe.client_nom " + _ 
		  "       WHERE SUBSTR(realiserev_date,1,4) = '" + str(anneeClient) +"'" + _
		  "           AND SUBSTR(realiserev_gl,1,1) = '4' "  + _
		  "        GROUP BY Client " + _
		  "        ORDER BY  Montant DESC "
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  
		  Dim resClientLibelleTemp As String = ""
		  Dim resClientMontantTemp As String = "  value"
		  Dim totFractionMontant As Double = 0.0
		  Dim accMontant As Double = 0.0
		  // Mettre les résultats dans deux Strings pour les 7 premiers clients
		  For i As Integer = 1 to 7
		    resClientLibelleTemp = resClientLibelleTemp + "," + dashboardRS.Field("Client").StringValue + " ($" + dashboardRS.Field("Montant").StringValue + " k)"
		    accMontant = accMontant + dashboardRS.Field("Montant").DoubleValue
		    Dim doubleTemp As Double = dashboardRS.Field("Montant").DoubleValue/montantTotal
		    Dim doubleTemp1 As Double = Round(doubleTemp*10000)/10000
		    totFractionMontant = totFractionMontant + doubleTemp1
		    resClientMontantTemp = resClientMontantTemp + "," + str(doubleTemp1)
		    dashboardRS.MoveNext
		  Next i
		  
		  // Calculer le reste
		  resClientLibelleTemp = resClientLibelleTemp + "," + "Autres" + " ( $" + str(montantTotal-accMontant) + ")"
		  resClientMontantTemp = resClientMontantTemp + "," + str(1-totFractionMontant)
		  
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  resClientLibelle = resClientLibelleTemp
		  resClientMontant = resClientMontantTemp
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub revTypeGLDisp()
		  
		  RevTypeGLChartView.Type = RevTypeGLChartView.TypePie
		  //RevTypeGLChartView.DoughnutRadius = 0.25
		  RevTypeGLChartView.DoughnutRadius = 0.50
		  RevTypeGLChartView.EnableSelection = True
		  
		  RevTypeGLChartView.Title = GROSSREVBYGL  + " (" + str(anneeTypeGL) + ")"
		  RevTypeGLChartView.BackgroundType = WebChartView.BackgroundSolid
		  RevTypeGLChartView.BackgroundColor.append &cFFFFFF
		  
		  Dim Data() As String
		  
		  Data.Append resTypeGLLibelle
		  Data.Append resTypeGLMontant
		  
		  RevTypeGLChartView.LegendPosition = WebChartView.Position.Right
		  
		  RevTypeGLChartView.setPieRadarStartAngle(-90)
		  RevTypeGLChartView.setDataLabelFormat("#.0%")
		  
		  RevTypeGLChartView.LoadFromCSV(Join(Data, EndOfLine))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub revTypeGLExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé pour les revenus (Facturation) pour tous les Types
		  strSQL = "SELECT ROUND(SUM(realiserev_montant)/1000,0) As Montant, ROUND(SUM(realiserev_paiement/1000),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE SUBSTR(realiserev_date,1,4) = '" + str(anneeTypeGL) +"'" + _
		  "           AND SUBSTR(realiserev_gl,1,1) = '4' " 
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  dashboardRS.MoveFirst
		  Dim montantTotal As Double = dashboardRS.Field("Montant").DoubleValue
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  
		  // Lecture du réalisé pour les revenus (Facturation) par Types
		  strSQL = "SELECT gltype_regroupe.gltype_groupe AS TypeRevDesc , ROUND(SUM(realiserev_montant)/1000,0) As Montant, ROUND(SUM(realiserev_paiement/1000),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "        LEFT JOIN gltype_regroupe ON realiserev.realiserev_gl_desc = gltype_regroupe.gltype_nom " + _ 
		  "       WHERE SUBSTR(realiserev_date,1,4) = '" + str(anneeTypeGL) +"'" + _
		  "           AND SUBSTR(realiserev_gl,1,1) = '4' "  + _
		  "        GROUP BY TypeRevDesc " + _
		  "        ORDER BY  Montant DESC "
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  
		  Dim resTypeGLLibelleTemp As String = ""
		  Dim resTypeGLMontantTemp As String = "  value"
		  Dim totFractionMontant As Double = 0.0
		  // Mettre les résultats dans deux Strings pour les 9 premiers types
		  For i As Integer = 1 to 9
		    resTypeGLLibelleTemp = resTypeGLLibelleTemp + "," + dashboardRS.Field("TypeRevDesc").StringValue
		    Dim doubleTemp As Double = dashboardRS.Field("Montant").DoubleValue/montantTotal
		    Dim doubleTemp1 As Double = Round(doubleTemp*10000)/10000
		    totFractionMontant = totFractionMontant + doubleTemp1
		    resTypeGLMontantTemp = resTypeGLMontantTemp + "," + str(doubleTemp1)
		    dashboardRS.MoveNext
		  Next i
		  
		  // Calculer le reste
		  resTypeGLLibelleTemp = resTypeGLLibelleTemp + "," + "Autres"
		  resTypeGLMontantTemp = resTypeGLMontantTemp + "," + str(1-totFractionMontant)
		  
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  resTypeGLLibelle = resTypeGLLibelleTemp
		  resTypeGLMontant = resTypeGLMontantTemp
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private anneeClient As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private anneeDepart As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private anneeTypeGL As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private maxScale As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private resClientLibelle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private resClientMontant As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private resTypeGLLibelle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private resTypeGLMontant As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private revAnneeResultsDouble(5,12) As Double
	#tag EndProperty


	#tag Constant, Name = GROSSREVBYCLIENT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"GROSS REVENUE by Client group"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"REVENU BRUT par CLIENT regroup\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"GROSS REVENUE by Client group"
	#tag EndConstant

	#tag Constant, Name = GROSSREVBYGL, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"GROSS REVENUE by Revenue Code"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"REVENU BRUT par code de Revenu"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"GROSS REVENUE by Revenue Code"
	#tag EndConstant

	#tag Constant, Name = VARREVBRUTMOIS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"GROSS REVENUE VARIATION by MONTH ($thousand)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"VARIATION DU REVENU BRUT par MOIS ($mille)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"GROSS REVENUE VARIATION by MONTH ($thousand)"
	#tag EndConstant


#tag EndWindowCode

#tag Events TypeGLAnneePlus
	#tag Event
		Sub Action()
		  If anneeTypeGL + 1 > Xojo.Core.Date.Now.Year Then
		    anneeTypeGL = Xojo.Core.Date.Now.Year
		  Else
		    anneeTypeGL = anneeTypeGL + 1
		  End 
		  revTypeGLExtr
		  revTypeGLDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TypeGLAnneeMoins
	#tag Event
		Sub Action()
		  If anneeTypeGL - 1 < Xojo.Core.Date.Now.Year - 5 Then
		    anneeTypeGL = Xojo.Core.Date.Now.Year - 5
		  Else
		    anneeTypeGL = anneeTypeGL - 1
		  End 
		  revTypeGLExtr
		  revTypeGLDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientAnneePlus
	#tag Event
		Sub Action()
		  If anneeClient + 1 > Xojo.Core.Date.Now.Year Then
		    anneeClient = Xojo.Core.Date.Now.Year
		  Else
		    anneeClient = anneeClient + 1
		  End 
		  revClientExtr
		  revClientDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClientAnneeMoins
	#tag Event
		Sub Action()
		  If anneeClient - 1 < Xojo.Core.Date.Now.Year - 5 Then
		    anneeClient = Xojo.Core.Date.Now.Year - 5
		  Else
		    anneeClient = anneeClient - 1
		  End 
		  revClientExtr
		  revClientDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
