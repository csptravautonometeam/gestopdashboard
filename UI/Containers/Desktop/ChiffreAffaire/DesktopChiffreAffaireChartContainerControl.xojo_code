#tag WebPage
Begin DesktopAllChartsContainerControl DesktopChiffreAffaireChartContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   584
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebChartView ChifAffAnneeChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   True
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   268
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   20
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   316
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   740
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h21
		Private Sub chifAffAnneeDisp(webChartView_param As WebChartView)
		  webChartView_param.Freeze = True
		  
		  webChartView_param.Title = CHIFFAFFMOIS
		  webChartView_param.BackgroundType = WebChartView.BackgroundSolid
		  webChartView_param.BackgroundColor.append &cFFFFFF
		  webChartView_param.Type = webChartView_param.TypeLine
		  webChartView_param.FollowValues = True
		  webChartView_param.FollowAllSeries = True
		  
		  Dim S As ChartSerie
		  Dim chifAffAnneeResultsDoubleTrav() As Double
		  
		  For index As Integer = 0 To 4
		    S = New ChartSerie
		    S.Title = str(anneeDepart+index)
		    ReDim chifAffAnneeResultsDoubleTrav(-1)
		    For i As Integer = 0 To 11
		      chifAffAnneeResultsDoubleTrav.append(chifAffAnneeResultsDouble(index,i))
		    Next i
		    S.Data = chifAffAnneeResultsDoubleTrav
		    S.LineWeight = 2
		    S.MarkerType = S.MarkerNone
		    //S.Shadow = True
		    //S.ShadowColor = &c000000C3
		    webChartView_param.AddSerie(S)
		  Next index
		  
		  Dim i As Integer
		  Dim d As date = New Date
		  d.Day = 1
		  For i  = 1 to 12
		    d.Month = i
		    webChartView_param.Axes(0).Label.append New Date(d)
		  Next
		  webChartView_param.Axes(0).AutoLabel = False
		  webChartView_param.Axes(0).LabelStyle.Format = "M"
		  webChartView_param.setTransparency(40)
		  webChartView_param.Axes(1).MajorGridLine = New ChartLine
		  webChartView_param.Axes(1).MajorGridLine.FillColor = &cE4E4E4
		  webChartView_param.Axes(1).MaximumScale = maxScale
		  //webChartView_param.Axes(1).MaximumScale = 600
		  webChartView_param.Axes(1).MajorUnit = 1000
		  webChartView_param.Axes(1).LabelStyle.Format = " k"
		  webChartView_param.Axes(0).LabelStyle.TextSize = 14
		  webChartView_param.Axes(0).LabelInterval = -1
		  webChartView_param.LegendPosition = WebChartView.Position.Right
		  
		  webChartView_param.HighlightMinValue(&cFF0000, -1)
		  
		  webChartView_param.Freeze = False
		  webChartView_param.Animate = True
		  webChartView_param.Redisplay()
		  'webChartView_param.StartAnimation(True, ChartAnimate.kEaseOutCirc)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub chifAffAnneeExtr()
		  Dim strSQL As String = ""
		  
		  // Lecture du réalisé chiffre d'affaires (Facturation)
		  strSQL = "SELECT SUBSTR(realiserev_date,1,4) AS Annee, SUBSTR(realiserev_date,6,2) AS Mois, ROUND(SUM(realiserev_montant)/1000,0) As Montant, ROUND(SUM(realiserev_paiement/1000),0) As ChifAffenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE SUBSTR(realiserev_date,1,4) >= '" + str(anneeDepart) +"'" + _
		  "           AND SUBSTR(realiserev_gl,1,1) = '4' "  + _
		  "        GROUP BY Annee, Mois " + _
		  "        ORDER BY  Annee, Mois "
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  
		  Dim tempDict As New Dictionary
		  maxScale = 0
		  // Mettre les résultats dans un dictionnaire
		  Dim accMontant As Integer = 0
		  Dim annee As String = ""
		  While Not  dashboardRS.EOF
		    If annee <> dashboardRS.Field("Annee").StringValue Then 
		      accMontant = 0
		      annee = dashboardRS.Field("Annee").StringValue
		    End If
		    accMontant = accMontant + dashboardRS.Field("Montant").IntegerValue
		    tempDict.Value(dashboardRS.Field("Annee").StringValue + dashboardRS.Field("Mois").StringValue) =  accMontant
		    If  dashboardRS.Field("Mois").StringValue = "12" And accMontant > maxScale Then maxScale =  accMontant
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Compléter les valeurs
		  Dim valeurPrecedente As String = ""
		  For indexAnnee As Integer = 0 To 4
		    Dim anneeStr As String = str(anneeDepart + indexAnnee)
		    For indexMois As Integer = 1 To 12
		      Dim moisStr As String = str(indexMois)
		      If indexMois < 10 Then moisStr = "0" + moisStr
		      Dim anneeMoisStr As String = anneeStr + moisStr 
		      chifAffAnneeResultsDouble(indexAnnee, indexMois-1) = CDbl(tempDict.Lookup(anneeMoisStr,"0"))
		      If tempDict.Lookup(anneeMoisStr,"0") = "0" Then 
		        chifAffAnneeResultsDouble(indexAnnee, indexMois-1) = CDbl(valeurPrecedente)
		      Else
		        valeurPrecedente = tempDict.Lookup(anneeMoisStr,"0")
		      End If
		    Next indexMois
		  Next indexAnnee
		  
		  // Arrondir maxScaleTemp à la centaine supérieure
		  Dim maxScaleTemp As String = str(maxScale)
		  Dim maxScaleDouble As Double = maxScale
		  Dim longueur As Integer = Len(maxScaleTemp)
		  maxScale = Floor((maxScaleDouble + 10 ^ (longueur - 2)) / 10 ^ (longueur - 2)) * 10 ^ (longueur - 2)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listAllProjetsDetail()
		  Dim anneeFin, moisFin As String
		  
		  // Extraire Année et Mois passé
		  Dim interval1 As New Xojo.Core.DateInterval
		  interval1.Months = 1
		  Dim lastMonth As Xojo.Core.Date = Xojo.Core.Date.Now - interval1
		  anneeFin = str(lastMonth.Year)
		  moisFin = str(lastMonth.Month)
		  Dim interval2 As New Xojo.Core.DateInterval
		  interval2.Years = 4
		  Dim firstYear As Xojo.Core.Date = lastMonth - interval2
		  anneeDepart = firstYear.Year
		  //anneeDepart = 2012
		  
		  // Graphique 
		  //chifAffTypeGLExtr
		  //chifAffTypeGLDisp
		  
		  // Graphique chifAffenu par année
		  chifAffAnneeExtr
		  //chifAffAnneeDisp(ChifAffAnneeChartViewBis)
		  chifAffAnneeDisp(ChifAffAnneeChartView)
		  
		  // ChifAffenu par client
		  //chifAffClientExtr
		  //chifAffClientDisp
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private anneeClient As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private anneeDepart As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private chifAffAnneeResultsDouble(5,12) As Double
	#tag EndProperty

	#tag Property, Flags = &h21
		Private maxScale As Integer = 0
	#tag EndProperty


	#tag Constant, Name = CHIFFAFFMOIS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"BUSINESS INCOME COMPARISON by MONTH ($thousand)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"CHIFFRE D\'AFFAIRE COMPARATIF par MOIS ($mille)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"BUSINESS INCOME COMPARISON by MONTH ($thousand)"
	#tag EndConstant


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
