#tag WebPage
Begin WebContainer DesktopSelectionContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   642
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin DeskTopSelectionMenu DeskTopSelectionMenuContainerControl
      Cursor          =   0
      Enabled         =   True
      Height          =   56
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   0
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1350
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin DesktopAllChartsContainerControl SelectionAllChartsContainerControl
      Cursor          =   0
      Enabled         =   True
      Height          =   584
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "-1"
      TabOrder        =   1
      Top             =   57
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1350
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  changementPageGraphique("ChiffreAffaire")
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub changementPageGraphique(pageGraphique_param As String)
		  
		  //Déterminer quel panneau de graphique il faut afficher
		  //Dim pageGraphiqueAafficher As String = pageGraphiqueListBox.CellTag(idpageGraphique_param,2)
		  
		  // Restaurer la valeur de la clé du record 
		  //allpageGraphiqueStruct.id = pageGraphiqueListBox.CellTag(idpageGraphique_param,1)
		  
		  // Libérer l'espace mémoire si c'est le cas
		  If Session.pointeurDesktopAllChartsContainerControl <> Nil Then
		    Session.pointeurDesktopAllChartsContainerControl.Close
		    Session.pointeurDesktopAllChartsContainerControl = Nil
		  End If
		  
		  //Dim hauteurPanneaupageGraphiqueSideBar As Integer = Me.Height
		  Select Case pageGraphique_param
		    
		  Case "ChiffreAffaire" // pageGraphique Chiffre d'affaire
		    Dim desktopChiffreAffaireContainerControlTemp As DesktopChiffreAffaireChartContainerControl = New DesktopChiffreAffaireChartContainerControl
		    Session.pointeurDesktopAllChartsContainerControl = desktopChiffreAffaireContainerControlTemp
		    desktopChiffreAffaireContainerControlTemp.EmbedWithin(SelectionAllChartsContainerControl, 0, 0, SelectionAllChartsContainerControl.Width, SelectionAllChartsContainerControl.Height)
		    desktopChiffreAffaireContainerControlTemp.LockTop = True
		    desktopChiffreAffaireContainerControlTemp.LockBottom = True
		    desktopChiffreAffaireContainerControlTemp.LockLeft = True
		    desktopChiffreAffaireContainerControlTemp.LockRight = True
		    desktopChiffreAffaireContainerControlTemp.listAllProjetsDetail
		    
		  Case "TousProjets" // pageGraphique Tous les projets
		    Dim desktopTousProjetsChartContainerControlTemp As DesktopTousProjetsChartContainerControl = New DesktopTousProjetsChartContainerControl
		    Session.pointeurDesktopAllChartsContainerControl = desktopTousProjetsChartContainerControlTemp
		    desktopTousProjetsChartContainerControlTemp.EmbedWithin(SelectionAllChartsContainerControl, 0, 0, SelectionAllChartsContainerControl.Width, SelectionAllChartsContainerControl.Height)
		    desktopTousProjetsChartContainerControlTemp.LockTop = True
		    desktopTousProjetsChartContainerControlTemp.LockBottom = True
		    desktopTousProjetsChartContainerControlTemp.LockLeft = True
		    desktopTousProjetsChartContainerControlTemp.LockRight = True
		    desktopTousProjetsChartContainerControlTemp.listAllProjetsDetail
		    
		  Case "Projets" // pageGraphique Projets
		    Dim DesktopProjetContainerControlTemp As DesktopProjetContainerControl = New DesktopProjetContainerControl
		    Session.pointeurDesktopAllChartsContainerControl = DesktopProjetContainerControlTemp
		    DesktopProjetContainerControlTemp.EmbedWithin(SelectionAllChartsContainerControl, 0, 0, SelectionAllChartsContainerControl.Width, SelectionAllChartsContainerControl.Height)
		    DesktopProjetContainerControlTemp.LockTop = True
		    DesktopProjetContainerControlTemp.LockBottom = True
		    DesktopProjetContainerControlTemp.LockLeft = True
		    DesktopProjetContainerControlTemp.LockRight = True
		    DesktopProjetContainerControlTemp.DeskTopProjetSideBarDialog.populateProjet
		    
		  Case "RetardPaiementFacture" // pageGraphique Retard paiement de factures
		    Dim desktopDelaiPaiementChartContainerControlTemp As DesktopDelaiPaiementChartContainerControl = New DesktopDelaiPaiementChartContainerControl
		    Session.pointeurDesktopAllChartsContainerControl = desktopDelaiPaiementChartContainerControlTemp
		    desktopDelaiPaiementChartContainerControlTemp.EmbedWithin(SelectionAllChartsContainerControl, 0, 0, SelectionAllChartsContainerControl.Width, SelectionAllChartsContainerControl.Height)
		    desktopDelaiPaiementChartContainerControlTemp.LockTop = True
		    desktopDelaiPaiementChartContainerControlTemp.LockBottom = True
		    desktopDelaiPaiementChartContainerControlTemp.LockLeft = True
		    desktopDelaiPaiementChartContainerControlTemp.LockRight = True
		    desktopDelaiPaiementChartContainerControlTemp.listRetardPaiementFactureDetail
		    
		  Case "AssigneNonAssigne" // pageGraphique Assigne non assigne
		    Dim desktopAssigneNonAssigneChartContainerControlTemp As DesktopAssigneNonAssigneChartContainerControl = New DesktopAssigneNonAssigneChartContainerControl
		    Session.pointeurDesktopAllChartsContainerControl = desktopAssigneNonAssigneChartContainerControlTemp
		    desktopAssigneNonAssigneChartContainerControlTemp.EmbedWithin(SelectionAllChartsContainerControl, 0, 0, SelectionAllChartsContainerControl.Width, SelectionAllChartsContainerControl.Height)
		    desktopAssigneNonAssigneChartContainerControlTemp.LockTop = True
		    desktopAssigneNonAssigneChartContainerControlTemp.LockBottom = True
		    desktopAssigneNonAssigneChartContainerControlTemp.LockLeft = True
		    desktopAssigneNonAssigneChartContainerControlTemp.LockRight = True
		    desktopAssigneNonAssigneChartContainerControlTemp.listAssigneNonAssigneDetail
		    
		  Case "ExcelChiffriers" // Chiffrier Excel projet
		    Dim desktopExcelProjetsChiffrierChartContainerControlTemp As DesktopExcelProjetsChiffrierChartContainerControl = New DesktopExcelProjetsChiffrierChartContainerControl
		    Session.pointeurDesktopAllChartsContainerControl = desktopExcelProjetsChiffrierChartContainerControlTemp
		    desktopExcelProjetsChiffrierChartContainerControlTemp.EmbedWithin(SelectionAllChartsContainerControl, 0, 0, SelectionAllChartsContainerControl.Width, SelectionAllChartsContainerControl.Height)
		    desktopExcelProjetsChiffrierChartContainerControlTemp.LockTop = True
		    desktopExcelProjetsChiffrierChartContainerControlTemp.LockBottom = True
		    desktopExcelProjetsChiffrierChartContainerControlTemp.LockLeft = True
		    desktopExcelProjetsChiffrierChartContainerControlTemp.LockRight = True
		    desktopExcelProjetsChiffrierChartContainerControlTemp.listExcelProjetsChiffrierDetail
		  End Select 
		  
		  
		  
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
