#tag Module
Protected Module SelectionModule
	#tag Constant, Name = ALERTE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Alert"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Alerte"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Alert"
	#tag EndConstant

	#tag Constant, Name = ASSIGNENONASSIGNE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Assigned\r\nNot Assigned"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Assign\xC3\xA9\r\nNon Assign\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Assigned\r\nNot Assigned"
	#tag EndConstant

	#tag Constant, Name = CHIFFAFFAIRE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Business \r\nIncome"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chiffre \r\nd\'affaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Business \r\nIncome"
	#tag EndConstant

	#tag Constant, Name = CHIFFEXCEL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Excel\r\nWorksheets"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chiffriers\r\nExcel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Excel\r\nWorksheets"
	#tag EndConstant

	#tag Constant, Name = EMPLOYE, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Employee"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Employee"
	#tag EndConstant

	#tag Constant, Name = HOUR, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Hour"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Heure"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Hour"
	#tag EndConstant

	#tag Constant, Name = PROJET, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Project"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projets"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Projects"
	#tag EndConstant

	#tag Constant, Name = RETPAIEMENTFAC, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Late\r\nPayment\r\nInvoice"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Retard\r\nPaiement\r\nFacture"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Late\r\nPayment\r\nInvoice"
	#tag EndConstant

	#tag Constant, Name = SOUMISSION, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Proposal"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Soumission"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Proposal"
	#tag EndConstant

	#tag Constant, Name = TOUSPROJETS, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"All \r\nprojects"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Tous les\r\nprojets"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"All \r\nprojects"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
