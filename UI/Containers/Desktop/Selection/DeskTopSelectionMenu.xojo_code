#tag WebPage
Begin WebContainer DeskTopSelectionMenu
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   56
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   838
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebToolbar SelToolbar
      ButtonDisabledStyle=   "0"
      ButtonStyle     =   "0"
      Cursor          =   0
      Enabled         =   True
      Height          =   56
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "2 WebToolbarSpace Space1  -1 Select... 0 0 1 1	2 WebToolbarSpace Space2  -1 Select... 0 0 1 1	0 WebToolbarButton AlertButton I1NlbGVjdGlvbk1vZHVsZS5BTEVSVEU= -1 Select... 41 0 1 1	1 WebToolbarSeparator Separator4  -1 Select... 0 0 1 1	0 WebToolbarButton BusinessButton I1NlbGVjdGlvbk1vZHVsZS5DSElGRkFGRkFJUkU= -1 Select... 64 0 1 1	1 WebToolbarSeparator Separator3  -1 Select... 0 0 1 1	0 WebToolbarButton AllProjectsButton I1NlbGVjdGlvbk1vZHVsZS5UT1VTUFJPSkVUUw== 809426943 Select... 58 0 1 1	0 WebToolbarButton ProjectButton I1NlbGVjdGlvbk1vZHVsZS5QUk9KRVQ= 724031487 Select... 52 0 1 1	1 WebToolbarSeparator Separator1  -1 Select... 0 0 1 1	0 WebToolbarButton LatePaymentButton I1NlbGVjdGlvbk1vZHVsZS5SRVRQQUlFTUVOVEZBQw== -1 Select... 62 0 1 1	0 WebToolbarButton AssignedButton I1NlbGVjdGlvbk1vZHVsZS5BU1NJR05FTk9OQVNTSUdORQ== 1167366143 Select... 87 0 1 1	1 WebToolbarSeparator Separator5  -1 Select... 0 0 1 1	0 WebToolbarButton EmployeeButton I1NlbGVjdGlvbk1vZHVsZS5FTVBMT1lF 77678591 Select... 67 0 1 1	0 WebToolbarButton HourButton I1NlbGVjdGlvbk1vZHVsZS5IT1VS -1 Select... 42 0 1 1	1 WebToolbarSeparator Separator2  -1 Select... 0 0 1 1	0 WebToolbarButton ExcelButton I1NlbGVjdGlvbk1vZHVsZS5DSElGRkVYQ0VM -1 Select... 77 0 1 1	3 WebToolbarFlexibleSpace FlexibleSpace1  -1 Select... 0 0 1 1"
      ItemStyle       =   "0"
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      ToggledDisabledStyle=   "0"
      ToggledStyle    =   "0"
      Top             =   0
      Vertical        =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   838
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
#tag EndWindowCode

#tag Events SelToolbar
	#tag Event
		Sub ButtonAction(Item As WebToolbarButton)
		  Dim desktopSelectionContainerControlTrav As DesktopSelectionContainerControl = DesktopSelectionContainerControl(Self.Parent)
		  
		  Select Case Item.Caption
		    
		  Case SelectionModule.CHIFFAFFAIRE
		    desktopSelectionContainerControlTrav.changementPageGraphique("ChiffreAffaire")
		    
		  Case SelectionModule.TOUSPROJETS
		    desktopSelectionContainerControlTrav.changementPageGraphique("TousProjets")
		    
		  Case SelectionModule.PROJET
		    desktopSelectionContainerControlTrav.changementPageGraphique("Projets")
		    
		  Case SelectionModule.RETPAIEMENTFAC
		    desktopSelectionContainerControlTrav.changementPageGraphique("RetardPaiementFacture")
		    
		  Case SelectionModule.ASSIGNENONASSIGNE
		    desktopSelectionContainerControlTrav.changementPageGraphique("AssigneNonAssigne")
		    
		  Case SelectionModule.CHIFFEXCEL
		    desktopSelectionContainerControlTrav.changementPageGraphique("ExcelChiffriers")
		    
		    
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
