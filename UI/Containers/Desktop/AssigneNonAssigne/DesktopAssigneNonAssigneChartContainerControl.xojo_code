#tag WebPage
Begin DesktopAllChartsContainerControl DesktopAssigneNonAssigneChartContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   584
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebChartView NonAssigneCodeGLChartView
      Animate         =   False
      AnimationHorizontal=   False
      AnimationTime   =   800
      BackgroundType  =   0
      Border          =   False
      BorderColor     =   &c82879000
      Continuous      =   False
      CrossesBetweenTickMarks=   False
      Cursor          =   0
      DisplaySelectionBar=   False
      DoubleYAxe      =   False
      DoughnutRadius  =   0.0
      Enabled         =   True
      EnableSelection =   False
      FollowAllSeries =   False
      FollowMouse     =   False
      FollowValues    =   False
      Freeze          =   False
      Height          =   494
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LastError       =   ""
      Left            =   20
      LegendPosition  =   ""
      LiveRefresh     =   False
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Margin          =   20
      PieRadarAngle   =   0.0
      Precision       =   0
      Scope           =   0
      ShowHelpTag     =   False
      Style           =   "0"
      TabOrder        =   -1
      TextFont        =   ""
      Title           =   ""
      Top             =   49
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton NonAssigneTypeGLAnneePlus
      AutoDisable     =   False
      Caption         =   "#ANNEEPLUS"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   546
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton NonAssigneTypeGLAnneeMoins
      AutoDisable     =   False
      Caption         =   "#ANNEEMOINS"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   930
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   546
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton NonAssigneTypeGLPageMoins
      AutoDisable     =   False
      Caption         =   "#PAGEMOINS"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   673
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   546
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton NonAssigneTypeGLPagePlus
      AutoDisable     =   False
      Caption         =   "#PAGEPLUS"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   251
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   0
      Top             =   546
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel YearLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   404
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1787973756"
      TabOrder        =   6
      Text            =   "YEAR"
      TextAlign       =   0
      Top             =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   182
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub listAssigneNonAssigneDetail()
		  Dim anneeFin, moisFin As String
		  
		  // Extraire Année et Mois passé
		  Dim interval1 As New Xojo.Core.DateInterval
		  interval1.Months = 1
		  Dim lastMonth As Xojo.Core.Date = Xojo.Core.Date.Now - interval1
		  anneeFin = str(lastMonth.Year)
		  moisFin = str(lastMonth.Month)
		  Dim interval2 As New Xojo.Core.DateInterval
		  interval2.Years = 4
		  Dim firstYear As Xojo.Core.Date = lastMonth - interval2
		  //anneeDepart = firstYear.Year
		  //anneeDepartNonAssigneCodeGL = 2016
		  anneeDepartNonAssigneCodeGL = Xojo.Core.Date.Now.Year
		  
		  // Graphique non assigné
		  nonAssigneCodeGLExtr
		  nonAssigneCodeGLDisp
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub nonAssigneCodeGLDisp()
		  If nonAssigneCodeGLLibelle.Ubound = -1 Then Exit Sub
		  
		  YearLabel.Text = str(anneeDepartNonAssigneCodeGL)
		  nonAssigneCodeGLChartView.Type = nonAssigneCodeGLChartView.TypeBullet
		  
		  // Vider le graphe précédent, si présent
		  Dim chartSerieTemp() As ChartSerie = nonAssigneCodeGLChartView.Series
		  Dim nbre As Integer = chartSerieTemp.Ubound
		  For i As Integer = 0 To nbre
		    nonAssigneCodeGLChartView.Series.Remove(0)
		  Next I
		  
		  Dim S As new ChartSerie
		  S.Title = AMOUNT
		  Dim nonAssigneCodeGLMontantTemp() As Integer
		  For i As Integer = indexTableauNonAssigne To indexTableauNonAssigne + 20
		    nonAssigneCodeGLMontantTemp.Append(nonAssigneCodeGLMontant(i))
		  Next i
		  S.Data = nonAssigneCodeGLMontantTemp
		  nonAssigneCodeGLChartView.AddSerie(S)
		  
		  S = New ChartSerie
		  S.Title = ""
		  S.Data = nonAssigneCodeGLMontantTemp
		  //S.Data = Array(8500, 5000, 1500)
		  S.FillColor = &c505050
		  S.FillType = ChartSerie.FillPicture
		  
		  // Vider Axes 0
		  Dim chartAxesLabelTemp() As Variant = nonAssigneCodeGLChartView.Axes(0).Label
		  nbre = chartAxesLabelTemp.Ubound
		  For i As Integer = 0 To nbre
		    nonAssigneCodeGLChartView.Axes(0).Label.Remove(0)
		  Next I
		  // Remplir Axes 0
		  nonAssigneCodeGLChartView.AddSerie(S)
		  For i As Integer = indexTableauNonAssigne To indexTableauNonAssigne + 20
		    nonAssigneCodeGLChartView.Axes(0).Label.append nonAssigneCodeGLLibelle(i)
		  Next i
		  
		  nonAssigneCodeGLChartView.BackgroundType = WebChartView.BackgroundSolid
		  nonAssigneCodeGLChartView.BackgroundColor.append &cFFFFFF
		  nonAssigneCodeGLChartView.LegendPosition = WebChartView.Position.Right
		  
		  nonAssigneCodeGLChartView.setTransparency(40)
		  
		  nonAssigneCodeGLChartView.Axes(1).Visible = False
		  nonAssigneCodeGLChartView.Axes(1).MaximumScale = nonAssigneCodeGLmaxScale
		  nonAssigneCodeGLChartView.Axes(0).AxeLine.visible = False
		  
		  nonAssigneCodeGLChartView.setDataLabel("", 14)
		  nonAssigneCodeGLChartView.DataLabel.Position = 5
		  
		  nonAssigneCodeGLChartView.Redisplay()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub nonAssigneCodeGLExtr()
		  Dim strSQL As String = ""
		  nonAssigneCodeGLMaxScale = 0
		  Redim nonAssigneCodeGLLibelle(-1)
		  Redim nonAssigneCodeGLMontant(-1)
		  Dim nonAssigneCodeGLLibelleTemp() As String
		  Dim nonAssigneCodeGLMontantTemp() As Integer
		  
		  // Lecture du réalisé pour les revenus (Facturation) par code de Grand Livre
		  strSQL = "SELECT realiserev_gl AS GL, realiserev_gl_desc AS Gl_Desc, ROUND(SUM(realiserev_montant),0) As SumMontant " + _
		  "        FROM realiserev " + _ 
		  "       WHERE realiserev_projet_no = '' " + _
		  "           AND SUBSTRING(realiserev_date,1,4) = '" + str(anneeDepartNonAssigneCodeGL) + "' " + _
		  "       GROUP BY GL, Gl_Desc "+ _ 
		  "       ORDER BY GL, Gl_Desc "
		  
		  Dim dashboardRS As RecordSet = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  While Not  dashboardRS.EOF
		    If dashboardRS.Field("SumMontant").IntegerValue > 0 Then
		      Dim libelleTemp As String = dashboardRS.Field("GL").StringValue + " " + dashboardRS.Field("Gl_Desc").StringValue
		      nonAssigneCodeGLLibelleTemp.Append( libelleTemp)
		      nonAssigneCodeGLMontantTemp.Append(dashboardRS.Field("SumMontant").IntegerValue)
		      If dashboardRS.Field("SumMontant").IntegerValue > nonAssigneCodeGLmaxScale Then nonAssigneCodeGLmaxScale =  dashboardRS.Field("SumMontant").IntegerValue
		    End If
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT GL, Gl_Desc, SUM(Montant) As SumMontant  FROM" + _
		  "   (SELECT realisedep_gl AS GL, realisedep_gl_desc AS Gl_Desc, ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '' "  + _
		  "           AND SUBSTRING(realisedep_date,1,4) = '" + str(anneeDepartNonAssigneCodeGL) + "' " + _
		  "       GROUP BY GL, Gl_Desc " + _
		  "   UNION " + _
		  "   SELECT '9999' AS GL, 'Salaires et bénéfices marginaux' AS Gl_Desc, ROUND(SUM(realisemocont_salbrut+realisemocont_das+realisemocont_cnesst),0) As Montant " + _
		  "        FROM realisemocont " + _ 
		  "       WHERE realisemocont_projet_no = '' "  + _
		  "          AND SUBSTRING(realisemocont_date_periode,1,4) = '" + str(anneeDepartNonAssigneCodeGL) + "' " + _
		  "       GROUP BY GL, Gl_Desc "+ _
		  "   )" + _
		  "  AS GLDepTable " + _ 
		  "  GROUP BY GL, Gl_Desc " + _ 
		  "  ORDER BY  GL, Gl_Desc "
		  
		  dashboardRS = Session.bdGroupeRPF.SQLSelect(strSQL)
		  If dashboardRS = Nil Then Exit
		  While Not  dashboardRS.EOF
		    If dashboardRS.Field("SumMontant").IntegerValue > 0 Then
		      Dim libelleTemp As String = dashboardRS.Field("GL").StringValue + " " + dashboardRS.Field("Gl_Desc").StringValue
		      nonAssigneCodeGLLibelleTemp.Append( libelleTemp)
		      nonAssigneCodeGLMontantTemp.Append(dashboardRS.Field("SumMontant").IntegerValue)
		      If dashboardRS.Field("SumMontant").IntegerValue > nonAssigneCodeGLmaxScale Then nonAssigneCodeGLmaxScale =  dashboardRS.Field("SumMontant").IntegerValue
		    End If
		    dashboardRS.MoveNext
		  Wend
		  dashboardRS.Close
		  dashboardRS = Nil
		  
		  nbreEntreeNonAssigne = nonAssigneCodeGLLibelleTemp.Ubound
		  If nonAssigneCodeGLLibelleTemp.Ubound > -1 Then
		    nonAssigneCodeGLLibelle = nonAssigneCodeGLLibelleTemp
		    nonAssigneCodeGLMontant = nonAssigneCodeGLMontantTemp
		  End If
		  indexTableauNonAssigne = 0
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private anneeDepartNonAssigneCodeGL As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private indexTableauNonAssigne As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nbreEntreeNonAssigne As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nonAssigneCodeGLLibelle() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nonAssigneCodeGLMaxScale As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nonAssigneCodeGLMontant() As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private nonAssigneCodeGLMontantAvecProfit As Integer
	#tag EndProperty


	#tag Constant, Name = AMOUNT, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Amount"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Montant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Amount"
	#tag EndConstant

	#tag Constant, Name = ANNEEMOINS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Year -"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ann\xC3\xA9e -"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Year -"
	#tag EndConstant

	#tag Constant, Name = ANNEEPLUS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Year +"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ann\xC3\xA9e +"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Year +"
	#tag EndConstant

	#tag Constant, Name = PAGEMOINS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Page -"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Page -"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Page -"
	#tag EndConstant

	#tag Constant, Name = PAGEPLUS, Type = String, Dynamic = True, Default = \"", Scope = Private
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Page +"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Page +"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Page +"
	#tag EndConstant


#tag EndWindowCode

#tag Events NonAssigneTypeGLAnneePlus
	#tag Event
		Sub Action()
		  If anneeDepartNonAssigneCodeGL + 1 > Xojo.Core.Date.Now.Year Then
		    anneeDepartNonAssigneCodeGL = Xojo.Core.Date.Now.Year
		  Else
		    anneeDepartNonAssigneCodeGL = anneeDepartNonAssigneCodeGL + 1
		  End 
		  nonAssigneCodeGLExtr
		  nonAssigneCodeGLDisp
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NonAssigneTypeGLAnneeMoins
	#tag Event
		Sub Action()
		  If anneeDepartNonAssigneCodeGL - 1 < Xojo.Core.Date.Now.Year - 5 Then
		    anneeDepartNonAssigneCodeGL = Xojo.Core.Date.Now.Year - 5
		  Else
		    anneeDepartNonAssigneCodeGL = anneeDepartNonAssigneCodeGL - 1
		  End 
		  nonAssigneCodeGLExtr
		  nonAssigneCodeGLDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NonAssigneTypeGLPageMoins
	#tag Event
		Sub Action()
		  indexTableauNonAssigne = indexTableauNonAssigne - 20
		  If (indexTableauNonAssigne -20) < 0 Then
		    indexTableauNonAssigne = 0
		  End 
		  nonAssigneCodeGLDisp
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NonAssigneTypeGLPagePlus
	#tag Event
		Sub Action()
		  Dim nbreEntreeNonAssigneTemp As Integer = nbreEntreeNonAssigne
		  indexTableauNonAssigne = indexTableauNonAssigne + 20
		  Dim indexTableauNonAssigneTemp As Integer = indexTableauNonAssigne
		  If (indexTableauNonAssigne + 20) > nbreEntreeNonAssigne Then
		    indexTableauNonAssigne = nbreEntreeNonAssigne - 20
		  End 
		  nonAssigneCodeGLDisp
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
